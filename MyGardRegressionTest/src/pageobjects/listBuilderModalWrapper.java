package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="listBuilderModalWrapper"                                
               , summary=""
               , connection="MyGardE2E"
               , auraComponent="listBuilderModalWrapper"
               , namespacePrefix=""
     )             
public class listBuilderModalWrapper {

	@ButtonType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneWorkspace') and (ancestor::div[contains(@class,'active') and contains(@class,'main-content')]//div[contains(@class, 'oneGlobalNav') or contains(@class, 'tabBarContainer')]//div[contains(@class, 'tabContainer') and contains(@class, 'active')] )]//button[normalize-space(.)='Add Covers and Vessels']")
	public WebElement addCoversAndVessels;
	
}
