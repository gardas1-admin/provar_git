package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardMyPortfolioClaimDetails"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardMyPortfolioClaimDetails {

	@TextType()
	@FindBy(xpath = "//div[text()='Claim details']")
	public WebElement Claim_details;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"page-title-text\")]")
	public WebElement Claim_details1;
			
}
