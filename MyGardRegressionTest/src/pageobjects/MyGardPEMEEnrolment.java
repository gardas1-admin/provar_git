package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardPEMEEnrolment"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardPEMEEnrolment {

	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Member:')]/following-sibling::div[1]")
	public WebElement NewEnrolClient;
	@PageWaitAfter.Field(timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Member:')]/following-sibling::div[1]/descendant::input[@placeholder='Search']")
	public WebElement PEMENewEnrolmentSearch;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Member:')]/following-sibling::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]")
	public WebElement PEMENewEnrolmentSearchedResult;
	@ButtonType()
	@FindBy(xpath = "//span[text()='Contact:']/ancestor::label[1]/following-sibling::div[1]")
	public WebElement PEMENewEnrollmentContact;
	@LinkType()
	@FindBy(xpath = "//span[text()='Contact:']/ancestor::label[1]/following-sibling::div[1]/descendant::ul[1]/descendant::span[5]")
	public WebElement MyGardPEMEContactSecondCheckBox;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Company name:']/following-sibling::input")
	public WebElement NewEnrollmentManningAgentCompanyName;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(text())='Contact person:']/ancestor::label[1]/following-sibling::input")
	public WebElement NewEnrollmentManningAgentContactPerson;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='City:']/following-sibling::input")
	public WebElement NewEnrollmentManningAgentCity;
	@TextType()
	@FindBy(xpath = "//input[@value='Next']")
	public WebElement NewEnrollmentNextBtn;
	@TextType()
	@FindBy(xpath = "//label[text()='Address:']")
	public WebElement PEMEEnrollmentClientPostalAddress1;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Prev']/ancestor::div[1]/following-sibling::div[1]")
	public WebElement NewEnrollmentSubmitBtn;
	@TextType()
	@FindBy(xpath = "//div[contains((text()),'Thank you for contacting Gard. Your request has been successfully registered. It will be processed in the next coming days. If nothing is received please contact Gard again.')]")
	public WebElement NewEnrollmentConfirmationMessage;
	@ButtonType()
	@FindBy(xpath = "//div[normalize-space(text())='Thank you for contacting Gard. Your request has been successfully registered. It will be processed in the next coming days. If nothing is received please contact Gard again.']/ancestor::div[1]/descendant::input")
	public WebElement NewEnrollmentConfirmationMesssageOkBtn;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::div[1]/descendant::input[@placeholder='Search']/ancestor::div[1]/following-sibling::ul[1]/descendant::li/descendant::a[@data-normalized-text='Blue Ship Invest AS']")
	public WebElement PEMEEnrollmentDetailsSearchedClient;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Status:')]/descendant::span[1]")
	public WebElement PEMEMembershipStatusUnderApproval;
	@TextType()
	@FindBy(xpath = "//div[@title='My account']")
	public WebElement MyAccount;
	@LinkType()
	@FindBy(xpath = "//a[text()='Logout']")
	public WebElement Logout;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::div[1]/descendant::input[@placeholder='Search']/ancestor::div[1]/following-sibling::ul[1]/descendant::div/descendant::li[2]/descendant::span[1]")
	public WebElement PEMENewEnrollmentSearchedClientFinal;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(text())='Thank you for contacting Gard. Your request has been successfully registered.']")
	public WebElement NewEnrollmentConfirmationMessageHover;
	@LinkType()
	@FindBy(xpath = "//span[text()='Contact:']/ancestor::label[1]/following-sibling::div[1]/descendant::ul[1]/descendant::span[2]")
	public WebElement MyGardPEMEContactFirstCheckBox;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Specify another address']")
	public WebElement DebitNotesDetailsAnotherAddressRadio;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Company name:')]/following-sibling::input[1]")
	public WebElement DebitNotesDetailsCompanyName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Contact person name:')]/following-sibling::input[1]")
	public WebElement DebitNotesDetailContactPersonName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Contact person email:')]/following-sibling::input[1]")
	public WebElement DebitNotesDetailsContactPersonEmail;
	@FindBy(xpath = "//label[contains(normalize-space(text()),'City:')]/following::input[1]")
	@TextType()
	public WebElement DebitNotesDetailsCity;
	@TextType()
	@FindBy(xpath = "//label[text()='Email:']/following-sibling::input[1]")
	public WebElement NewEnrolmentManningAgentEmail;
	@LinkType()
	@FindBy(xpath = "//div[text()='Debit Note details']/following-sibling::span[1]/descendant::div[3]")
	public WebElement DebitNotesDetailsEditIcon;
	@TextType()
	@FindBy(xpath = "//label[text()='Use Member postal address']")
	public WebElement EditDebitNoteAddressUseClientPostalAddress;
	@TextType()
	@FindBy(xpath = "//label[text()='Use Member postal address']/ancestor::table[1]/ancestor::div[2]/following-sibling::span[1]/descendant::label[text()='Comments:']/following-sibling::input[1]")
	public WebElement EditDebitNoteAddressComments;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Use Member postal address']/ancestor::table[1]/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement EditDebitNoteAddressSubmitBtn;
	@ButtonType()
	@FindByLabel(label = "Ok")
	public WebElement EditDebitNoteConfirmationMesssageOkBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Activity feed']")
	public WebElement ActivityFeed;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Use member postal address')]")
	public WebElement NewEnrolmentDebitNotesUseMemPostalAdd;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Contact person name:')]/following-sibling::input[1]")
	public WebElement NewEnrolmentDebitNotesDetailsContactPersonName;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Member:')]/following-sibling::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]")
	public WebElement PEMEEnroledSearchedClient;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Member:')]/following-sibling::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]")
	public WebElement PRODPEMENewEnrolmentSearchedResult;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Member:']/following::button[1]")
	public WebElement MemberDropdown;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='PEME Examination details']")
	public WebElement PEMEExaminationDetails;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)")
	public WebElement ListObjectValues;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[1]")
	public WebElement ListObjectValues1;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[2]")
	public WebElement ListObjectValues2;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[3]")
	public WebElement ListObjectValues3;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[4]")
	public WebElement ListObjectValues4;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[5]")
	public WebElement ListObjectValues5;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[6]")
	public WebElement ListObjectValues6;	
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[7]")
	public WebElement ListObjectValues7;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[8]")
	public WebElement ListObjectValues8;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[9]")
	public WebElement ListObjectValues9;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[10]")
	public WebElement ListObjectValues10;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[11]")
	public WebElement ListObjectValues11;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[12]")
	public WebElement ListObjectValues12;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[13]")
	public WebElement ListObjectValues13;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[14]")
	public WebElement ListObjectValues14;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[15]")
	public WebElement ListObjectValues15;
	@ButtonType()
	@FindBy(xpath = "(//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/li/a/label/span)[16]")
	public WebElement ListObjectValues16;
	@TextType()
	@FindBy(xpath = "//div[@id='mCSB_6_container' and contains(@class,'mCSB_container')]/../../../div/input[1]")
	public WebElement ObjInput;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Object']/following::button[1]")
	public WebElement ObjectDropdown;
	@TextType()
	@FindBy(xpath = "//label[text()='Object']/following::li[text()='No result found!']")
	public WebElement NoResultFound;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Billing address line 1:']/following-sibling::input")
	public WebElement Address1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Billing address line 2:']/following-sibling::input")
	public WebElement Address2;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Billing address line 3:']/following-sibling::input")
	public WebElement Address3;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Billing address line 4:']/following-sibling::input")
	public WebElement Address4;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Zip code:']/following-sibling::input")
	public WebElement ZipCode;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='State:']/following-sibling::input")
	public WebElement State;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Country:']/following-sibling::input")
	public WebElement Country;
}
