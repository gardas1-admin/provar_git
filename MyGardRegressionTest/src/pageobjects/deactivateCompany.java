package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Deactivate Company"                                
               , summary=""
               , connection="MyGardRegressionQABox"
               , auraComponent="deactivateCompany"
               , namespacePrefix=""
     )             
public class deactivateCompany {

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Ok']")
	public WebElement okButton;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='This company is MyGard Enabled and cannot be deactivated.']")
	public WebElement ErrorVerification;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Company has been successfully deactivated.']")
	public WebElement DeactivatedMessage;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='This company is on risk and cannot be deactivated.']")
	public WebElement CompanyOnRisk;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='You cannot deactivate a company where contacts have assigned an ESP Skill']")
	public WebElement ESPSkillError;
	@TextType()
	@AuraBy(componentXPath = "//force:modal//lightning:inputField[@aura:id= 'reasonField']", qualifier = "Reason_for_deactivation__c")
	@SalesforceField(name = "Reason_for_deactivation__c", object = "Account")
	public WebElement reasonForDeactivation;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//button[normalize-space(.)='Save']")
	public WebElement save;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='This company is on risk and cannot be deactivated.']")
	public WebElement BrokerOnRiskMessage1;
	@ButtonType()
	@AuraBy(componentXPath = "//force:modal//lightning:inputField[@aura:id= 'reasonField']", qualifier = "Reason_for_deactivation__c")
	@SalesforceField(name = "Reason_for_deactivation__c", object = "Account")
	public WebElement ReasonForDeactivationDD;
	@ButtonType()
	@AuraBy(componentXPath = "//force:modal//lightning:inputField[@aura:id= 'reasonField']", qualifier = "Reason_for_deactivation__c")
	@SalesforceField(name = "Reason_for_deactivation__c", object = "Account")
	public WebElement ReasonForDeactivation;
	
}
