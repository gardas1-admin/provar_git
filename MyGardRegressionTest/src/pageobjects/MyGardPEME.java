package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardPEME"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardPEME {

	@LinkType()
	@FindBy(xpath = "//a[text()='Join programme']")
	public WebElement joinProgramme;
			
}
