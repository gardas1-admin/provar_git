package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="downloadEmailsAsFiles"                                
               , summary=""
               , connection="MyGardE2E"
               , auraComponent="downloadEmailsAsFiles"
               , namespacePrefix=""
     )             
public class downloadEmailsAsFiles {

	@ButtonType()
	@AuraBy(componentXPath = "//lightning:button[@label= 'Convert emails to files']")
	public WebElement convertEmailsToFiles;
	@ButtonType()
	@AuraBy(componentXPath = "//lightning:button[@label= 'Convert emails to files']")
	public WebElement convertEmailsToFiles1;
	
}
