package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="home__heroChart"                                
               , summary=""
               , connection="MyGardRegressionUAT"
               , lightningComponent="heroChart"
               , namespacePrefix="home"
     )             
public class home__heroChart {

	@TextType()
	@FindBy(xpath = "(//span[@class='slds-card__header-title slds-truncate'])[1]")
	public WebElement QuarterlyPerformance_Title;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Quarterly Performance']")
	public WebElement QuartelyPerformance;
	
}
