package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardMyRequests"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class MyGardMyRequests {

	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='Crown Ship Builders Corp Ship Insurers Ltd. Search byAny wordExact phrase Check all Crown Ship Builder... Ship Insurers Ltd.']/div/button")
	public WebElement ClientFilter;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Crown Ship Builder...']/../input[1]")
	public WebElement CrownShipBuilderOption;
	@LinkType()
	@FindBy(xpath = "//span/div/table/tbody/tr[1]/td[3]/a")
	public WebElement ServiceReq_requestName;
	@TextType()
	@FindBy(xpath = "//span/div/table/tbody/tr[1]/td[6]/span")
	public WebElement ServiceRequest_caseHandler;
	@LinkType()
	@FindBy(xpath = "//span/div/table/tbody/tr[1]/td[2]/a")
	public WebElement ServiceRequest_requestId;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Raised date')]/div[contains(@class,'item-sort')]")
	public WebElement RaisedDate_Sorted;
			
}
