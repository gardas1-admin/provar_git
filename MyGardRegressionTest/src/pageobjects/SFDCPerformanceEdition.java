package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCPerformanceEdition"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionPROD"
     )             
public class SFDCPerformanceEdition {

	@LinkType()
	@FindBy(xpath = "//div[@id='Contact_body']/table/tbody/tr[2]/th/a[1]")
	public WebElement Contact;
	@LinkType()
	@FindBy(xpath = "//td/div/div/div/a[normalize-space(.)='Switch to Lightning Experience' and contains(@class,'switch-to-lightning')]")
	public WebElement switchToLightningExperience;
	@TextType()
	@FindBy(xpath = "//div/div/div/div/div/input[@data-aura-class='uiInput uiInputTextForAutocomplete uiInput--{remove}' and @placeholder='Search Salesforce' and @title='Search Salesforce' and @type='text']")
	public WebElement searchSalesforce;
	@LinkType()
	@FindBy(xpath = "//th//a[normalize-space(.)=concat('Nathan O',\"'\",'Connor') and @data-aura-class='forceOutputLookup' and @title=concat('Nathan O',\"'\",'Connor')]")
	public WebElement Nathan_O_Connor;
	@LinkType()
	@FindBy(xpath = "(//a[text()='Notification – PEME enrolment submission for Crown Ship Builders Corp'])[1]")
	public WebElement notificationPEMEEnrolmentSubmissionForCrownShipBuildersCorp;
			
}
