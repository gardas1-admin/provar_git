package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCUnitedInsuranceBrokerCompanyHome"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCUnitedInsuranceBrokerCompanyHome {

	@BooleanType()
	@FindBy(xpath = "//td[text()='Broker On Risk']/following-sibling::td[1]/descendant::div[1]/descendant::img[@title='Checked']")
	public WebElement BrokerOnRiskFlag;
	@LinkType()
	@FindBy(xpath = "//th[text()='Contact Name']/ancestor::tr[1]/following-sibling::tr[1]/descendant::a[text()='Tom Richardson']")
	public WebElement ContactTomRichardson;
	@TextType()
	@FindBy(xpath = "//span[contains(normalize-space(text()),'MyGard show as acquired by')]/ancestor::td[1]/following-sibling::td[1]")
	public WebElement AcquiredBy;
	@ButtonType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::input[@title='Save']")
	public WebElement SaveBtn;
	@BooleanType()
	@FindBy(xpath = "//td[text()='MyGard data sharing enabled']/following-sibling::td[1]/descendant::img[@title='Checked']")
	public WebElement MyGardDataSharingEnable;
	@FindBy(xpath = "//td[text()='MyGard data sharing enabled']/following-sibling::td[1]/descendant::img[@title='Checked']")
	@BooleanType()
	public WebElement EnableShareData;
	@ButtonType()
	@FindBy(xpath = "//input[@title='New Shared Company']")
	public WebElement NewSharedCompanyBtn;
	@LinkType()
	@FindBy(xpath = "//span[contains((text()),'MyGard data sharing: target companies')]")
	public WebElement MyGardDataSharingTargetCompanies;
	@LinkType()
	@FindBy(xpath = "//span[text()='MyGard data sharing: received data']")
	public WebElement MyGardDataSharingReceivedData1;
	@BooleanType()
	@FindBy(xpath = "//td[text()='MyGard enabled']/following-sibling::td[1]/descendant::img[1]")
	public WebElement MyGardEnabledCheck;
	@BooleanType()
	@FindBy(xpath = "//td[text()='MyGard PEME enabled']/following-sibling::td[1]/descendant::img[1]")
	public WebElement MyGardPEMEEnabledChecked;
			
}
