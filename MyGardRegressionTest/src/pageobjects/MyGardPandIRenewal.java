package pageobjects;

import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.ButtonType;
import com.provar.core.testapi.annotations.JavascriptBy;
import com.provar.core.testapi.annotations.LinkType;
import com.provar.core.testapi.annotations.Page;
import com.provar.core.testapi.annotations.TestLogger;
import com.provar.core.testapi.annotations.TextType;

@Page( title="MyGardPandIRenewal"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
   
public class MyGardPandIRenewal {

	 WebDriver driver;
	@TestLogger
	public Logger testLogger;
	 
     public MyGardPandIRenewal(WebDriver driver){
     this.driver=driver;
     }

	public void actionsClick(String elementXpath, String elementName) {

		testLogger.info("Working with " + elementName);
		WebElement e = driver.findElement(By.xpath(elementXpath));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",e);
		js.executeScript("arguments[0].style.border='3px solid red'",e);
		
		Actions actions = new Actions(driver);
		actions.moveToElement(e).click().build().perform();

	}

	@LinkType()
	@FindBy(linkText = "P&I RENEWAL")
	public WebElement P_IRenewalTab;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Renewal updates']")
	public WebElement renewalUpdates;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Claims review']")
	public WebElement claimsReview;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Portfolio reports']")
	public WebElement portfolioReports;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='General increase']")
	public WebElement generalIncrease;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Blue cards and MLCs']")
	public WebElement blueCradTab;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Certificates of entry']")
	public WebElement certTab;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Object']/following::button[1]")
	public WebElement ObjectDD;
	@LinkType()
	@FindBy(xpath = "(//label[text()='Object']/following::ul/div/descendant::li[1]/a/label)[1]")
	public WebElement Object_CheckAll_option;
	@FindBy(xpath = "//div/div/input[@value='Go' and @type='button']")
	@ButtonType()
	public WebElement GoBtn;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Year']/following::button[1]")
	public WebElement YearDD;
	@LinkType()
	@FindBy(xpath = "(//label[text()='Year']/following::ul/div/descendant::li[1]/a/label[1])[1]")
	public WebElement CheckAllDD;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim Type']/following::button[1]")
	public WebElement ClaimType_ClaimReview;
	@LinkType()
	@FindBy(xpath = "(//label[text()='Claim Type']/following::ul/div/descendant::li[1]/a/label[1])[1]")
	public WebElement Claim_Type_CheckAll;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim Status']/following::button[1]")
	public WebElement Claim_Status_DD;
	@LinkType()
	@FindBy(xpath = "(//label[text()='Claim Status']/following::ul/div/descendant::li[1]/a/label[1])[1]")
	public WebElement Claim_Status_Check_all_option;
	@ButtonType()
	@FindBy(xpath = "//input[@type='button' and @value='Clear']")
	public WebElement Clear;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following::button[1]")
	public WebElement ClientDD;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[2]/a")
	public WebElement filterClient;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Product area']/following::button[1]")
	public WebElement ProductAreaDD;
	@LinkType()
	@FindBy(xpath = "//label[text()='Product area']/following::li[2]/a")
	public WebElement ProductArea_P_IRep;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Agreement type']/following::button[1]")
	public WebElement AgreementTypeDD;
	@LinkType()
	@FindBy(xpath = "//label[text()='Agreement type']/following::li[2]/a")
	public WebElement AgreementType_owners;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover group']/following::button[1]")
	public WebElement CovergrpDD;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Blue Cards and MLCs']")
	public WebElement blueCradTab1;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Type of blue card']/following::li[1]/a/label/input[1]")
	public WebElement TypeasOfBlueCardCheckAll;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Type of blue card']/following::button[1]")
	public WebElement TypeofBlueDD;
	@ButtonType()
	@FindBy(xpath = "(//input[@type='button' and @value='Go'])[2]")
	public WebElement go;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Document type description']/following::button[1]")
	public WebElement DocumentTypeDesc;
	@LinkType()
	@FindBy(xpath = "//label[text()='Document type description']/following::li[1]/a/label/input")
	public WebElement DocuTypeCheckAll;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('div.claim-filter-sections.filter.clear.clear-btn-style > input')")
	public WebElement clear;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('input.tab-filter-btn')")
	public WebElement goJS;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover group']/following::a[2]")
	public WebElement CoverGrpSelection;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Renewal request']")
	public WebElement renewalRequest;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New MLC Certificate']")
	public WebElement newMLCCertificate;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('span:nth-child(4) > div.form-submit-section > input:nth-child(2)')")
	public WebElement cancel;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='Renewal of Blue Card']")
	public WebElement renewal_bluecard_link;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('div.popup-form-container > div > div.form-submit-section > input:nth-child(2)')")
	public WebElement RenewalofBlueCardCancel;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Submit']")
	public WebElement Report_Submit;
	@TextType()
	@FindBy(xpath = "//table[@id='test']/tbody/tr[1]/td[6]/span[1]")
	public WebElement GardclaimReference;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='View details']")
	public WebElement GardclaimReferenceViewDetails;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='My portfolio report']")
	public WebElement PortfolioReportTab;
	@TextType()
	@FindBy(xpath = "//th[1]/div/input[@type='checkbox']")
	public WebElement SelectAllDocument;
	@ButtonType()
	@FindBy(xpath = "//input[@name='Download']")
	public WebElement downloadSelectedFiles;
	@TextType()
	@FindBy(xpath = "//div[@id='DocumentnameIcon']")
	public WebElement DocumentName;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Policy year']")
	public WebElement Policy_Year;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Document type description']")
	public WebElement Document_type_desc;
	@ButtonType()
	@FindBy(xpath = "//span[4]/div/div/input[1]")
	public WebElement Export_to_Excel;
	@TextType()
	@FindBy(xpath = "//div[@id='ObjectIcon']")
	public WebElement ObjectHeader;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Valid from']")
	public WebElement ValidFrom;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Last updated']")
	public WebElement LastUpdated;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='2022']")
	public WebElement PolicyYear_BlueCardMLCS;
	@ButtonType()
	@FindBy(xpath = "//span/span[normalize-space(.)='Renewal of Blue Card will not be available until November']//div/input")
	public WebElement ok;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_10_container']//span[normalize-space(.)='Check all']")
	public WebElement CheckAllBtn;
			
}
