package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="ManningAgentDetails"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class ManningAgentDetails {

	@ButtonType()
	@FindBy(css = "#topButtonRow input[type='button'][name='create_manning_company']")
	public WebElement CreateManningCompanyBtn;
	@ButtonType()
	@FindBy(xpath = "//h2[text()='Manning agent detail Detail']/ancestor::td[1]/following-sibling::td[1]/descendant::input[3]")
	public WebElement PEMEManningAgentDetailsEdit;
	@ButtonType()
	@FindBy(xpath = "//h2[text()='Manning agent detail Detail']/ancestor::td[1]/following-sibling::td[1]/descendant::input[1]")
	public WebElement ManningAgentDetailEditBtn;
			
}
