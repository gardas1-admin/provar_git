package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="PEME Exam Approve Reject"                                
               , summary=""
               , page="PEMEExamApproveReject"
               , namespacePrefix=""
               , object="PEME_Exam_Detail__c"
               , connection="MyGardRegressionStaging"
     )             
public class PEMEExamApproveReject {

	@TextType()
	@VisualforceBy(componentXPath = "apex:inputTextarea[@id='apexComments']")
	public WebElement Approve_RejectComments;
	@ButtonType()
	@VisualforceBy(componentXPath = "apex:commandButton[@id='rejectAll']")
	public WebElement Reject;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Approve']")
	public WebElement ApproveBtn;
	
}
