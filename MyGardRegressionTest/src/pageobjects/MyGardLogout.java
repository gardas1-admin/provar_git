package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardLogout"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class MyGardLogout {

	@TextType()
	@FindBy(xpath = "//div[@title='My account']")
	public WebElement MyAccountIcon;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class, \"my-account-link-holder\")]/descendant::a[text()='Logout']")
	public WebElement MyGardlogout;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='User name:']/following-sibling::input")
	public WebElement ReLoginUserName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Password:']/following-sibling::input")
	public WebElement ReLoginPassword;
			
}
