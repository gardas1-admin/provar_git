package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardClaimDetails"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegression"
     )             
public class MyGardClaimDetails {

	@TextType()
	@FindBy(xpath = "//label[text()='Gard reference no.:']")
	public WebElement GardReferenceNo;
	@TextType()
	@FindBy(xpath = "//label[text()='Gard reference no.:']/following-sibling::div[1]")
	public WebElement AllClaimsGardReference;
	@TextType()
	@FindBy(xpath = "//div[text()='Restricted due to privacy regulations']")
	public WebElement ClaimDescriptionMessage;
	@TextType()
	@FindBy(xpath = "//div[text()='Claim details']/following-sibling::span[1]")
	public WebElement ClaimDetailsMarkAsFavorite;
	@LinkType()
	@FindBy(xpath = "//span[text()='Reserve:']/ancestor::label[1]/following-sibling::div[1]/descendant::span[1]")
	public WebElement ClaimReserveAmt;
	@LinkType()
	@FindBy(xpath = "//span[text()='Paid:']/ancestor::label[1]/following-sibling::div[1]/descendant::span[1]")
	public WebElement ClaimPaidAmt;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Payment history']")
	public WebElement PaymentHistoryTab;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Reserve history']")
	public WebElement ReserveHistoryTab;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Sub claim - Equip…']")
	public WebElement subClaimEquipTab;
	@LinkType()
	@FindBy(xpath = "//a[@alt='Sub claim - Equipment, other']")
	public WebElement subClaimEquipTab1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Documents']")
	public WebElement ClaimDetail_documentsTab;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Manage claim']")
	public WebElement ManageClaim;
	@TextType()
	@FindBy(xpath = "//div[@id='commentOnClaim']")
	public WebElement ClaimDetailsCommentOnClaim;
	@TextType()
	@FindBy(xpath = "//div[@id='commentOnClaim']//textarea")
	public WebElement ClaimComment;
	@ButtonType()
	@FindBy(xpath = "//div[@id='commentOnClaim']/div/div/div/input[1]")
	public WebElement submit;
	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div[1]")
	public WebElement SuccessMessageOnComment;
	@ButtonType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div/input[1]")
	public WebElement okButton;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_6_container']/div[2]/div/a[1]")
	public WebElement Export_the_Claim_detail_Page;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_5_container']/span[2]//div/a[1]")
	public WebElement Export_to_Excel_Payment_history_t;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_5_container']/span[2]//div/a[1]")
	public WebElement Export_to_Excel_Reverse_history_t;
	@LinkType()
	@FindBy(xpath = "//div[2]/div/a[1]")
	public WebElement Export_to_Excel_Sub_Claim_Page;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Object name:']")
	public WebElement ClaimDetail_ObjectName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim status:']")
	public WebElement ClaimDetail_ClaimStatus;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Cover type:']")
	public WebElement ClaimDetails_CoverType;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Client reference no.:']")
	public WebElement ClaimDetails_ClientRefNo;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claims handler on Customer side:']")
	public WebElement ClaimDetails_ClaimsHandler_Heading;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim description:']")
	public WebElement ClaimDetail_ClaimDesc;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Event:']")
	public WebElement ClaimDetails_Event;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim type:']")
	public WebElement ClaimDetails_ClaimType;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Reserve $ 100%:']")
	public WebElement ClaimDetail_Reverse$;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Event date:']")
	public WebElement ClaimDetail_EventDate;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim detail:']")
	public WebElement ClaimDetail_ClaimDetail;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Paid $ 100%:']")
	public WebElement Paid$;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Event details:']")
	public WebElement ClaimDetails_EventDetails;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim registered on:']")
	public WebElement ClaimDetails_ClaimRegNo;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Total $ 100%:']")
	public WebElement ClaimDetail_Total;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Event location:']")
	public WebElement ClaimDetails_EventLocation;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claims lead:']")
	public WebElement ClaimDetail_ClaimLead;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Gard share:']")
	public WebElement ClaimDetail_GardShare;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Reserve $ (100.00%):']")
	public WebElement ClaimDetailReverse$;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Paid $ (100.00%):']")
	public WebElement ClaimDetail_Paid$;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Total $ (100.00%):']")
	public WebElement ClaimDetail_Total1;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Your claims handler']")
	public WebElement ClaimDetailsInfos_YourClaimHandler;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Your claims adjuster']")
	public WebElement ClaimDetail_ClaimAdjuster;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claims handler on Customer side']")
	public WebElement ClaimHandlerOnCustSide;
	@TextType()
	@FindBy(xpath = "//th[1]/div[1]")
	public WebElement Date_Heading;
	@TextType()
	@FindBy(xpath = "//th[2]/div[1]")
	public WebElement PaymentHistory_ExpenseCode;
	@TextType()
	@FindBy(xpath = "//th[3]/div[1]")
	public WebElement PaymentHistory_Name;
	@TextType()
	@FindBy(xpath = "//th[4]/div[1]")
	public WebElement PaymentHistory_Currency;
	@TextType()
	@FindBy(xpath = "//th[5]/div[1]")
	public WebElement PaymentHistory_Amount;
	@TextType()
	@FindBy(xpath = "//th[6]/div[1]")
	public WebElement PaymentHistory_Total;
	@TextType()
	@FindBy(xpath = "//th[7]/div[1]")
	public WebElement sub_claim_data_Status;
	@TextType()
	@FindBy(xpath = "//th[8]/div[1]")
	public WebElement sub_claim_data_ClaimDesc;
			
}
