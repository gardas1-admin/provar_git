package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCCompaniesHome"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCCompaniesHome {

	@LinkType()
	@FindBy(xpath = "//a[text()='Bergvall Marine AS']")
	public WebElement CompanyName_BergvallMarineAS;
	@LinkType()
	@FindBy(xpath = "//a[text()='United Insurance Brokers Ltd']")
	public WebElement CompanyName_UnitedInsuranceBrokers;
	@LinkType()
	@FindBy(xpath = "//a[text()='Seatankers Management Co. Ltd.']")
	public WebElement CompanyName_SeatankersManagement;
			
}
