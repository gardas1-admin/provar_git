package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardNews"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardNews {

	@TextType()
	@FindBy(xpath = "//h1[text()='Company news']")
	public WebElement CompanyNewsField;
			
}
