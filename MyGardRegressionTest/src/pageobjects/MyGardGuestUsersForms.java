package pageobjects;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardGuestUsersForms"                                
     , summary=""
     , relativeUrl=""
     , connection="QABoxEntryformForShipOwner"
     )             
public class MyGardGuestUsersForms {

	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Client:']/div/input")
	public WebElement GuestclientName;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Pick a date']/input")
	public WebElement ship_owner_att_risk_date;
	@TextType()
	@JavascriptBy(jspath = "return document.querySelector('td.dp_current')")
	public WebElement TodayDate;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Time in GMT:']/following-sibling::div//button")
	public WebElement TimeInGMT;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='23:00 GMT']")
	public WebElement _2300GMT;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Object name:']/following-sibling::input")
	public WebElement ship_owner_obj_name;
	@TextType()
	@FindBy(xpath = "//div[@id='guest_user_imo']/div/input")
	public WebElement ship_owner_imo;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Flag:']/following-sibling::div//button")
	public WebElement Flag;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Aland']")
	public WebElement Flag_Aland;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Port of registry:']/following-sibling::div//button")
	public WebElement PortofRegistry;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Aalborg']")
	public WebElement Port_Aalborg;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Call sign:']/following-sibling::input")
	public WebElement ship_owner_call_sign;
	@TextType()
	@FindBy(xpath = "//div/div/span/div/input")
	public WebElement ship_owner_gross_tonnage;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Classification:']/following-sibling::div//button")
	public WebElement Classification;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='American Bureau']")
	public WebElement AmericanBureau_Classification;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('input.form-btn.next-nav')")
	public WebElement next;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)=concat('Registered owner',\"'\",'s name:')]/following-sibling::textarea")
	public WebElement ship_owner_registered_owner;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)=concat('Registered owner',\"'\",'s address:')]/following-sibling::textarea")
	public WebElement ship_owner_registered_owner_address;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)=concat('Registered owner',\"'\",'s national registration no:')]/following-sibling::textarea")
	public WebElement ship_owner_registered_owner_natRegNo;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Mortgagee(s), name and address:']/following-sibling::textarea")
	public WebElement Mortgagees_name_and_address;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Same as registered owner']/../input[1]")
	public WebElement SameAsRegisteredOwnerCheckbox;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Name to appear on premium invoice:']/following-sibling::input")
	public WebElement ship_owner_name_premium_invoice;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='VAT not applicable']/../input[@type='checkbox']")
	public WebElement VATnotApplicableCheckBox;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='VAT – Name and address of the operating company:']/following-sibling::textarea")
	public WebElement ship_owner_vat_name;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Registered for VAT – VAT number:']/following-sibling::input")
	public WebElement RegisteredForVATNumber;
	@ButtonType()
	@FindBy(xpath = "//div[@id='country_code']//button")
	public WebElement Country;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='IN - India']")
	public WebElement Country_iNIndia;
	@TextType()
	@FindBy(xpath = "//td[1]/input")
	public WebElement assuredMemberAndCoAssuredsName;
	@ButtonType()
	@FindBy(xpath = "//div[@id='assured-country']//button")
	public WebElement Country_AssuredDetails;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='India']")
	public WebElement Country_indiaAssuredDetails;
	@TextType()
	@FindBy(xpath = "//td[4]/input")
	public WebElement city_AssuredDetails;
	@ButtonType()
	@FindBy(xpath = "//div[@id='capacity']//button")
	public WebElement Capacity_AssuredDetails;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Beneficial Owners']")
	public WebElement beneficialOwners_AssuredDetails;
	@TextType()
	@FindBy(xpath = "//td[7]/input")
	public WebElement natRegNo_AssuredDetails;
	@TextType()
	@FindBy(xpath = "//td//label/../input[@type='checkbox']")
	public WebElement TickOff_AssuredDetails;
	@LinkType()
	@FindBy(xpath = "//td/a")
	public WebElement action_AssuredDetails;
	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[1]/input")
	public WebElement assuredMemberAndCoAssuredsName1;
	@ButtonType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]//button[normalize-space(.)='Assured']")
	public WebElement MemberType_AssuredDetails;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_5_container']//a[normalize-space(.)='Co-Assured']")
	public WebElement coAssured_AssuredDetails;
	@ButtonType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]//div[@id='assured-country']//button")
	public WebElement Country_AssuredDetails2;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_6_container']//a[normalize-space(.)='Albania']")
	public WebElement Country_albania_AssuredDetails;
	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[4]/input")
	public WebElement city_AssuredDetails1;
	@ButtonType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]//div[@id='capacity']//button")
	public WebElement Capacity_AssuredDetails2;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_7_container']//a[normalize-space(.)='Other']")
	public WebElement Capacity_otherAssuredDetails;
	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[6]/input")
	public WebElement capacityOther_AssuredDetails;
	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[7]/input")
	public WebElement natRegNo_AssuredDetails1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='I hereby confirm that the information provided is correct to the best of my knowledge.']/../input[@type='checkbox']")
	public WebElement frmRev;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Afghanistan']")
	public WebElement Country_Submission;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Name']/following-sibling::input")
	public WebElement ship_owner_gross_tonnage1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Email']/following-sibling::input")
	public WebElement Email;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('div.right-button-set > input')")
	public WebElement submit;
	@ButtonType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div/input")
	public WebElement ok;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='05:00 GMT']")
	public WebElement _0500GMT;
	@TextType()
	@FindBy(xpath = "//label[text()=' Gross tonnage:']/following::input[1]")
	public WebElement ship_owner_gross_tonnage2;
	@ButtonType()
	@FindBy(xpath = "//span[text()='Country code:']/following::button[1]")
	public WebElement Countrycode;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_4_container']//a[normalize-space(.)='Other']")
	public WebElement other_AssuredDetails;
	@TextType()
	@FindBy(xpath = "//td[6]/input")
	public WebElement capacityOther;
	@TextType()
	@JavascriptBy(jspath = "return document.querySelector('td:nth-child(8) > div > label')")
	public WebElement TickOff_AssuredDetails1;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Assured name should not be empty.']")
	public WebElement AssuredNameshldNotbeEmpty;
	@LinkType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[9]/a[1]")
	public WebElement action_AssuredDetails1;
			
}
