package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardObjectDetails"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardObjectDetails {

	@TextType()
	@FindBy(xpath = "//div[text()='Object details']")
	public WebElement Object_Details;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(text())='Covers on risk']")
	public WebElement CoversOnRiskTab;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Underwriter')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::span[1]")
	public WebElement UnderwriterOnObjectDetails;
	@TextType()
	@FindBy(xpath = "//div[@alt='Land line']")
	public WebElement UnderwriterLandline;
	@TextType()
	@FindBy(xpath = "//div[@alt='Mobile']")
	public WebElement UnderwriterMobile;
	@TextType()
	@FindBy(xpath = "//div[@alt='Email']")
	public WebElement UnderwriterEmail;
	@TextType()
	@FindBy(xpath = "//div[@alt='Location']")
	public WebElement UnderwriterLocation;
	@LinkType()
	@FindBy(xpath = "//a[contains(@class, \"vcard-download\")]")
	public WebElement UnderwriterVCard;
	@LinkType()
	@FindBy(xpath = "//div[text()='Object details']/ancestor::div[1]/following-sibling::div[1]/descendant::a[text()='Open claims']")
	public WebElement OpenClaimsTab;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Gard reference')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement GardReferenceLink;
	@LinkType()
	@FindBy(xpath = "//a[text()='MY PORTFOLIO']")
	public WebElement MyPortfolioTab;
	@TextType()
	@FindBy(xpath = "//div[text()='Object details']/following-sibling::span[1]")
	public WebElement ObjectDetailsMarkAsFavorite;
	@LinkType()
	@FindBy(xpath = "//div[text()='Object details']/ancestor::div[1]/following-sibling::div[1]/descendant::a[1]")
	public WebElement NewClaimObjectDetails;
	@TextType()
	@FindBy(xpath = "//label[text()='My claim reference:']/following-sibling::input[1]")
	public WebElement NewClaimMyclaimRf;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Claim type:')]/following-sibling::div[1]")
	public WebElement NewClaimClaimType;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Claim type:')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewClaimclaimsearch;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Claim type:')]/following-sibling::div[1]/descendant::span[8]")
	public WebElement NewClaim_ClaimTypeSearchedClaimType;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim details:']/following::button[1]")
	public WebElement NewClaimclmDetail;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Claim details:')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewClaimclaimdetailsearch;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Claim details:')]/following-sibling::div[1]/descendant::span[8]")
	public WebElement NewClaimclaimDetailsSearchedClaimDetail;
	@ButtonType()
	@FindBy(xpath = "//div[text()='For urgent matters, please contact our ']/preceding-sibling::div[1]/descendant::input[1]")
	public WebElement NewClaimSaveBtn;
	@ButtonType()
	@FindBy(xpath = "//div[text()='For urgent matters, please contact our ']/preceding-sibling::div[1]/descendant::input[2]")
	public WebElement NewClaimCancelBtn;
	@ButtonType()
	@FindBy(xpath = "//div[text()='All unsaved information would be lost.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewClaimCancelContinuePopUp;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search by object name / IMO']")
	public WebElement ReportNewClaimSearchClaimText;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Cargo']")
	public WebElement clmType;
			
}
