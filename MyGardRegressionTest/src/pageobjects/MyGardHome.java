package pageobjects;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import com.provar.core.model.ui.api.UiFacet;
import com.provar.core.testapi.annotations.*;


@Page( title="MyGardHome"                                
, relativeUrl="https://2017r2dev-mygard.cs8.force.com/mygard/HomePage"
, connection="MyGardRegressionStaging"
		)             
public class MyGardHome {

	WebDriver driver;

	@TestLogger
	public Logger testLogger;

	public MyGardHome(WebDriver driver) {

		this.driver = driver;
	}

	public void actionsClick(String elementXpath, String elementName) {

		testLogger.info("Working with " + elementName);
		WebElement e = driver.findElement(By.xpath(elementXpath));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",e);
		js.executeScript("arguments[0].style.border='3px solid red'",e);

		Actions actions = new Actions(driver);
		actions.moveToElement(e).click().build().perform();

	}
	public void selectDropdownUsingList(String WebElements, String ExpectedValue){

		List<WebElement> Options=driver.findElements(By.xpath(WebElements));
		for(WebElement Option: Options){
			System.out.println(Option.getText());
			if(Option.getText().equalsIgnoreCase(ExpectedValue)){
				Option.click();
				break;
			}

		}
	}

	public void JSClick(String WebElements){
	
		WebElement element = driver.findElement(By.xpath(WebElements));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"claim-updates\")]")
	public WebElement MyGardClaimsUpdatesTab;
	@LinkType()
	@FindBy(xpath = ".//a[text()='View all covers']")
	public WebElement MyGardViewAllCoversLink;
	@LinkType()
	@FindBy(xpath = "//a[text()='HOME']")
	public WebElement MyGardHomeTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Company News']")
	public WebElement MyGardCompanyNews;
	@TextType()
	@FindBy(xpath = "//div[@title='My account']")
	public WebElement MyAccountButton;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class, \"my-account-link-holder\")]/descendant::a[text()='Logout']")
	public WebElement MyGardLogout;
	@LinkType()
	@FindBy(xpath = "//a[text()='Products']")
	public WebElement FooterProductsLink;
	@LinkType()
	@FindBy(xpath = "//a[text()='Publications']")
	public WebElement FooterPublications;
	@LinkType()
	@FindBy(xpath = "//a[text()='Publications']")
	public WebElement updates;
	@LinkType()
	@FindBy(xpath = "//a[text()='Updates']")
	public WebElement publications;
	@TextType()
	@FindBy(xpath = "//h1[text()='Publications']")
	public WebElement Publications;
	@LinkType()
	@FindBy(xpath = "//a[text()='Contact']")
	public WebElement FooterContact;
	@LinkType()
	@FindBy(xpath = "//a[text()='Terms of use']")
	public WebElement FooterTermsOfUse;
	@TextType()
	@FindBy(xpath = "//div[text()='TERMS OF USE - MyGard']")
	public WebElement TERMSOFUSE;
	@LinkType()
	@FindBy(xpath = "//a[text()='Services']")
	public WebElement FooterServices;
	@LinkType()
	@FindBy(xpath = "//a[text()='Topics']")
	public WebElement FooterTopics;
	@LinkType()
	@FindBy(xpath = "//a[text()='Topics']")
	public WebElement FooterTopics1;
	@LinkType()
	@FindBy(xpath = "//a[text()='Browser requirements']")
	public WebElement FooterBrowserRequirements;
	@TextType()
	@FindBy(xpath = "//div[text()='Browser requirements']")
	public WebElement BrowserRequirementPopUp;
	@TextType()
	@FindBy(xpath = "//div[@class='popup-close']")
	public WebElement BrowserReqCloseOpoup;
	@LinkType()
	@FindBy(xpath = "//a[text()='Browser requirements']/ancestor::span/following-sibling::span[1]/descendant::a[text()='MyGard feedback']")
	public WebElement FooterMyGardFeedback;
	@ButtonType()
	@FindBy(xpath = "//a[text()='Availability']/ancestor::li[1]")
	public WebElement DropdownlistAvailability;
	@LinkType()
	@FindBy(xpath = "//a[text()='Business opportunity']")
	public WebElement DropdownlistBusinessOpportunity;
	@LinkType()
	@FindBy(xpath = "//a[text()='Communication']")
	public WebElement DropdownlistCommunication;
	@LinkType()
	@FindBy(xpath = "//a[text()='Documentation']")
	public WebElement DropdownlistDocumentation;
	@LinkType()
	@FindBy(xpath = "//a[text()='Fees']")
	public WebElement DropdownlistFees;
	@ButtonType()
	@FindBy(xpath = "//button[@alt='Availability']")
	public WebElement Dropdownlist;
	@LinkType()
	@FindBy(xpath = "//a[text()='Gard event']")
	public WebElement DropdownlistGardevent;
	@LinkType()
	@FindBy(xpath = "//a[text()='MyGard bugs or problems']")
	public WebElement DropdownlistMyGardbugs;
	@LinkType()
	@FindBy(xpath = "//a[text()='MyGard experience']")
	public WebElement DropdownlistMyGardExp;
	@LinkType()
	@FindBy(xpath = "//a[text()='Other']")
	public WebElement DropdownlistOther;
	@LinkType()
	@FindBy(xpath = "//a[text()='Payments']")
	public WebElement DropdownlistPayments;
	@LinkType()
	@FindBy(xpath = "//a[text()='Product/Cover']")
	public WebElement DropdownlistProduct;
	@LinkType()
	@FindBy(linkText = "Response time")
	public WebElement DropdownlistResponseTime;
	@LinkType()
	@FindBy(xpath = "//a[text()='Service']")
	public WebElement DropdownlistServices;
	@ButtonType()
	@FindBy(xpath = "//input[contains(@class, \"popup-close\")]")
	public WebElement FeebackPopUpClose;
	@LinkType()
	@FindBy(xpath = "//a[text()='Privacy policy']")
	public WebElement FooterPrivacyPolicy;
	@TextType()
	@FindBy(xpath = "//div[text()='PRIVACY POLICY']")
	public WebElement PrivacyPolicy;
	@LinkType()
	@FindBy(xpath = "//a[text()='MyGard.support@gard.no']")
	public WebElement MyGardSupportGardNo;
	@LinkType()
	@FindBy(xpath = "//a[contains(@class, \"fb\")]")
	public WebElement FooterFacebookLink;
	@LinkType()
	@FindBy(xpath = "//a[contains(@class, \"ln\")]")
	public WebElement FooterLinkedIn;
	@LinkType()
	@FindBy(xpath = "//a[text()='MY PORTFOLIO']")
	public WebElement MyGardMyPortfolioTab;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'New request')]")
	public WebElement MyGardNewRequest;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to assureds']")
	public WebElement RequestChangeToAssureds;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']")
	public WebElement RequestChangeToAssuredPopUp;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestChangeToAssuredClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement ReqChangeToAssuredClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement clientBlock;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Cover:')]")
	public WebElement RequestChangetoAssuredCover;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestChangeToAssuredObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestChangeToAssuredsContactPreference;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to assureds']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestChangeToAssuredsDateEffective;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestChangeToAssuredsCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'New request')]/following-sibling::div[1]/descendant::div[2]/descendant::a[1]")
	public WebElement RequestChangeToMortgagees;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to mortgagees']")
	public WebElement RequestChangeToMortgagees1;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to mortgagees']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestChangeToMortgageesClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to mortgagees']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Cover:')]")
	public WebElement RequestChangeToMortgageesCover;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to mortgagees']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestChangeToMortgageesObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to mortgagees']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestChangeToMortgageesDateEffective;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestChangeToMortgageesCloseBtn;
	@TextType()
	@FindBy(xpath = "//a[text()='Change to terms']")
	public WebElement RequestChangeToTerms1;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to terms']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestChangeToTermsClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to terms']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Cover:')]")
	public WebElement RequestChangeToTermsCover;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to terms']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestChangeToTermsObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to terms']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestChangeToTermsContactPreference;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to terms']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestChangeToTermsDateEffective;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestChangeToTermsCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Miscellaneous changes']")
	public WebElement RequestMiscellaneousChanges;
	@TextType()
	@FindBy(xpath = "//span[text()='Request miscellaneous changes']")
	public WebElement RequestMiscellaneousChanges1;
	@TextType()
	@FindBy(xpath = "//span[text()='Request miscellaneous changes']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestMiscellaneousChangesClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request miscellaneous changes']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Cover:')]")
	public WebElement RequestMiscellaneousChangesCover;
	@TextType()
	@FindBy(xpath = "//span[text()='Request miscellaneous changes']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestMiscellaneousChangesObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request miscellaneous changes']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestMiscellaneousChangesDateEffective;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestMiscellaneousChangescloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Covers for new objects']")
	public WebElement RequestCoversForNewObjects;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']")
	public WebElement RequestCoversForNewObjectsPopUp;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestCoversForNewObjectsClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Product area:')]")
	public WebElement RequestCoversForNewObjectsProductArea;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Cover:')]")
	public WebElement RequestCoversForNewObjectsCover;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestCoversForNewObjectsObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestCoversForNewObjectsContactPreference;
	@TextType()
	@FindBy(xpath = "//span[text()='Request covers for new objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestCoversForNewObjectsDateEffective;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestCoversFOrNewObjectsCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Additional covers for existing objects']")
	public WebElement RequestAdditionalCoversForExistingObjects;
	@TextType()
	@FindBy(xpath = "//span[text()='Request additional covers for existing objects']")
	public WebElement RequestAdditionalCoversForExistingObjectsPopUp;
	@TextType()
	@FindBy(xpath = "//span[text()='Request additional covers for existing objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestAdditionalCoversForExistingObjectsClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request additional covers for existing objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Product area:')]")
	public WebElement RequestAdditionalCoversForExistingObjectsProductArea;
	@TextType()
	@FindBy(xpath = "//span[text()='Request additional covers for existing objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestAdditionalCoversForExistingObjectsObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request additional covers for existing objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestAdditionalCoversForExistingObjectsContactPreference;
	@TextType()
	@FindBy(xpath = "//span[text()='Request additional covers for existing objects']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestAdditionalCoversForExistingObjectsDateEffective;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestAdditionalCoversForExistingObjectsCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Blue Card for new object']")
	public WebElement RequestBlueCardForNewObject;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']")
	public WebElement RequestBlueCardForNewObjectPopUp;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestBlueCardforNewObjectClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestBlueCardForNewObjectObjName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestBlueCardForNewObjectDateEffective;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestBlueCardForNewObjectContactPreferance;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Type of Blue Card:')]")
	public WebElement RequestBlueCardForNewObjectTypeOfBlueCard;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Flag:')]")
	public WebElement RequestBlueCardForNewObjectFlag;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Port of registry:')]")
	public WebElement RequestBlueCardForNewObjectPortOfRegistry;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Call sign:')]")
	public WebElement RequestBlueCardForNewObjectCallSign;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Call sign:')]/ancestor::div[1]/following-sibling::div[1]/descendant::label")
	public WebElement RequestBlueCardForNewObjectRegisteredOwnerName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request Blue Card for new object']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Call sign:')]/ancestor::div[1]/following-sibling::div[2]/descendant::label")
	public WebElement RequestBlueCardForNewObjectRegisteredOwnerAddress;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestBlueCardForNewObjectCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Request Blue Card for new object']")
	public WebElement RequestBlueCardForNewObject1;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to the Blue Card']")
	public WebElement RequestChangeToTheBlueCard;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']")
	public WebElement RequestChangeToTheBlueCardPopUp;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client:')]")
	public WebElement RequestChangeToTheBlueCardClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestChangeToTheBlueCardObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Type of Blue Card:')]")
	public WebElement RequestChangeToTheBlueCardTypeOfBlueCard;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestChangeToTheBlueCardDateEffective;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Date effective:')]")
	public WebElement RequestChangeToTheBlueCardDateEffective1;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestChangeToTheBlueCardContactPreference;
	@TextType()
	@FindBy(xpath = "//span[text()='Request change to the Blue Card']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Reason:')]")
	public WebElement RequestChangeToTheBlueCardReason;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestChangeToTheBlueCardCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='New MLC Certificate']")
	public WebElement RequestNewMLCCertificate;
	@TextType()
	@FindBy(xpath = "//span[text()='Request new MLC Certificate']")
	public WebElement RequestNewMLCCertificatePopUp;
	@TextType()
	@FindBy(xpath = "//span[text()='Request new MLC Certificate']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Client :')]")
	public WebElement RequestNewMLCCertificateClient;
	@TextType()
	@FindBy(xpath = "//span[text()='Request new MLC Certificate']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Object name:')]")
	public WebElement RequestNewMLCCertificateObjectName;
	@TextType()
	@FindBy(xpath = "//span[text()='Request new MLC Certificate']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Effective from:')]")
	public WebElement RequestNewMLCCertificateEffectiveFrom;
	@TextType()
	@FindBy(xpath = "//span[text()='Request new MLC Certificate']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Effective to:')]")
	public WebElement RequestNewMLCCertificateEffectiveTo;
	@TextType()
	@FindBy(xpath = "//span[text()='Request new MLC Certificate']/ancestor::div[1]/following-sibling::div[1]/descendant::label[contains(normalize-space(text()),'Contact preference:')]")
	public WebElement RequestNewMLCCertificateContactPreference;
	@LinkType()
	@FindBy(xpath = "//a[@class='popup-close']")
	public WebElement RequestNewMLCCertificateCloseBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Forms']/ancestor::div[1]")
	public WebElement MyPortfolioFormsTab;
	@TextType()
	@FindBy(xpath = "//div[text()='P&I forms']")
	public WebElement P_I_forms;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']")
	public WebElement Application_form_for_Ship_owners;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']")
	public WebElement check_PI;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']")
	public WebElement check_PI1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']")
	public WebElement check_PI2;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[1]")
	public WebElement Ship_Owners_Action_Button;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[1]")
	public WebElement Ship_Owners_Action_Button1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[normalize-space(text())='Action']")
	public WebElement ApplicationShipOwnerAction;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[1]")
	public WebElement ApplicationShipOwnerButton;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[1]/descendant::div[text()='Upload']")
	public WebElement ShipOwnerUploadButton;
	@LinkType()
	@FindBy(linkText = "Download")
	public WebElement ShipOwnerDownloadButton;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[1]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement ShipOwnerFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Application form for Ship owners']/descendant::div[1]")
	public WebElement Ship_Owners_Action_Button2;
	@TextType()
	@FindBy(xpath = "//*[@id=\"MyPortfolioForms:pageFrame:j_id1118:check-PI\"]/div[2]/div[1]/div[1]/label/div")
	public WebElement Ship_Owners_Action_Button_test;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Chartered object'] /descendant::div[normalize-space(text())='Action']")
	public WebElement CharteredObjectButton;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']")
	public WebElement ApplicationFormForShipOwners;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Chartered object']/descendant::div[text()='Upload']/ancestor::div[4]")
	public WebElement check_PI3;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(text())='Action']/ancestor::label[normalize-space(text())='Application form for Ship owners']/descendant::div[1]")
	public WebElement Cle;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::div[1]")
	public WebElement MyPortfolioClient;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Ship owners']/descendant::div[1]")
	public WebElement Ship_Owners_Action_Button3;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::div[1]")
	public WebElement MyPortfolioClientFilter;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::div[1]/descendant::li[2]/descendant::input[1]")
	public WebElement ClientFilterFirstCheckboxSelect;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Clear']/ancestor::div[1]/preceding-sibling::div[1]/descendant::input[@value='Go']")
	public WebElement MyPortfolioGoBtn;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Clear']")
	public WebElement MyPortfolioClearBtn;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover']/ancestor::div[1]/descendant::div[1]")
	public WebElement MyPortfolioCoverFilter;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover']/ancestor::div[1]/descendant::div[1]/descendant::li[2]/descendant::input[1]")
	public WebElement CoverFilterFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Policy year']/ancestor::div[1]/descendant::div[1]")
	public WebElement MyPortfolioPolicyYearFilter;
	@LinkType()
	@FindBy(xpath = "//label[text()='Policy year']/ancestor::div[1]/descendant::div[1]/descendant::li[2]/descendant::input[1]")
	public WebElement PolicyYearFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Product area']/ancestor::div[1]/descendant::div[1]")
	public WebElement MyPortfolioProductAreaChecklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Product area']/ancestor::div[1]/descendant::div[1]/descendant::li[2]/descendant::input[1]")
	public WebElement ProductAreaFirstCheckboxSelect;
	@LinkType()
	@FindBy(xpath = "//div[text()='My portfolio']/following-sibling::div[1]/descendant::a[text()='Objects']")
	public WebElement MyPortfolioObjectsTab;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'View objects')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement NumberInViewObjectsColumn;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Client')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::td[1]")
	public WebElement VerifyClientName;
	@LinkType()
	@FindBy(xpath = "//div[text()='My portfolio']/following-sibling::div[1]/descendant::a[text()='Objects']")
	public WebElement MyPortfolioObjectsTab1;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Object')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement ObjectNameInObjectTab;
	@TextType()
	@FindBy(xpath = "//div[text()='Object details']")
	public WebElement Object_Details;
	@LinkType()
	@FindBy(xpath = "//a[text()='Forms']/ancestor::div[1]/following-sibling::div[normalize-space(.)='Documents' and contains(@class,'draft-claims')]")
	public WebElement PortfolioReportsTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Forms']/ancestor::div[1]/following-sibling::div[1]/a[1]")
	public WebElement PortfolioReports;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::button[1]")
	public WebElement PortfolioReportsClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::input[@placeholder='Search']")
	public WebElement PortfolioReportsClientSearch;
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::input[@placeholder='Search']/ancestor::div[1]/following-sibling::ul[1]//descendant::li[3]/descendant::span[1]")
	@LinkType()
	public WebElement SearchClientFromClientPicklist;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Product area']/following-sibling::div[1] /descendant::button[1]")
	public WebElement PortfolioReportProductAreaPicklist;
	@TextType()
	@FindBy(xpath = "//label[text()='Product area']/following-sibling::div[1]/descendant::input[1]")
	public WebElement ProductAreaSearch;
	@LinkType()
	@FindBy(xpath = "//label[text()='Product area']/following-sibling::div[1]/descendant::span[8]")
	public WebElement SearchedProductAreaIReport;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Agreement type'] /following-sibling::div[1]/descendant::button[1]")
	public WebElement PortfolioReportAgreementTypePicklist;
	@TextType()
	@FindBy(xpath = "//label[text()='Agreement type']/following-sibling::div[1]/descendant::input[@placeholder='Search']")
	public WebElement AgreementTypeSearch;
	@LinkType()
	@FindBy(xpath = "//label[text()='Agreement type']/following-sibling::div[1]/descendant::span[8]")
	public WebElement SearchedAgreementType;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover group'] /following-sibling::div[1]/descendant::button[1]")
	public WebElement PortfolioReportsCoverGroup;
	@TextType()
	@FindBy(xpath = "//label[text()='Cover group']/following-sibling::div[1]/descendant::input[1]")
	public WebElement CoverGroupSearch;
	@LinkType()
	@FindBy(xpath = "//a[text()='Submit']")
	public WebElement PortfolioReportsSubmit;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]")
	public WebElement PortfolioReportsClientPicklist1;
	@ButtonType()
	@FindBy(css = "div.btn-group.bootstrap-select.filter-field.open button.btn.dropdown-toggle.selectpicker.btn-default")
	public WebElement PortfolioReportsClientPicklist2;
	@LinkType()
	@FindBy(xpath = "//a[text()='MY CLAIMS']/following-sibling::span[1]/descendant::a[1]")
	public WebElement PEMETabClient;
	@LinkType()
	@FindBy(xpath = "//a[text()='Renewal of Blue Card']")
	public WebElement RequestRenewalOfBlueCard;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(text())='Renewal of Blue Card will not be available until November']")
	public WebElement BlueCardRenewalAvailabilityMessage;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Ok']")
	public WebElement OkButtonRenewalBlueCardMessage;
	@TextType()
	@FindBy(xpath = "//div[@title='My account']")
	public WebElement MyAccountIconButton;
	@TextType()
	@FindBy(xpath = "//label[text()='Letter of undertaking:']")
	public WebElement RequestRenewalOfBlueCardPopUp;
	@TextType()
	@FindBy(xpath = "//label[text()='Letter of undertaking:']")
	public WebElement LetterofUndertaking;
	@LinkType()
	@FindBy(xpath = "//a[text()='MY CLAIMS']")
	public WebElement MyClaimsTab;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::span[1]")
	public WebElement MyClaimsClientPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::span[1]/descendant::li[2]/descendant::input[1]")
	public WebElement ClientFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Clear']/ancestor::div[1]/preceding-sibling::div[1]/descendant::input[@value='Go']")
	public WebElement MyClaimsGoBtn;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Clear']")
	public WebElement MyClaimsClearBtn;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover']/following-sibling::div[1]/descendant::div[1]")
	public WebElement OpenClaimsCoverPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover']/following-sibling::div[1]/descendant::div[1] /descendant::li[2]/descendant::input[1]")
	public WebElement CoverFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim type']/following-sibling::div[1]/descendant::div[1]")
	public WebElement OpenClaimsClaimTypePicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Claim type']/following-sibling::div[1]/descendant::div[1] /descendant::li[2]/descendant::input[1]")
	public WebElement ClaimTypeFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Year']/following-sibling::div[1]/descendant::div[1]")
	public WebElement OpenClaimYearPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Year']/following-sibling::div[1]/descendant::div[1] /descendant::li[1]/descendant::input[1]")
	public WebElement YearFirstCheckbox;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Gard claim reference')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement GardClaimReference;
	@LinkType()
	@FindBy(xpath = "//div[text()='My claims']/following-sibling::div[1]/descendant::a[text()='All claims']")
	public WebElement AllClaimsSubTab;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::span[1]")
	public WebElement AllClaimsClientPicklist;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover']/following-sibling::div[1]/descendant::div[1]")
	public WebElement AllClaimsCoverPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover']/following-sibling::div[1]/descendant::div[1] /descendant::li[2]/descendant::input[1]")
	public WebElement AllClaimsCoverFirstCheckbox;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover']/following-sibling::div[1]/descendant::div[1] /descendant::li[2]/descendant::input[1]")
	public WebElement AllClaimsGardClaimReference;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Gard claim reference')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement AllClaimGardReferenceLink;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Claims recipient')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::span[1]")
	public WebElement ClaimsRecipientFirst;
	@LinkType()
	@FindBy(xpath = "//div[text()='My claims']/following-sibling::div[1]/descendant::a[text()='Draft claims']")
	public WebElement DraftClaimsSubTab;
	@LinkType()
	@FindBy(xpath = "//div[text()='My claims']/following-sibling::div[1]/descendant::a[text()='Forms']")
	public WebElement MyClaimsFormsSubTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Claim form - Crew']")
	public WebElement ClaimFormCrew;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'First notice - Crew claim')]")
	public WebElement FirstNoticeCrewClaim;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'First notice - Non-crew claim')]")
	public WebElement FirstNoticeNonCrewClaim1;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Free pass agreement')]")
	public WebElement FreePassAgreement;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Stowaway questionnaire - Spanish')]")
	public WebElement StowawayQuestionnaireFrench;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Stowaway questionnaire - Swahili')]")
	public WebElement StowawayQuestionnaireSwahili;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Claim form - ECR family')]")
	public WebElement ClaimFormECRFamily;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'First notice - ECR family')]")
	public WebElement FirstNoticeECRFamily;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'First notice - Stowaway')]")
	public WebElement FirstNoticeStowaway;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Stowaway questionnaire - English')]")
	public WebElement StowawayQuestionnaireEnglish;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Stowaway questionnaire - French')]")
	public WebElement StowawayQuestionnaireSpanish;
	@LinkType()
	@FindBy(xpath = "//div[text()='My claims']/following-sibling::div[1]/descendant::a[1]")
	public WebElement NewClaim;
	@LinkType()
	@FindBy(xpath = "//label[text()='Claim type']/following-sibling::div[1]/descendant::div[1] /descendant::li[3]/descendant::input[1]")
	public WebElement ClaimTypeCrewCheckbox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='On risk']/ancestor::div[1]/descendant::div[1]")
	public WebElement My_Portfolio_On_Risk;
	@LinkType()
	@FindBy(xpath = "//label[text()='On risk']/ancestor::div[1]/descendant::div[1]/descendant::li[2]/descendant::input[1]")
	public WebElement My_Portfolio_OnRisk_Checklist;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Underwriter')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::span[1]")
	public WebElement MyPortfolioCoverUnderwriter;
	@TextType()
	@FindBy(xpath = "//div[@alt='Land line']")
	public WebElement MyPortfolioCoverUnderwriterPhone;
	@TextType()
	@FindBy(xpath = "//div[@alt='Mobile']")
	public WebElement MyPortfolioCoverUnderwriterMobile;
	@LinkType()
	@FindBy(xpath = "//div[@alt='Email']")
	public WebElement MyPortfolioCoverUnderwriterEmail;
	@TextType()
	@FindBy(xpath = "//div[@alt='Location']")
	public WebElement MyPortfolioCoverUnderwriterLocation;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::span[1]")
	public WebElement MyPortfolioObjectsClientPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::span[1]/descendant::li[2]/descendant::input[1]")
	public WebElement MyPortfolioObjectsClientFilter;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Object name']/ancestor::div[1]/descendant::button[1]")
	public WebElement MyPortfolioObjectsObjectNamePicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Object name']/ancestor::div[1]/descendant::button[1]/following-sibling::ul[1]/descendant::input[2]")
	public WebElement MyPortfolioObjectsObjectNameFilterCheckbox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover']/ancestor::div[1]/descendant::button[1]")
	public WebElement MyPortfolioObjectsCoverPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover']/ancestor::div[1]/descendant::button[1]/following-sibling::ul[1]/descendant::input[2]")
	public WebElement MyPortfolioCoverFilterCheckList;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover']/ancestor::div[1]/descendant::button[1]/following-sibling::ul[1]/descendant::input[2]")
	public WebElement MyPortfolioObjectsPolicyYearPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Policy year']/ancestor::div[1]/descendant::button[1]/following-sibling::ul[1]/descendant::input[2]")
	public WebElement MyPortfolioObjectsPolicyYearCheckbox;
	@LinkType()
	@FindBy(xpath = "//label[text()='Policy year']/following::ul[1]/div/div/li[2]/a/span[1]")
	public WebElement CurrentYearPolicyYear;
	@ButtonType()
	@FindBy(xpath = "//label[text()='On risk']/ancestor::div[1]/descendant::button[1]")
	public WebElement MyPortfolioObjectsOnRisk;
	@LinkType()
	@FindBy(xpath = "//label[text()='On risk']/ancestor::div[1]/descendant::button[1]/following-sibling::ul[1]/descendant::input[2]")
	public WebElement MyPortfolioObjectsOnRiskCheckbox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Product area']/ancestor::div[1]/descendant::button[1]")
	public WebElement MyPortfolioObjectsProductAreaPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Product area']/ancestor::div[1]/descendant::button[1]/following-sibling::ul[1]/descendant::input[2]")
	public WebElement MyPortfolioObjectsProductAreaCheckbox;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]")
	public WebElement MyPortfolioPortfolioReportsClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::input[@placeholder='Search']/following-sibling::span[1]")
	public WebElement MyPortfolioReportsClientsLookUpIcon;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::input[@placeholder='Search']/ancestor::div[1]/following-sibling::ul[1]/descendant::span[3]")
	public WebElement SearchedClientFromClientPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Cover group']/following-sibling::div[1]/descendant::span[8]")
	public WebElement SearchedCoverGroup;
	@TextType()
	@FindBy(xpath = "//div[text()='Welcome,']")
	public WebElement WelcomeMessage;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Go']/preceding-sibling::div[1]/descendant::button[1]")
	public WebElement HeaderMultiSelectPicklist;
	@LinkType()
	@FindBy(xpath = "//span[text()='Check all']/../input[1]")
	public WebElement MultiSelectSecondCheckBox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Go']")
	public WebElement HeaderGoBtn;
	@TextType()
	@FindBy(xpath = "//div[@class='search-opener']")
	public WebElement SearchOpenerLookUp;
	@TextType()
	@FindBy(xpath = "//input[@class='site-search-box']")
	public WebElement HeaderSearchText;
	@LinkType()
	@FindBy(xpath = "//a[text()='Gard List of Correspondents']")
	public WebElement GardListOfCorrespondents;
	@LinkType()
	@FindBy(xpath = "//a[text()='Staff news']")
	public WebElement SearchResultStaffNews;
	@TextType()
	@FindBy(xpath = "//h3[text()='Claims']")
	public WebElement SearchedClaimsResults;
	@TextType()
	@FindBy(xpath = "//div[text()='Personal information']")
	public WebElement PersonalInformation;
	@TextType()
	@FindBy(xpath = "//div[text()='Personal information']/ancestor::div[1]/following-sibling::div[1]/descendant::div[text()='Bergvall Marine AS']")
	public WebElement PersonalInformationCompany;
	@TextType()
	@FindBy(xpath = "//div[text()='My subscriptions']")
	public WebElement MySubscriptionSubTab;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Client')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::td[1]")
	public WebElement VerifyClientInClaimsUpdate;
	@LinkType()
	@FindBy(xpath = "//a[text()='My information & subscriptions']/ancestor::div[1]/following-sibling::span[1]/descendant::a[text()='My admin preferences']")
	public WebElement MyAdminPreference;
	@LinkType()
	@FindBy(xpath = "//a[text()='Request new user access']")
	public WebElement RequestNewUserAccess;
	@TextType()
	@FindBy(xpath = "//label[text()='Not in the list?']")
	public WebElement UserAccessNotInTheList;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Submit and new']/preceding-sibling::input[1]")
	public WebElement UserAccesssCancel;
	@LinkType()
	@FindBy(xpath = "//a[text()='My information & subscriptions']/ancestor::div[1]/following-sibling::div[2]/descendant::a[text()='My address book']")
	public WebElement MyAddressBookLink;
	@TextType()
	@FindBy(xpath = "//div[@class='accordion-opener default-accordion-opener']")
	public WebElement ExpandUnderWriter;
	@LinkType()
	@FindBy(xpath = "//a[text()='Client contacts']/ancestor::div[1]")
	public WebElement ClientContactsSubTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Share data with clients']")
	public WebElement ShareDataWithClients;
	@TextType()
	@FindBy(xpath = "//div[text()='Already shared']")
	public WebElement AlreadySharedSubTab;
	@TextType()
	@FindBy(xpath = "//div[text()='Object details']/following-sibling::span[1]")
	public WebElement ObjectDetailsMarkFavoriteIcon;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Object']/following-sibling::div[1]/descendant::div[1]")
	public WebElement OPenClaimsObjectPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Object']/following-sibling::div[1]/descendant::div[1] /descendant::li[2]/descendant::input[1]")
	public WebElement MyClaimObjectFirstCheckBoxSelect;
	@LinkType()
	@FindBy(xpath = "//label[text()='Year']/following-sibling::div[1]/descendant::div[1] /descendant::li[6]/descendant::input[1]")
	public WebElement OpenClaimYearLastCheckBoxSelect;
	@TextType()
	@FindBy(xpath = "//input[@value='Clear']/ancestor::span[1]/following-sibling::div[1]/descendant::span[1]")
	public WebElement MyClaimMarkAsFIlterIcon;
	@TextType()
	@FindBy(xpath = "//label[text()='Enter a filter name:']/following-sibling::input[1]")
	public WebElement SavedFilterPopoup;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Done']")
	public WebElement SavedFilterDoneBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='FAVOURITES']")
	public WebElement FavouritesTab;
	@TextType()
	@FindBy(xpath = "//div[text()='Claims']/ancestor::div[4]/descendant::div[1]")
	public WebElement ExpandClaimsInFavorite;
	@TextType()
	@FindBy(xpath = "//div[text()='Objects']/ancestor::div[4]/descendant::div[1]")
	public WebElement ExpandObjectsInFavorites;
	@TextType()
	@FindBy(xpath = "//div[text()='Saved filters']/ancestor::div[4]/descendant::div[1]")
	public WebElement ExpandSavedFilterInFavorites;
	@LinkType()
	@FindBy(xpath = "//div[text()='Claims']/ancestor::div[1]/following-sibling::div[1]/descendant::div[@title='Remove from favourite']")
	public WebElement MarkClaimsNotFavorite;
	@LinkType()
	@FindBy(xpath = "//div[text()='Objects']/ancestor::div[1]/following-sibling::div[1]/descendant::div[@title='Remove from favourite']")
	public WebElement ObjectNotFavorite;
	@LinkType()
	@FindBy(xpath = "//div[text()='Saved filters']/ancestor::div[1]/following-sibling::div[1]/descendant::div[@title='Remove from favourite']")
	public WebElement SavedFilterRemoveAsFAvorite;
	@TextType()
	@FindBy(xpath = "//*[@title='Contact me']")
	public WebElement HeaderContactMe;
	@LinkType()
	@FindBy(xpath = "//a[text()='Help']")
	public WebElement HeaderHelp;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class, \"emergency-section\")]//a")
	public WebElement HeaderEmergency;
	@LinkType()
	@FindBy(xpath = "//a[text()='Help']")
	public WebElement MyGardHeaderHelp;
	@LinkType()
	@FindBy(xpath = "//a[contains(@class, \"faq-help-icon\")]")
	public WebElement MyGardHeaderHelp1;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"portfolio-forms-category-left-options\")]/div[1]/label/div")
	public WebElement PortfolioFormsShipOwnerBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Members']")
	public WebElement PEMEClinicTab;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Member']/ancestor::div[1]/descendant::button[1]")
	public WebElement PEMECLIENTMemberPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Member']/ancestor::div[1]/descendant::ul[1]/descendant::input[@value='multiselect-all']")
	public WebElement PEMEClientCheckAllCheckBox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Go']")
	public WebElement ClientsGoBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='View details']")
	public WebElement ViewEnrollmentDetails;
	@TextType()
	@FindBy(xpath = "(//label[text()='Company name:']/following-sibling::div[1])[1]")
	public WebElement ManningAgentCompanyName;
	@TextType()
	@FindBy(xpath = "//div[text()='TESTER']")
	public WebElement ManningAgentContactPersonName;
	@TextType()
	@FindBy(xpath = "//span[text()='demo line 1,']")
	public WebElement ManningAgentAddress;
	@LinkType()
	@FindBy(xpath = "//span[text()='Demo line 2,']")
	public WebElement ManningAgentAddress1;
	@TextType()
	@FindBy(xpath = "//span[text()='Norway,']")
	public WebElement ManningAgentAddress2;
	@LinkType()
	@FindBy(xpath = "//a[text()='Manning agents']/ancestor::div[1]/following-sibling::div[1]")
	public WebElement PEMEObjectsSubTab;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Object']/ancestor::div[1]/descendant::button[1]")
	public WebElement ObjectsPicklist;
	@LinkType()
	@FindBy(xpath = "//span[text()='Check all']/ancestor::li[1]/following-sibling::li[1]/descendant::input[1]")
	public WebElement SearchedObjectFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Go']")
	public WebElement ObjectsGoBtn;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Clear']")
	public WebElement ObjectsClearBtn;
	@LinkType()
	@FindBy(xpath = "//span[text()='Check all']/ancestor::li[1]/following-sibling::li[2]/descendant::input[1]")
	public WebElement SearchedObjectSecondCheckBox;
	@LinkType()
	@FindBy(xpath = "//a[text()='Invoices']")
	public WebElement PEMEInvoiceTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='New invoice']")
	public WebElement NewInvoiceBtn;
	@ButtonType()
	@FindBy(xpath = "//label[contains((text()),'Member')]/following-sibling::div[1]")
	public WebElement NewInvoiceClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains((text()),'Member:')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewInvoiceClientSearchText;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Member:')]/following-sibling::div[1]/descendant::div[3]/following-sibling::ul[1]/descendant::li[4]/descendant::a[1]/descendant::span[1]")
	public WebElement NewInvoiceSearchedClient;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Period covered - From:')]/following-sibling::span[1]/descendant::input[1]")
	public WebElement NewInvoicePickADateFrom;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::table[2]/descendant::tr[2]/descendant::td[3]")
	public WebElement NewInvoicePeriodFromCalenderStartDate;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Invoice number:')]/following-sibling::input[1]")
	public WebElement NewInvoiceinvoiceno;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Manning agent:')]/following-sibling::div[1]")
	public WebElement NewInvoiceManningAgentPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Manning agent:')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ManningAgentSearch;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Manning agent:')]/following-sibling::div[1]/descendant::li[2]/descendant::span[1]")
	public WebElement MannningAgentSearched;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Gard PEME reference no.:')]/following-sibling::div[1]")
	public WebElement NewInvoiceGardPEMERefNo;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Gard PEME reference no.:')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewInvoiceSearchedGardPEMERefNoText;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Gard PEME reference no.:')]/following-sibling::div[1]/descendant::li[3]/descendant::span[1]")
	public WebElement NewInvoiceSearchedGardPEMERefNo;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Period covered - To:')]/following-sibling::span[1]/descendant::input[1]")
	public WebElement NewInvoicePickADateTo;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"calendar-container\")]/div[2]/table[2]/tbody/tr[5]/td[3]")
	public WebElement NewInvoicePeriodToCalenderEndDate;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object:')]/following-sibling::div[1]")
	public WebElement NewInvoiceObjectPicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object:')]/following-sibling::div[1]/descendant::li[4]/descendant::span[1]")
	public WebElement NewInvoiceSearcheFirstCheckBox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Next']")
	public WebElement NewInvoiceNextBtn;
	@TextType()
	@FindBy(xpath = "//div[text()='Date']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[1]/descendant::input[1]")
	public WebElement FirstInvoiceDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::table[2]/descendant::tr[3]/descendant::td[5]")
	public WebElement FirstInvoiceSetCalenderDate;
	@TextType()
	@FindBy(xpath = "//div[text()='Examinee']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[2]/descendant::input[1]")
	public WebElement FirstInvoiceExaminee;
	@TextType()
	@FindBy(xpath = "//div[text()='Age']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[3]/descendant::input[1]")
	public WebElement FirstInvoiceAge;
	@TextType()
	@FindBy(xpath = "//div[text()='Rank']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[4]/descendant::input[1]")
	public WebElement FirstInvoiceRank;
	@TextType()
	@FindBy(xpath = "//div[text()='Examination']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[5]/descendant::input[1]")
	public WebElement FirstInvoiceExamination;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Amount (PHP)')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[7]/descendant::input[1]")
	public WebElement FirstInvoiceAmountPHP;
	@LinkType()
	@FindBy(xpath = "//div[text()='Action']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[8]/descendant::a[1]")
	public WebElement AddANewRow;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Examinee']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[1]/descendant::input[1]")
	public WebElement SecondInvoiceDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::table[2]/descendant::tr[3]/descendant::td[5]")
	public WebElement SecondInvoiceCalenderDate;
	@TextType()
	@FindBy(xpath = "//div[text()='Examinee']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[2]/descendant::input[1]")
	public WebElement SecondInvoiceExaminee;
	@TextType()
	@FindBy(xpath = "//div[text()='Age']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[3]/descendant::input[1]")
	public WebElement SecondInvoiceAge;
	@TextType()
	@FindBy(xpath = "//div[text()='Rank']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[4]/descendant::input[1]")
	public WebElement SecondInvoiceRank;
	@TextType()
	@FindBy(xpath = "//div[text()='Examination']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[5]/descendant::input[1]")
	public WebElement SecondInvoiceExamination;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Amount (PHP)')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[7]/descendant::input[1]")
	public WebElement SecondInvoiceAmountPHP;
	@LinkType()
	@FindBy(xpath = "//div[text()='Action']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[8]/descendant::a[2]")
	public WebElement AddANewRow1;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Examinee']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[1]/descendant::input[1]")
	public WebElement ThirdInvoiceDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::table[2]/descendant::tr[3]/descendant::td[6]")
	public WebElement ThirdInvoiceSetCalenderDate;
	@TextType()
	@FindBy(xpath = "//div[text()='Examinee']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[2]/descendant::input[1]")
	public WebElement ThirdInvoiceexaminee;
	@TextType()
	@FindBy(xpath = "//div[text()='Age']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[3]/descendant::input[1]")
	public WebElement ThirdInvoiceAge;
	@TextType()
	@FindBy(xpath = "//div[text()='Rank']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[4]/descendant::input[1]")
	public WebElement ThirdInvoiceRank;
	@TextType()
	@FindBy(xpath = "//div[text()='Examination']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[5]/descendant::input[1]")
	public WebElement ThirdInvoiceExamination;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Amount (PHP)')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[7]/descendant::input[1]")
	public WebElement ThirdInvoiceAmountPHP;
	@TextType()
	@FindBy(xpath = "//label[text()='Use bank details from previous invoice?']")
	public WebElement UseBankDetailsCheckbox;
	@TextType()
	@FindBy(xpath = "//textarea[@class='new-claim-field new-claim-description-field']")
	public WebElement NewInvoiceBankDetails;
	@LinkType()
	@FindBy(xpath = "//textarea[@class='new-claim-field new-claim-description-field']")
	public WebElement NewInvoiceBankDetailsText;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Cancel']/ancestor::div[1]/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewInvoiceSubmit;
	@TextType()
	@FindBy(xpath = "//span[contains(normalize-space(text()),'Thank you for contacting Gard. Your invoice has been successfully submitted. Submit another invoice')]")
	public WebElement InvoiceSubmissionMessage;
	@ButtonType()
	@FindBy(xpath = "//span[contains(normalize-space(text()),'Thank you for contacting Gard. Your invoice has been successfully submitted. Submit another invoice')]/ancestor::div[1]/following-sibling::div[1]/descendant::input[2]")
	public WebElement InvoiceSubmissioNoBtn;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Invoice number')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement InvoiceNumber;
	@LinkType()
	@FindBy(xpath = "//a[text()='Examination details']/ancestor::div[1]")
	public WebElement InvoiceDetailsExaminationDetailsSubTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Bank details']/ancestor::div[1]")
	public WebElement InvoiceDetailsBankDetails;
	@TextType()
	@FindBy(xpath = "//div[text()='Status']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::td[4]")
	public WebElement InvoiceStatus;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Status')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[3]/descendant::td[8]")
	public WebElement RejectedInvoiceStatus;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Status')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[2]/descendant::td[10]/descendant::a[1]")
	public WebElement EditActionIcon;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Amount:')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement EditAmountInEditExamDetailPopUp;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Approval history']/ancestor::div[1]/following-sibling::div[1]/descendant::input[1]")
	public WebElement SubmitBtnInEditExamDetailPopUp;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Status')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::td[6]")
	public WebElement recordData;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Review and submit']/ancestor::div[1]/preceding-sibling::div[1]/descendant::input[2]")
	public WebElement MyPortfolioFormCancel;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Chartered object']")
	public WebElement ApplicationFormForCharteredObjects;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Chartered object']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement CharteredObjectDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Chartered object']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement CharteredObjectsFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Mobile offshore unit']")
	public WebElement MobileOffshoreUnit;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Mobile offshore unit'] /descendant::div[1]")
	public WebElement MobileOffshoreUnitActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Mobile offshore unit']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement MobileOffshoreUnitDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Mobile offshore unit']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement MobileOffshoreUnitFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Offshore object']")
	public WebElement ApplicationFormForOffshoreObjects;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Offshore object'] /descendant::div[1]")
	public WebElement OffshoreObjectActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Offshore object']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement OffshoreObjectDownloadButton;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Offshore object']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement OffshoreObjectFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Free pass agreement'] /descendant::div[1]")
	public WebElement FreePassAgreementActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Free pass agreement']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement FreePassAgreementDownloadLink;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Free pass agreement']")
	public WebElement FreePassAgreementForm;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Comprehensive charterers cover entry form']")
	public WebElement ComprehensiveCharterersCoverEntryForm;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Comprehensive charterers cover entry form'] /descendant::div[1]")
	public WebElement ComprehensiveChartersActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Comprehensive charterers cover entry form']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement ComprehensiveChartersDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Charterers renewal questionnaire']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement CharterersRenewalDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Comprehensive charterers cover entry form']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement ComprehensiveFormFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='ITOPF form for new entries']")
	public WebElement ITOPFformForNewEntries;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='ITOPF form for new entries'] /descendant::div[1]")
	public WebElement ITOPFFormActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='ITOPF form for new entries']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement ITOPFDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Layup form Marine']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement MarineLayupDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='ITOPF form for new entries']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement ITOPFformFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Layup form P&I']")
	public WebElement LayupFormP_I;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Layup form P&I']/descendant::div[contains(normalize-space(text()),'Action')]")
	public WebElement LayupFormActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Layup form P&I']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement LayupFormDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Layup form P&I']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement LayUpFormFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='RI declaration form']")
	public WebElement RIDeclarationForm;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='RI declaration form'] /descendant::div[1]")
	public WebElement RIDeclarationFormActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='RI declaration form']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement RIDeclaationrDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='RI declaration form']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement RIDeclarationFormFillInOnline;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Renewal LOU-LOI for Gard P&I, Shipowners']")
	public WebElement RenewalLOU_LOIForm;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Renewal LOU-LOI for Gard P&I, Shipowners'] /descendant::div[1]")
	public WebElement RenewalLOU_LOIFormActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Renewal LOU-LOI for Gard P&I, Shipowners']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement RenewalLOULOIDownloadLink;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Renewal LOU-LOI for Gard P&I, MOU']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement RenewalLOULOIMOUDownloadLink;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Application form MLC / Undertakings MLC']")
	public WebElement UndertakingMLCForm;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Application form MLC / Undertakings MLC'] /descendant::div[1]")
	public WebElement UndertakingMLCActionBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Application form MLC / Undertakings MLC']/descendant::div[2]/descendant::a[text()='Download']")
	public WebElement UndertakingMLCDownloadLink;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Charterers renewal questionnaire'] /descendant::div[1]")
	public WebElement ChartererRenewalFormActionBtn;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Charterers renewal questionnaire']")
	public WebElement ChartererRenewalForm;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Charterers renewal questionnaire']/descendant::div[2]/descendant::a[2]")
	public WebElement ChartererRenewalFormFillInOnline;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Choose client name')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement ChooseClientName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Choose client name')]/following-sibling::div[1]/descendant::div[3]/descendant::input[1]")
	public WebElement search;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Choose client name')]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement SearchedClientFromChooseClientName;
	@ButtonType()
	@FindBy(xpath = "//div[text()='My address book']/following-sibling::div[1]")
	public WebElement MyAddressBookDropdown;
	@LinkType()
	@FindBy(xpath = "//a[text()='Share data with brokers']")
	public WebElement ShareDataWithBrokers;
	@LinkType()
	@FindBy(xpath = "//a[text()='New share']")
	public WebElement NewShareBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Objects']/ancestor::div[1]/following-sibling::div[1]/descendant::a[1]")
	public WebElement MyPortfolioDocumentTab;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Action')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[text()='Suspended']")
	public WebElement ActionSuspended;
	@ButtonType()
	@FindBy(xpath = "//div[contains((text()),'Are you sure you want to stop sharing this data?')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement StopSharingCancelBtn;
	@LinkType()
	@FindBy(xpath = "//div[contains((text()),'Are you sure you want to stop sharing this data?')]/following-sibling::div[1]/descendant::input[2]")
	public WebElement StopSharingContinueBtn;
	@TextType()
	@FindBy(xpath = "//td[text()='Suspended']")
	public WebElement ShareDataStatusSuspended;
	@TextType()
	@FindBy(xpath = "//a[text()='Resume']")
	public WebElement ResumeActionBtn;
	@ButtonType()
	@FindBy(xpath = "//div[contains((text()),'Are you sure you want to start sharing this data?')]/following-sibling::div[1]/descendant::input[2]")
	public WebElement StartSharingContinueBtn;
	@TextType()
	@FindBy(xpath = "//a[text()='Edit']")
	public WebElement EditActionBtn;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Action')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[text()='Delete']")
	public WebElement ActionDeleteShare;
	@ButtonType()
	@FindBy(xpath = "//div[contains((text()),'Are you sure you want to delete this share?')]/following-sibling::div[1]/descendant::input[2]")
	public WebElement DeleteSharingContinueBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Activity feed']")
	public WebElement ActivityFeed;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::span[1]/descendant::div[2]")
	public WebElement RequestChangeToTermsClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::span[1]/descendant::input[@placeholder='Search']")
	public WebElement RequestChangeToTermsSearchText;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::span[1]/descendant::div[4]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement RequestChangeToTermsSearchedClient;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Cover:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement RequestChangeToTermsCoverPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Cover:')]/following-sibling::div[1]/descendant::input[@placeholder='Search']")
	public WebElement RequestChangeToTermsSearchedText;
	@LinkType()
	@FindBy(xpath = "//*[@id='mCSB_19_container']/descendant::li[3]/descendant::a[1]/descendant::span[1]")
	public WebElement RequestChangeToTermsSearchedCover;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]/descendant::div[2]")
	public WebElement RequestChangeToTermsObjectNamePicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]/descendant::li[2]/descendant::span[1]")
	public WebElement RequestChangeToTermsSearchedObjectName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/ancestor::span[2]/following-sibling::div[1]/descendant::div[3]")
	public WebElement RequestChangeToTermsContactPreference1;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Date effective:')]/following-sibling::span[1]/descendant::button")
	public WebElement RequestChangeToTermsDateEffectivePickDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::td[text()='20']")
	public WebElement PickDateFromCalender;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Submit and new']/preceding-sibling::input[2]")
	public WebElement RequestChangeToTermsSubmitBtn;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Thank you for contacting Gard. Your request has been successfully registered.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonRequestChangeToTermsMessage;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]/descendant::div[2]")
	public WebElement RequestChangeToBlueCardObjectNamePicklist;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]/descendant::li[2]/descendant::span[1]")
	public WebElement RequestChangeToBlueCardObjectNameCheckbox;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Type of Blue Card:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement RequestChangeToTheBlueCardTypeOfBlueCard1;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Type of Blue Card:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement RequestChangeToTheBlueCardTypeOfBlueCardPicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Type of Blue Card:')]/following-sibling::div[1]/descendant::li[2]/descendant::span[1]")
	public WebElement RequestChangeToTheBlueCardTypepfBlueCardCheckbox;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Reason:')]/following-sibling::div[1]/descendant::label[1]")
	public WebElement RequestChangeToTheBlueCardReasonCheckbox;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Date effective:')]/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement RequestChangeToTheBlueCardSubmitBtn;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'New object name:')]/following-sibling::input[1]")
	public WebElement RequestChangeToTheBlueCardNewObjectName;
	@TextType()
	@FindBy(xpath = "//label[text()='Gross tonnage:']/ancestor::div[1]/following-sibling::div[1]/descendant::input[1]")
	public WebElement RequestChangeToTheBlueCardGrosstonnage;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Product area:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement RequestCoverForNewObjectsProductAreaPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Product area:')]/following-sibling::div[1]/descendant::div[3]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement RequestCoversForNewObjectsProductAreaSearchedText;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]")
	public WebElement RequestCoverForNewObjectsObjectName;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Cover:')]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::div[1]/descendant::li[2]/descendant::span[1]")
	public WebElement RequestCoverForNewObjectCoverCheckbox;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]")
	public WebElement RequestCoversForNewObjectsObjectNameText;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'IMO no:')]/following-sibling::input[1]")
	public WebElement RequestCoverForNewObjectsIMOno;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"cover-updates\")]")
	public WebElement MyGardCoversUpdatesTab;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Select company to login']/following-sibling::div[1]/descendant::div[2]")
	public WebElement SelectCompanyToLoginSelectCompanyPopUp;
	@LinkType()
	@FindBy(xpath = "//div[text()='Select company to login']/following-sibling::div[1]/descendant::div[2]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement SelectCompanyToLoginSearchedCompany;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Select company to login']/following-sibling::div[1]/descendant::div[2]/following-sibling::div[1]/descendant::input[1]")
	public WebElement SelectCompanyToLoginContinueBtn;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to mortgagees']")
	public WebElement ChangeToMortgagees1;
	@LinkType()
	@FindBy(xpath = "//div[text()='Gard contact name']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::td[1]/descendant::a[1]")
	public WebElement GardContactName;
	@ButtonType()
	@FindBy(xpath = "//label[contains((text()),'Client')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement SubmittedInvoicesClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains((text()),'Client')]/following-sibling::div[1]/descendant::div[2]/descendant::input[1]")
	public WebElement SubmittedInvoicesClientSearch;
	@LinkType()
	@FindBy(xpath = "//label[contains((text()),'Client')]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[1]/descendant::span[1]")
	public WebElement SubmittedInvoicesCheckBoxCheck;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Choose client name')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement MyPortfolioFormsChooseClientNamePicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Choose client name')]/following-sibling::div[1]/descendant::div[3]/following-sibling::ul[1]/descendant::div[2]/descendant::li[3]/descendant::a[1]/descendant::span[1]")
	public WebElement MyPortfolioFormsChooseNameSearch;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Number of vessels chartered:']/following-sibling::td[1]/descendant::input")
	public WebElement TimeChartersNoOfVesselsChartered;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Number of vessels chartered:']/following-sibling::td[2]/descendant::input")
	public WebElement VoyageChartersNoOfVesselsChartered;
	@LinkType()
	@FindBy(xpath = "//td[normalize-space(text())='Number of vessels chartered:']/following-sibling::td[3]/descendant::a")
	public WebElement NoOfVesselsCharteredRowArrow;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Duration of Charters (average # of days):']/following-sibling::td[1]/descendant::input")
	public WebElement TimeChartersDurationOfCharters;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Duration of Charters (average # of days):']/following-sibling::td[2]/descendant::input")
	public WebElement VoyageChartersDurationOfCharters;
	@LinkType()
	@FindBy(xpath = "//td[normalize-space(text())='Duration of Charters (average # of days):']/following-sibling::td[3]/descendant::a")
	public WebElement DurationOfChartersRowArrow;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Average vessel size (in GT):']/following-sibling::td[1]/descendant::input")
	public WebElement TimeChartersAverageVesselSize;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Average vessel size (in GT):']/following-sibling::td[2]/descendant::input")
	public WebElement VoyageChartersAverageVesselSize;
	@LinkType()
	@FindBy(xpath = "//td[normalize-space(text())='Average vessel size (in GT):']/following-sibling::td[3]/descendant::a")
	public WebElement AverageVesselSizeRowArrow;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Average vessel age (years):']/following-sibling::td[1]/descendant::input")
	public WebElement TimeChartersAverageVesselAge;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(text())='Average vessel age (years):']/following-sibling::td[2]/descendant::input")
	public WebElement VoyageChartersAverageVesselAge;
	@LinkType()
	@FindBy(xpath = "//td[normalize-space(text())='Average vessel age (years):']/following-sibling::td[3]/descendant::a")
	public WebElement AverageVesselAgeRowArrow;
	@TextType()
	@FindBy(xpath = "//td[text()='Geography(please be specific including ports and routes):']/following-sibling::td[1]/descendant::input[1]")
	public WebElement GeographyCurrentInsuranceYear;
	@TextType()
	@FindBy(xpath = "//td[text()='Cargo type(s) and MT volumes:']/following-sibling::td[1]/descendant::input[1]")
	public WebElement CargoCurrentInsuranceYear;
	@TextType()
	@FindBy(xpath = "//td[text()='US trade as a % of total trade:']/following-sibling::td[1]/descendant::input[1]")
	public WebElement USTradeCurrentInsuranceYear;
	@TextType()
	@FindBy(xpath = "//td[text()='Owned Cargo']/following-sibling::td[1]/descendant::input[1]")
	public WebElement OwnedCargoCurrentInsuranceYr;
	@TextType()
	@FindBy(xpath = "//td[text()='Third Party Cargo']/following-sibling::td[1]/descendant::input[1]")
	public WebElement ThirdPartyCargoCurrentInsuranceYr;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Does the Assured(s) trade outside Institute trade']/following-sibling::div[1]/descendant::td[1]/descendant::label[1]")
	public WebElement TradeOutsideLimitsRadioBtn;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Frequency:')]/following-sibling::input[1]")
	public WebElement CharterersRenewalQuestionairreFrequency;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Applicable contract provisions:')]/following-sibling::input[1]")
	public WebElement CharterersRenewalQuestionnaireApplicableContractProvisions;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Geography:')]/following-sibling::input[1]")
	public WebElement CharterersRenewalQuestionairreGeography;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Is the vessel ice-classed for the trade?')]/following-sibling::input[1]")
	public WebElement ChartersRenewalQuestionairreVesselIiceClassed;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Is H&M in place when the vessel is calling in ice?')]/following-sibling::input[1]")
	public WebElement CharterersRenewalQuestionairreIs_HM_in_place;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Is H&M in place when the vessel is calling in ice?')]/ancestor::div[5]/following-sibling::div[1]/descendant::div[2]/descendant::label[2]")
	public WebElement ChartererRenewalQuestionairreDoesTheAssuredPerformSTSTransfer;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Does the Assured(s) perform blending of Cargo?')]/following-sibling::div[1]/descendant::tr[1]/descendant::td[2]/descendant::label[1]")
	public WebElement CharterersRenewalQuestionairreAssuredPerformBlendingOfCargo;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Does the Assured(s) perform landside carriage?')]/ancestor::div[1]/preceding-sibling::div[1]/descendant::td[2]/descendant::label[1]")
	public WebElement CharterersRenewalQuestionairreDoesTheAssuredSOwnOrOperateMarineTerminals;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Does the Assured(s) perform landside carriage?')]/following-sibling::div[1]/descendant::tr[1]/descendant::td[2]/descendant::label[1]")
	public WebElement CharterersRenewalQuestionnairePerformLandsideCarriage;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Is the Assured(s) engaged in landside storage of cargo?')]/following-sibling::div[1]/descendant::tr[1]/descendant::td[2]/descendant::label[1]")
	public WebElement CharterersRenewalQuestionairreLandsideStorageOfCargo;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Does the Assured(s) sell bunkers?')]/following-sibling::div[1]/descendant::tr[1]/descendant::td[2]/descendant::label[1]")
	public WebElement CharterersRenewalQuestionairreAssuredSellBunkers;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Does the Assured(s) sell bunkers?')]/ancestor::div[1]/following-sibling::div[1]/descendant::tr[1]/descendant::td[2]/descendant::label[1]")
	public WebElement CharterersRenewalQuestionairreOtherRelevantInfo;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'I hereby confirm that the information provided is correct to the best of my knowledge.')]")
	public WebElement ChartererRenewalQuestionairreConfirmationCheck;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Place')]/following-sibling::input[1]")
	public WebElement ChatererRenewalQuestionairreViewerSignPlace;
	@TextType()
	@FindBy(xpath = "//div[text()='Personal information']/following-sibling::div[text()='Edit']")
	public WebElement Personal_Information_Edit;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"cont-info-edit-pop\")]/div/div[2]/div/div[1]/div[1]/div[3]/div/div/button")
	public WebElement ChangeYourPersonsalInformation;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopup1:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeYourPersonalInforCheckbox1;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopup1:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[3]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeYourPersonalInfoContactforCheckbox2;
	@ButtonType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopup1:ctry']/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ChangeYourPersonalInfosubmit;
	@TextType()
	@FindBy(xpath = "//div[text()='Your personal information has been successfully updated.']")
	public WebElement PersonalInformationUpdationConfirmation;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Your personal information has been successfully updated.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement PersonalInfoConfirmationOkButton;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"cont-info-edit-pop\")]/div/div[2]/div/div[1]/div[1]/div[3]/div/div/button")
	public WebElement ChangeYourPersonalInfoContactFor;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopup1:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeYourPersonalInforCheckBox1;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopup1:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[3]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeYourPersonalInfoContactForChechbox2;
	@ButtonType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopup1:ctry']/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ChangeYourPersonalInfoSubmit;
	@TextType()
	@FindBy(xpath = "//div[text()='Your personal information has been successfully updated.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement PersonalInformationConfirmationOkBtn;
	@TextType()
	@FindBy(xpath = "//div[contains((text()),'My company')]")
	public WebElement MyCompany_sContact;
	@TextType()
	@FindBy(xpath = "//div[contains((text()),'Berggren')]/preceding-sibling::div[text()='Edit']")
	public WebElement MyCompanyContactEdit;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"cont-info-pop\")]/div/div[2]/div/div[1]/div[1]/div[3]/div/div/button")
	public WebElement ChangeContactInfoContactFor;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeContactInformationContactForCheckbox1;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[3]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeContactInformationContactForCheckbox2;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[4]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeContactInformationContactForCheckBox3;
	@ButtonType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:ctry']/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ChangeContactInfoSubmit;
	@TextType()
	@FindBy(xpath = "//div[text()='Contact information has been successfully updated.']")
	public WebElement ContactInformationUpdationConfirmationMessage;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Contact information has been successfully updated.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement ContactInformationUpdationMessageOkButton;
	@TextType()
	@FindBy(xpath = "//div[text()='Anders Mjaaland']/preceding-sibling::div[1]")
	public WebElement My_Company_Contact_Edit_icon;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"cont-info-pop\")]/div/div[2]/div/div[1]/div[1]/div[3]/div/div/button")
	public WebElement ChangeContactInfoContactFor1;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeContactInforContactForCeckbox1;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[3]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeContactInfoContactForCheckbox2;
	@LinkType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:title']/ancestor::div[1]/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[4]/descendant::a[1]/descendant::label[1]/descendant::span[1]")
	public WebElement ChangeContactInfoContactForCheckbox3;
	@ButtonType()
	@FindBy(xpath = "//input[@id='myAccount:pageFrame:customPopupEdit:ctry']/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ChangeContactInfoSubmit1;
	@TextType()
	@FindBy(xpath = "//div[text()='Contact information has been successfully updated.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement ContactInformationUpdationOkBtn;
	@ButtonType()
	@FindBy(xpath = "//div[contains((text()),'Thank you for contacting Gard')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonCharterersRenewalQuestionnaire;
	@TextType()
	@FindBy(xpath = "//div[text()='aritra.mukherjee']/following-sibling::div[1]/descendant::li[text()='Underwriter (main Contact)']")
	public WebElement Underwriter_main_Contact_;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(text())='Client:']/following-sibling::span[1]/descendant::div[2]")
	public WebElement BlueCardForNewObjectClientPicklist;
	@LinkType()
	@FindBy(xpath = "//td[text()='Anders Mjaaland1']/following-sibling::td[2]/descendant::a[@class='role-change-icon']")
	public WebElement MyAdminPreferenceAdminUser;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Welcome,']/ancestor::div[4]/following-sibling::div[1]/descendant::div[4]/following-sibling::div[1]/descendant::input[1]")
	public WebElement MyAdminPreferenceConfirmBtn;
	@TextType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::span[1]")
	public WebElement ActivityFeedClientPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::ul[1]/descendant::div[2]/descendant::li[1]/descendant::a[1]")
	public WebElement ActivityFeedSearchedClient;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following-sibling::div[1]/descendant::ul[1]/descendant::div[2]/descendant::li[18]/descendant::a[1]/descendant::span[1]")
	public WebElement PortfolioReportSearchedClient;
	@ButtonType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'The user is already logged in to the Community')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonLoggedIntoCommunity;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Type of Blue Card:')]/ancestor::div[3]/descendant::label[contains(normalize-space(text()),'Client:')]/following-sibling::span[1]/descendant::li[14]/descendant::a[1]/descendant::span[1]")
	public WebElement SearchedClientBlueCardforNewObjectClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::input[1]")
	public WebElement BlueCardforNewObjectObjName;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Type of Blue Card:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement BlueCardforNewObjectTypeofBlueCardPicklist;
	@LinkType()
	@FindBy(xpath = "//span[text()='Bunkers (BBC)']")
	public WebElement RequestCardforNewObjectTypepfBlueCardCheckbox;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Call sign:')]/following-sibling::input[1]")
	public WebElement BlueCardforNewObjectCallSign;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Flag:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement BlueCardforNewObjectFlag;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Flag:')]/following-sibling::div[1]/descendant::div[3]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement BlueCardforNewObjectSearchedFlag;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Date effective:')]/following-sibling::span[1]/descendant::input[1]")
	public WebElement BlueCardforNewObjectDateEffective;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Port of registry:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement BlueCardforNewObjectPortofRegistry;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Port of registry:')]/following-sibling::div[1]/descendant::div[3]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement BlueCardforNewObjectSearchedPort;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Call sign:')]/ancestor::div[1]/following-sibling::div[1]/descendant::input[1]")
	public WebElement BlueCardforNewObjectROName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Call sign:')]/ancestor::div[1]/following-sibling::div[2]/descendant::textarea[1]")
	public WebElement BlueCardforNewObjectROAddress;
	@ButtonType()
	@FindBy(xpath = "//span[text()='(Max 500 characters)']/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement BlueCardforNewObjectSubmitBtn;
	@ButtonType()
	@FindBy(xpath = "//div[contains((text()),'Thank you for contacting Gard')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonBlueCardforNewObjectMessage;
	@TextType()
	@FindBy(xpath = "//div[text()='Change your personal information']/following-sibling::div[1]/descendant::div[2]/descendant::div[4]/descendant::div[2]")
	public WebElement ContactForPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/ancestor::div[1]/descendant::span[1]/descendant::ul[1]/descendant::li[7]/descendant::a[1]/descendant::span[1]")
	public WebElement ClientSixthCheckbox;
	@LinkType()
	@FindBy(xpath = "//label[text()='Year']/following-sibling::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[11]/descendant::a[1]/descendant::span[1]")
	public WebElement OpenClaimYear2017;
	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Gard claim reference')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[4]")
	public WebElement GardClaimReferenceNew;
	@LinkType()
	@FindBy(xpath = "//span[text()='Reserve:']/ancestor::label[1]/following-sibling::div[1]/descendant::span[1]")
	public WebElement ClaimReserveAmt;
	@LinkType()
	@FindBy(xpath = "//span[text()='Reserve:']/ancestor::label[1]/following-sibling::div[1]/descendant::span[1]")
	public WebElement ClaimReserveAmt1;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Choose client name')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement MyPortfolioFormsChooseClientNamePicklist1;
	@FindBy(xpath = "//*[@id='mCSB_5_container']/descendant::li[3]/descendant::a[1]/descendant::span[1]")
	@TextType()
	public WebElement MyPortfolioFormsSearchedClientName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Renewal LOU-LOI for Gard P&I')]/descendant::div[5]")
	public WebElement RenewalLOU_LOIUploadLink;
	@FindBy(id = "MyPortfolioForms:pageFrame:j_id1105:manual-upload-doc-uwr")
	@ButtonType(file = true)
	public WebElement manual_upload_doc_uwr;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Upload']")
	public WebElement RenewalLOULOIFileUploadBtn;
	@TextType()
	@FindBy(xpath = "//div[contains((text()),'Thank you for contacting Gard')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ConfirmationMessageRenewalLOULOI;
	@LinkType()
	@FindBy(xpath = "//a[text()='P&I RENEWAL']")
	public WebElement P_IRenewalTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Blue cards and MLCs']/ancestor::div[1]")
	public WebElement P_IRenewalBlueCardsandMLCSubTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Renewal request']")
	public WebElement P_IRenewalRequestRenewalBtn;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Yours faithfully,')]/ancestor::div[3]/following-sibling::div[1]/descendant::input[2]")
	public WebElement RenewalOfBlueCardCancelBtn;
	@ButtonType()
	@FindBy(xpath = "//label[contains((text()),'I hereby confirm')]/ancestor::div[1]/following-sibling::div[3]/descendant::input[2]")
	public WebElement NewMLCCertificateCancelBtn;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(text())='Client :']/following-sibling::span[1]/descendant::div[2]")
	public WebElement RenewalOfBlueCardClientDropdown;
	@FindBy(xpath = "//label[normalize-space(text())='Client :']/ancestor::div[1]/descendant::span[2]/descendant::ul[1]/descendant::li[3]/descendant::a[1]")
	@LinkType()
	public WebElement RenewalOfBlueCardSearchedClient;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(text())='Object name:']/following-sibling::span[1]/descendant::div[2]")
	public WebElement RenewalOfBlueCardObjectNameBtn;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(text())='Object name:']/ancestor::div[1]/descendant::span[2]/descendant::ul[1]/descendant::li[1]/descendant::a[1]/descendant::span[1]")
	public WebElement RenewalOfBlueCardObjectNameCheckboxCheckAll;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Effective from:')]/following-sibling::span[1]/descendant::input[1]")
	public WebElement NewMLCCertificateEffectiveFrom;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::table[2]/descendant::tr[2]/descendant::td[4]")
	public WebElement NewMLCCertificateEffectFromCalenderFrom;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Effective to:')]/following-sibling::span[1]/descendant::input[1]")
	public WebElement NewMLCCertificateEffectiveTo;
	@TextType()
	@FindBy(xpath = "//a[text()='Terms and Conditions']/ancestor::span[1]/preceding-sibling::span[1]/descendant::textarea[1]")
	public WebElement NewMLCCertificateComment;
	@TextType()
	@FindBy(xpath = "//label[contains((text()),'I hereby confirm that the information')]")
	public WebElement NewMLCCertificateUndertakingsCertificate;
	@ButtonType()
	@FindByLabel(label = "Submit")
	public WebElement submit;
	@ButtonType()
	@FindBy(xpath = "//label[contains((text()),'I hereby confirm that the information')]/ancestor::div[1]/following-sibling::div[3]/descendant::input[1]")
	public WebElement NewMLCCertificateSubmitBtn;
	@LinkType()
	@FindBy(xpath = "//a[@class='pager-next pager-section']")
	public WebElement MyCompanyContactsNextPageNavigateArrow;
	@TextType()
	@FindBy(xpath = "//a[text()='ag@examplebergvallmarine.com']/parent::div[1]/preceding-sibling::div[5]/descendant::div[text()='Edit']")
	public WebElement MyCompanysContactsEdit;
	@TextType()
	@FindBy(xpath = "//label[text()='No longer employed by company?']/ancestor::div[3]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ChangeContactInfoStLine1;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Contact information has been successfully updated.']/following-sibling::div[1]/descendant::span[1]")
	public WebElement ContactInfoSuccessMessageOkButton;
	@ButtonType()
	@FindBy(xpath = "//label[text()='No longer employed by company?']/ancestor::div[3]/preceding-sibling::div[1]/descendant::span[1]/descendant::div[3]")
	public WebElement ChangeContactInfoContactFor2;
	@LinkType()
	@FindBy(xpath = "//label[text()='No longer employed by company?']/ancestor::div[3]/preceding-sibling::div[1]/descendant::span[1]/descendant::div[3]/descendant::ul[1]/descendant::li[3]/descendant::span[1]")
	public WebElement select_contact_role_panel;
	@LinkType()
	@FindBy(xpath = "//a[text()='Create new contact']")
	public WebElement CreateNewContact;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Provide MyGard access?')]/ancestor::div[4]/preceding-sibling::div[2]/descendant::span[1]/descendant::div[3]")
	public WebElement AddNewContactInfoContactFor;
	@LinkType()
	@FindBy(xpath = "//div[text()='Gard ref no.']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::a[1]")
	public WebElement SearchResultsGardRefNo;
	@TextType()
	@FindBy(xpath = "//a[text()='Reserve history']/ancestor::div[1]/preceding-sibling::div[1]")
	public WebElement ClaimDetailsPaymentHistorySubTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Reserve history']/ancestor::div[1]")
	public WebElement ClaimDetailsReserveHistorySubTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Payment history']/ancestor::div[1]")
	public WebElement ClaimDetailsPaymentHistorySubTabSwitchFromReserveHistorySubTab;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"copy-right-details\")]")
	public WebElement MyGard_Footer;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(text())='Comment on claim']")
	public WebElement ClaimDetailsCommentOnClaim;
	@TextType()
	@FindBy(xpath = "//div[text()='Comment']/ancestor::div[1]/descendant::textarea[1]")
	public WebElement ClaimDetailsCommentOnClaimComment;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Comment']/ancestor::div[2]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ClaimDetailsCommentClaimSubmit;
	@ButtonType()
	@FindBy(xpath = "//div[normalize-space(text())='Thank you for submitting your comments on the claim.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonClaimDetailsCommentSubmit;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tbody[1]/descendant::td[1]")
	public WebElement NewMLCCertificateEffectiveToPrevMonthIcon;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Arrival date:')]/following-sibling::span[1]/descendant::button[@class='Zebra_DatePicker_Icon Zebra_DatePicker_Icon_Inside']")
	public WebElement LayupFormP_IArrivalDate;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Departure date:')]/following-sibling::span[1]/descendant::button[@class='Zebra_DatePicker_Icon Zebra_DatePicker_Icon_Inside']")
	public WebElement LayUpFormDepartureDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tbody[1]/descendant::td[1]")
	public WebElement LayupFormP_IPrevMonthIcon;
	@LinkType()
	@FindBy(xpath = "//a[text()='My covers']/ancestor::div[1]/following-sibling::div[2]")
	public WebElement MyPortfolioDocumentsSubTab;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Layup form Marine']/descendant::div[2]/descendant::a[normalize-space(text())='Fill in online']")
	public WebElement LayupFormMarineFillInOnline;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Arrival date final moorings:')]/following-sibling::span[1]/descendant::button[@class='Zebra_DatePicker_Icon Zebra_DatePicker_Icon_Inside']")
	public WebElement LayUpFormMarineArrivalDate;
	@TextType()
	@FindBy(xpath = "//div[text()='Company information']/following-sibling::span[1]/descendant::div[text()='Edit']")
	public WebElement CompanyInformationEditIcon;
	@TextType()
	@FindBy(xpath = "//label[text()='Fax']/following-sibling::input[1]")
	public WebElement ChangeYourCompanyInfoFax;
	@TextType()
	@FindBy(xpath = "//div[text()='Company information']/following-sibling::span[1]/descendant::div[text()='Edit']")
	public WebElement MyAddressBookCompanyInfoEditIcon;
	@TextType()
	@FindBy(xpath = "//label[text()='Fax']/following-sibling::input")
	public WebElement MyAddressBookChangeCompanyInfoFax;
	@TextType()
	@FindBy(xpath = "//label[text()='24 hour phone']/following-sibling::input[1]")
	public WebElement ChangeYourCompanyInfo24HrPhone;
	@ButtonType()
	@FindBy(xpath = "//label[text()='24 hour phone']/ancestor::div[2]/following-sibling::div[3]/descendant::input[1]")
	public WebElement ChangeYourCompanyInfoSubmit;
	@TextType()
	@FindBy(xpath = "//label[text()='24 hour phone']/ancestor::div[1]/preceding-sibling::div[1]/descendant::input[2]")
	public WebElement ChangeYourCompanyInfoPhone;
	@TextType()
	@FindBy(xpath = "//label[text()='First name:']/following-sibling::input[1]")
	public WebElement RequestNewuserAccessFirstName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Last name:']/following-sibling::input[1]")
	public WebElement RequestNewUserAccessLastName;
	@TextType()
	@FindBy(xpath = "//label[text()='Telephone:']/following-sibling::input[1]")
	public WebElement RequestNewUserTelephone;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Email:']/following-sibling::input[1]")
	public WebElement RequestNewUserAccessEmail;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Email:']/ancestor::div[1]/following-sibling::div[1]/descendant::textarea[1]")
	public WebElement RequestNewUserAccessEmail1;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Submit and new']")
	public WebElement RequestNewUserAccessSubmitAndNew;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Primary assured name:')]/following-sibling::input[1]")
	public WebElement ComprehensiveChartersCoverFormPrimaryAssuredName;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Primary assured’s address:')]/following-sibling::textarea[1]")
	public WebElement ComprehensiveChartersCoverFormPrimaryAddress;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Phone no:')]/following-sibling::input[1]")
	public WebElement ComprehensiveCharterersCoverFormPhone;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Name to appear on premium invoice:')]/following-sibling::input[1]")
	public WebElement ComprehensiveCharterersCoverFormNamePremiumInvoice;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Address details:')]/following-sibling::textarea[1]")
	public WebElement ComprehensiveCharterersCoverFormAddressDetails;
	@TextType()
	@FindBy(xpath = "//label[contains((text()),'name and address of the operating')]/following-sibling::textarea[1]")
	public WebElement ComprehensiveCharterersCoverFormVatNameAddress;
	@TextType()
	@FindBy(xpath = "//label[contains((text()),'Registered for VAT')]/following-sibling::input[1]")
	public WebElement ComprehensiveCharterersCoverFormVAT;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_3_container']/descendant::li[4]/descendant::a[1]")
	public WebElement HeaderContactMeAreaDropdownlist;
	@TextType()
	@FindBy(xpath = "//label[text()='Subject']/following-sibling::div[1]/descendant::textarea[1]")
	public WebElement HeaderContactMeSubject;
	@TextType()
	@FindBy(xpath = "//label[text()='Subject']/ancestor::div[1]/following-sibling::div[2]/descendant::div[2]")
	public WebElement HeaderContactMePhone;
	@TextType()
	@FindBy(xpath = "//label[text()='Subject']/ancestor::div[1]/following-sibling::div[2]/descendant::div[3]")
	public WebElement HeaderContactMeEmail;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Subject']/ancestor::span[1]/following-sibling::div[1]/descendant::input[1]")
	public WebElement HeaderContactMeSubmit;
	@TextType()
	@FindBy(xpath = "//label[text()='Website']/ancestor::div[2]/descendant::input[1]")
	public WebElement ChangeYourCompanyInfoEmail;
	@TextType()
	@FindBy(xpath = "//label[text()='Website']/following-sibling::input[1]")
	public WebElement ChanegYourCompanyInformationWebsite;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Clients:')]/following-sibling::span[1]/descendant::div[2]")
	public WebElement ChangeTotheBlueCardClientDropdown;
	@PageWaitAfter.Field(field = "ChangeToTheBlueCardObjectName", timeoutSeconds = 10)
	@FindBy(linkText = "AKOFS Offshore AS")
	@LinkType()
	public WebElement ChangeToTheBlueCardSearchedClient;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object name:')]/following-sibling::span[1]/descendant::div[2]/descendant::button")
	public WebElement ChangeToTheBlueCardObjectName;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Type of Blue Card:')]/following-sibling::div[1]/descendant::div[1]/descendant::button")
	public WebElement ChangeToTheBlueCardTypeOfBlueCard;
	@ButtonType()
	@FindBy(xpath = "//span[text()='(Max 500 characters)']/ancestor::div[2]/following-sibling::div[1]/descendant::input[1]")
	public WebElement ChangeToTheBlueCardSubmit;
	@TextType()
	@FindBy(xpath = "//span[text()='(Max 500 characters)']/preceding-sibling::textarea[1]")
	public WebElement ChangeToTheBlueCardComment;
	@LinkType()
	@FindBy(css = "#mCSB_17_container a.multiselect-all label.checkbox span")
	public WebElement ChangeToTheBlueCardObjectNameCheckboxCheckAll;
	@TextType()
	@FindBy(xpath = "//label[text()='Provide MyGard access?']/ancestor::div[4]/preceding-sibling::div[2]/descendant::label[@for='last-name']/following-sibling::input")
	public WebElement AddNewContactInfoLastName;
	@ButtonType()
	@FindBy(xpath = "//*[@id=\"myAccount:pageFrame:customPopupEdit:select-contact-role-panel\"]/div/div/div/button")
	public WebElement AddNewContactInfoContactFor1;
	@LinkType()
	@FindBy(linkText = " Accounting Secondary M&E")
	public WebElement AddNewContactInfoContactforCheckBox;
	@LinkType()
	@FindBy(xpath = "//span[@id='myAccount:pageFrame:customPopupEdit:select-contact-role-panel']/descendant::div[2]/descendant::div[1]/descendant::ul\n[1]/descendant::div[2]/descendant::li[9]/descendant::span[1]")
	public WebElement AddNewContactInfoContactForChecklist;
	@TextType()
	@FindBy(xpath = "//label[text()='Provide MyGard access?']/ancestor::div[4]/preceding-sibling::div[1]/descendant::label[3]/following-sibling::input")
	public WebElement AddNewContactInfoEmail;
	@TextType()
	@FindBy(xpath = "//div[@class='my-contact-tab single-tab selected']/following::div[@class='info-edit personal-info-edit-myacc'][2]")
	public WebElement MyCompanyContactsEditIcon;
	@TextType()
	@FindBy(xpath = "//label[text()='No longer employed by company?']/ancestor::div[3]/descendant::input[1]")
	public WebElement ChangeContactInfoPhone;
	@TextType()
	@FindBy(xpath = "//label[text()='No longer employed by company?']/ancestor::div[5]/descendant::input[1]")
	public WebElement ChangeContactInfoFirstName;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Contact information has been successfully updated.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement ContactInfoUpdateMessage;
	@ButtonType()
	@FindBy(xpath = "//label[@for='contact-me-area']/following-sibling::div[1]")
	public WebElement HeaderContactMeAreaPicklist;
	@LinkType()
	@FindBy(xpath = "//label[text()='Trading patterns: ']/following-sibling::div[1]/descendant::tr[2]/descendant::td[3]/descendant::a[1]")
	public WebElement CharterersRenewalQuestionnaireTradingPatternLeftUppeTableRow1Arrow;
	@LinkType()
	@FindBy(xpath = "//label[text()='Trading patterns: ']/following-sibling::div[1]/descendant::tr[3]/descendant::td[3]/descendant::a[1]")
	public WebElement CharterersRenewalQuestionnaireTradingPatternLeftUpperTableRow2Arrow;
	@LinkType()
	@FindBy(xpath = "//label[text()='Trading patterns: ']/following-sibling::div[1]/descendant::tr[4]/descendant::td[3]/descendant::a[1]")
	public WebElement CharterersRenewalQuestionnaireTradingPatternLeftUpperTableRow3Arrow;
	@LinkType()
	@FindBy(xpath = "//label[text()='The Cargo (owned vs third party): ']/following-sibling::div[1]/descendant::tr[2]/descendant::td[3]/descendant::a[1]")
	public WebElement CharterersRenewalQuestionnaireTradingPatternLeftLowerTableRow1Arrow;
	@LinkType()
	@FindBy(xpath = "//label[text()='The Cargo (owned vs third party): ']/following-sibling::div[1]/descendant::tr[3]/descendant::td[3]/descendant::a[1]")
	public WebElement CharterersRenewalQuestionnaireTradingPatternLeftLowerTableRow2Arrow;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Name of the Assured(s):')]/following-sibling::div[1]")
	public WebElement CharterersRenewalQuestionnaireNameOfAssuredPicklist;
	@TextType()
	@FindBy(xpath = "//label[text()='My claim reference:']/following-sibling::input[1]")
	public WebElement NewClaimMyClaimRef;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client:']/following-sibling::div[1]")
	public WebElement NewClaimClientPicklist;
	@ButtonType()
	@FindBy(xpath = "//span[text()='Event date:']/ancestor::label[1]/following-sibling::span[1]/descendant::button")
	public WebElement NewClaimEventDatePickADate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::table[2]/descendant::tr[3]/descendant::td[4]")
	public WebElement NewClaimEventDateCalenderDate;
	@LinkType()
	@FindBy(xpath = "//a[text()='My information & subscriptions']")
	public WebElement MyInformationAndSubscriptions;
	@TextType()
	@FindBy(xpath = "//div[text()='My claims']/following-sibling::div[1]/descendant::a[text()='Registered claims']")
	public WebElement RegisteredClaimsSubTab;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='In progress claims' and @href='/mygard/DraftClaims']")
	public WebElement InProgressClaimsSubTab;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::div[1]/descendant::div[3]/following-sibling::ul[1]/descendant::li[5]/descendant::span[1]")
	public WebElement NewClaimClientSearchedClient;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Subject']/ancestor::span[1]/preceding-sibling::span[1]/descendant::span[10]/descendant::div[1]")
	public WebElement HeaderContactMeClientPicklist;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to MLC Certificate']")
	public WebElement ChangeToMLCCertificate;
	@LinkType()
	@FindBy(xpath = "//a[text()=\"COFR letter\"]")
	public WebElement COFRLetter;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Name of the Assured')]/following-sibling::div[1]")
	public WebElement ChartersRenewalQuestNameOfAssuredPicklist;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Summary-Invoice details']/ancestor::div[8]/following-sibling::div[1]/descendant::input[@value='Cancel']")
	public WebElement NewInvoice;
	@LinkType()
	@FindBy(xpath = "//a[text()='tw']")
	public WebElement Footer_Twitter;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"request-updates\")]")
	public WebElement ServiceRequestTab;
	@LinkType()
	@FindBy(xpath = "//div[text()='Request ID']/ancestor::thead/following-sibling::tbody[1]/descendant::td[2]/descendant::a[1]")
	public WebElement ServicerequestsRequestID;
	@ButtonType()
	@FindByLabel(label = "View profile")
	public WebElement Click_the_View_Profile;
	@TextType()
	@FindBy(xpath = "//label[text()=' Gard PEME reference no.:']/following::li[2]")
	public WebElement GardPEMERef;
	@FindBy(css = "td.dp_current")
	@TextType()
	public WebElement PickToDate;
	@TextType()
	@FindBy(xpath = "(//table[@class='dp_daypicker'])[2]/descendant::td[@class='dp_current']/following-sibling::td[1]")
	public WebElement PickTODATE;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Manning agent:']/following::li[2]")
	public WebElement seacrestMaritimeManagementInc;
	@TextType()
	@FindBy(xpath = "(//table[@class='dp_daypicker'])[3]/descendant::td[@class='dp_current']")
	public WebElement PickDate;
	@PageRow()
	public static class ServiceReqTable {
	}
	@FacetFindBys(value = {
			@FacetFindBy(findBy = @FindBy(xpath = "//span[normalize-space(.)=concat('Client Request ID Request Object Raised Date Case Handler Status Expected closure date Raised By Crown Ship Builders Corp 00676853 Request change to assureds Vespa 04-07-2019 Portfolio requests … New 04-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676852 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676850 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676849 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676847 COFR Request Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor')]/div/table/tbody/tr/td[normalize-space(.)='Crown Ship Builders Corp']"), facet = UiFacet.DATA_ROWS),
			@FacetFindBy(findBy = @FindBy(xpath = "//span[normalize-space(.)=concat('Client Request ID Request Object Raised Date Case Handler Status Expected closure date Raised By Crown Ship Builders Corp 00676853 Request change to assureds Vespa 04-07-2019 Portfolio requests … New 04-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676852 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676850 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676849 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676847 COFR Request Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor')]/div/table/tbody/tr/td[normalize-space(.)='Crown Ship Builders Corp']"), facet = UiFacet.HEADER_ROW) })
	@FindBy(xpath = "//span[normalize-space(.)=concat('Client Request ID Request Object Raised Date Case Handler Status Expected closure date Raised By Crown Ship Builders Corp 00676853 Request change to assureds Vespa 04-07-2019 Portfolio requests … New 04-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676852 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676850 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676849 Request change to assureds Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor Crown Ship Builders Corp 00676847 COFR Request Vespa 03-07-2019 Portfolio requests … New 03-07-2019 Nathan O',\"'\",'Connor')]/div/table/tbody/tr")
	@PageTable(firstRowContainsHeaders = false, row = ServiceReqTable.class)
	public List<ServiceReqTable> ServiceReqTable;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[1]/div")
	public WebElement ServiceReqHeadings_Client;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[2]/div")
	public WebElement ServiceReqHeading_ReqID;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[3]/div")
	public WebElement Service_Req_Heading_Reg;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[4]/div")
	public WebElement ServiceReqHeading_Obj;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[5]/div")
	public WebElement ServiceReqHeadingRaisedDate;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[6]/div")
	public WebElement ServiceReqHeadingCase;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[7]/div")
	public WebElement ServiceReqHeadingStatus;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[8]/div")
	public WebElement ServiceReqHeadingExpectedClosureDate;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[9]/div")
	public WebElement ServiceReqHeadingRaisedBy;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[1]")
	public WebElement ServiceReqClient;
	@LinkType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[3]/a[1]")
	public WebElement ServiceReq_request;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[4]")
	public WebElement ServiceReq_Obj;
	@LinkType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[7]/a[1]")
	public WebElement ServiceReq_status;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[9]")
	public WebElement ServiceReq_RaisedBy;
	@TextType()
	@FindBy(xpath = "//span[text()='Multi']")
	public WebElement HomeMultiDD;
	@TextType()
	@FindBy(xpath = "//span[text()='Multi']/following::input[1]")
	public WebElement searchVal;
	@TextType()
	@FindBy(xpath = "(//tbody/tr/td[@class='requestid'])[1]/a[1]")
	public WebElement ServiceReqID;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Request ID:']")
	public WebElement service_req_details_form;
	@LinkType()
	@FindBy(xpath = "(//td[@class='request'])[1]/a")
	public WebElement ServiceReq_Request;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='The user is already logged in to the Community']")
	public WebElement MultiLoginPopup;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='7']")
	public WebElement ToDate;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='7']")
	public WebElement ToDate1;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='8']")
	public WebElement TmrwDate;
	@TextType()
	@FindBy(xpath = "(//td[@class='dp_weekend dp_current'])[2]")
	public WebElement ToDate11;
	@TextType()
	@FindBy(xpath = "(//td[normalize-space(.)='9'])[2]")
	public WebElement Nextday;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='7' and contains(@class,'dp_weekend')]")
	public WebElement TodayDate;
	@TextType()
	@FindBy(xpath = "//label[text()='Object']/following::input[1]")
	public WebElement ObjectTextbox;
	@TextType()
	@FindBy(xpath = "(//label[text()='Object']/following::ul/li[normalize-space(.)='No result found!'])[1]")
	public WebElement NoResultFountErr;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Portfolio reports']")
	public WebElement PortfolioReports1;
	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='Request change to assureds'])[1]")
	public WebElement ServiceReq_request1;
	@LinkType()
	@FindBy(xpath = "(//a[text()='Request change to assureds'])[1]")
	public WebElement ServiceReq_request11;
	@TextType()
	@FindBy(xpath = "//a[@class='vcard-download']/following::div[@class='info-name']")
	public WebElement UnderWriterName;
	@TextType()
	@FindBy(css = "td.dp_current.dp_hover")
	public WebElement TodayDate1;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('td.inv-exm-examination > div > div > div > button')")
	public WebElement Examination;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='PEME 40 or above']/label/input[@type='checkbox']")
	public WebElement PEME40OrAbove;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('tr.custom-row.even > td.inv-exm-examination > div > div > div > button')")
	public WebElement Examination1;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_3_container']//a[normalize-space(.)='PEME 40 or above']/label/input[1]")
	public WebElement pEME40OrAbove;
	@TextType()
	@JavascriptBy(jspath = "return document.querySelector('td.dp_current')")
	public WebElement Pick2DayDate;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('tr:nth-child(3) > td.inv-exm-examination > div > div > div > button')")
	public WebElement Examination2;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_4_container']//a[normalize-space(.)='PEME 40 or above']/label/input")
	public WebElement pEME40OrAbove1;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Marine forms']")
	public WebElement check_marine;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Active Shipping an...']/label/input[1]")
	public WebElement ActiveShippingAnClient;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Active Shipping and Management Pte Ltd']")
	public WebElement activeShippingAndManagementPteLtd;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Bank branch name / code:']/following-sibling::input")
	public WebElement bank_branch_name_code;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Account number:']/following-sibling::input")
	public WebElement account_number;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='SWIFT/BIC Code:']/following-sibling::input")
	public WebElement swift_bic_code;
	@TextType()
	@FindBy(xpath = "//label[text()='Invoice number:']/following::div[1]")
	public WebElement InvoiceNumber1;
	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div[1]")
	public WebElement SuccessfullyRegistered;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='General account template']")
	public WebElement generalAccountTemplate;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabClaims Analysis']/a")
	public WebElement claimsAnalysis;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Marine Reports']")
	public WebElement MarineReport;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Documents']")
	public WebElement docTab;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following::button[1]")
	public WebElement ClientDD;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::ul[1]/div/div/li[3]")
	public WebElement Client2FromClientPicklist;
	@ButtonType()
	@FindBy(xpath = "//div/input[@value='Go' and @type='submit']/..")
	public WebElement GoBtn;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Please select Policy Year.']")
	public WebElement PolicyYearCheck;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Policy year']/following-sibling::div[1]//button")
	public WebElement PolicyYearDD;
	@LinkType()
	@FindBy(xpath = "//label[text()='Policy year']/following::ul[1]/div/div/li[2]")
	public WebElement PolicyYear1stValue;
	@LinkType()
	@FindBy(xpath = "//label[text()='Policy year']/following::ul[1]/div/div/li[2]/a/label/span[1]")
	public WebElement PolicyYearLatestYrValue;	
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Document type description']/following-sibling::div//button[1]")
	public WebElement DocumentTypeDesc;
	@LinkType()
	@FindBy(xpath = "//div[@class='mCSB_container']//a[normalize-space(.)='Check all']/label/input")
	public WebElement DocTypeSelectAll;
	@LinkType()
	@FindBy(xpath = "(//span[normalize-space(.)='Check all']/../input[1])[3]")
	public WebElement ObjectCheckAll;
	@LinkType()
	@FindBy(xpath = "(//span[text()='Check all']/../input[1])[3]")
	public WebElement Object_CheckAll;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim detail']/following-sibling::div//button")
	public WebElement ClaimDetail;
	@LinkType()
	@FindBy(xpath = "(//label[text()='Claim detail']/following::label/input)[1]")
	public WebElement ClaimDetail_First_Checkbox;

	@LinkType()
	@FindBy(xpath = "//label[text()='Claim detail']/following-sibling::div[1]/descendant::div[1] /descendant::li[2]/descendant::input[1]")
	public WebElement ClaimDetail_firstCheckbox;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Year']/following-sibling::div[1]/descendant::div[1] /descendant::li[1]/descendant::input[1]")
	public WebElement Year_firstCheckbox;

	@TextType()
	@JavascriptBy(jspath = "return document.querySelector('div.single-tab.selected')")
	public WebElement ClaimDetailsInfos;

	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim type']/parent::div/following-sibling::div[1]//input[@type='submit' and @value='Go']")
	public WebElement GoBtn1;

	@LinkType()
	@FindBy(xpath = "//li[3]/a[1]")
	public WebElement _rdClientSelection;

	@ButtonType()
	@FindBy(xpath = "//span/div[@id='contact-info-pop-container']//input[1]")
	public WebElement Cancel_Yes;

	@LinkType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[3]/a[1]")
	public WebElement ServiceReq_request111;

	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[6]/span[1]")
	public WebElement caseHandler;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='View all service requests']")
	public WebElement viewAllServiceRequests;

	@LinkType()
	@FindBy(xpath = "//span/div/table/tbody/tr[1]/td[3]/a")
	public WebElement ServiceReq_requestName;

	@LinkType()
	@FindBy(xpath = "//label[text()='Object']/following::a[1]/label/input[1]")
	public WebElement ObjectCheckAll1;

	@LinkType()
	@FindBy(xpath = "//label[text()='Year']/following::ul/div/div/li[2]/a/label/span[1]")
	public WebElement MyClaimYearCurrent;

	@TextType()
	@FindBy(xpath = "(//table/tbody/tr/td[11]/a[1])[1]")
	public WebElement GardClaimReference1;

	@ButtonType()
	@FindBy(xpath = "//input[@id='download_claim-doc']")
	public WebElement downloadSelectedFiles;

	@LinkType()
	@FindBy(xpath = "//div[@class='global-print-dwnld']/a[@title='Export to excel']")
	public WebElement ExportasExcel;

	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Status']/following-sibling::div//button")
	public WebElement StatusPickList;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Approved']")
	public WebElement Status_Approved;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Draft invoices']")
	public WebElement DraftInvoices;

	@TextType()
	@FindBy(xpath = "//div/div/div[normalize-space(.)='Age must be a number between 18 to 71.']")
	public WebElement Error_Message;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Account holder:']/following-sibling::input")
	public WebElement account_holder;

	@ButtonType()
	@FindBy(xpath = "//span/div[2]/div/input[1]")
	public WebElement ExportasExcel1;

	@ButtonType()
	@FindBy(xpath = "//form/div[3]/div/input[1]")
	public WebElement ExporttoExcelRegClaim;

	@ButtonType()
	@FindBy(xpath = "//form/div[2]/div/input[1]")
	public WebElement ExporttoExcelInProgressClaim;

	@ButtonType()
	@FindBy(xpath = "//button[@type='button']")
	public WebElement SelectCompanyBtn;

	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search']")
	public WebElement MultiCompPopupCompany;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Bergvall Marine ASOslo, Norway']")
	public WebElement BergallCompany;

	@TextType()
	@FindBy(xpath = "//table/tbody/tr/td[2]")
	public WebElement InvoiceNumber2;

	@ButtonType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div[2]/input[1]")
	public WebElement Deleteconfirm;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Bernhard Schulte GmbH & Co. KG.Hamburg, Germany']")
	public WebElement MultiCompPopup;

	@TextType()
	@FindBy(xpath = "//th[2]/div[1]")
	public WebElement DocumentTabHeader1;

	@TextType()
	@FindBy(xpath = "//th[3]/div[1]")
	public WebElement DocumentTabHeader2;

	@TextType()
	@FindBy(xpath = "//th[4]/div")
	public WebElement DocumentTabHeader3;

	@TextType()
	@FindBy(xpath = "//th[5]/div")
	public WebElement DocumentTabHeader4;

	@TextType()
	@FindBy(xpath = "//th[6]/div[1]")
	public WebElement DocumentTabHeader5;

	@TextType()
	@FindBy(xpath = "//th[7]/div")
	public WebElement DocumentTabHeader6;

	@TextType()
	@FindBy(xpath = "//th[8]/div")
	public WebElement DocumentTabHeader7;

	@TextType()
	@FindBy(xpath = "//th[9]/div")
	public WebElement DocumentTabHeader8;

	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Document Type']/following-sibling::div//button")
	public WebElement DocumentType;

	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_15_container']//a[normalize-space(.)='Check all']")
	public WebElement DocumentTypeCheckAll;

	@TextType()
	@FindBy(xpath = "//th[7]/div/div/div")
	public WebElement LastUpdatedSorted;

	@BooleanType()
	@FindBy(xpath = "//tr[1]/td[1]/input[@type='checkbox']")
	public WebElement Doc1stCheckBox;

	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Company information']")
	public WebElement CompanyInformation;

	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Business']")
	public WebElement MySubscription_BusinessSec;

	@TextType()
	@FindBy(xpath = "//form//div[normalize-space(.)='Updates']")
	public WebElement MySubscription_Updates;

	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Governance']")
	public WebElement MySubscription_Governance;

	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Language']")
	public WebElement MySubscriptionLang;

	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Email preferences']")
	public WebElement MySubscriptionEmailPref;

	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Employee name']")
	public WebElement EmpName;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Download data']")
	public WebElement GardContactsAddBook_id;

	@TextType()
	@FindBy(xpath = "//th[1]/div[1]")
	public WebElement TableHeading_Client;

	@TextType()
	@FindBy(xpath = "//th[10]/div[1]")
	public WebElement TableHeader10;

	@TextType()
	@FindBy(xpath = "//th[11]/div[1]")
	public WebElement TableHeading11;

	@TextType()
	@FindBy(xpath = "//th[12]/div[1]")
	public WebElement TableHeading12;

	@TextType()
	@FindBy(xpath = "//th[13]/div[1]")
	public WebElement AllClaimStatus;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[2]/div/div[1]/div[1]/div[1]/input")
	public WebElement PI_FirstName;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div/div[1]/div[2]/div[1]/input")
	public WebElement PI_LastName;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[2]/div/div[1]/div[1]/div[2]/input")
	public WebElement PI_Title;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[1]/div[2]/div[2]/input")
	public WebElement dept;

	@TextType()
	@FindBy(xpath = "(//div/label[text()='Phone']/following-sibling::input[2])[1]")
	public WebElement PI_Phone;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[2]/div[2]/div/input")
	public WebElement PI_Email;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[2]/div[1]/div[2]/input[1]")
	public WebElement PI_mob;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[3]/div[2]/input[1]")
	public WebElement PI_StreetLine;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[3]/div[3]/input")
	public WebElement PI_StreetLine1;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[3]/div[4]/input[1]")
	public WebElement PI_StreetLine2;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[@id='contact-info-pop-wrapper']//div[3]/div[5]/input")
	public WebElement PI_St_Line;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[6]/div[1]/div[1]/input[1]")
	public WebElement PI_City;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[6]/div[2]/div[1]/input[1]")
	public WebElement PostalCode;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[6]/div[1]/div[2]/input[1]")
	public WebElement PI_ctry;

	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-container']//div[6]/div[2]/div[2]/input")
	public WebElement PI_State;

	@TextType()
	@FindBy(xpath = "//div[2]/div[2]/div[1]/div[1]/div[2]/input[1]")
	public WebElement CI_StLine1;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Street Line 1']/following-sibling::input[1]")
	public WebElement CI_VisitingAddr_StLine1;

	@TextType()
	@FindBy(xpath = "//div/div[1]/div[3]/input[1]")
	public WebElement CI_Postalstrt2;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Street Line 2']/following-sibling::input")
	public WebElement CI_VAStLine2;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Street Line 3']/following-sibling::input")
	public WebElement CI_PA_St_Line3;

	@TextType()
	@FindBy(xpath = "//div[2]/div[4]/input[1]")
	public WebElement CI_VAStLine3;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Street Line 4']/following-sibling::input[1]")
	public WebElement CI_PAStLine4;

	@TextType()
	@FindBy(xpath = "//div[1]/div[2]/div[5]/input[1]")
	public WebElement CIVAStLine4;

	@TextType()
	@FindBy(xpath = "//div[1]/div[6]/div[1]/input[1]")
	public WebElement CIPACIty;

	@TextType()
	@FindBy(xpath = "//div[2]/div[6]/div[1]/input[1]")
	public WebElement CI_VA_City;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Postal Code']/following-sibling::input[1]")
	public WebElement PA_pcode;

	@TextType()
	@FindBy(xpath = "//div[2]/div[6]/div[2]/input[1]")
	public WebElement CI_pcode;

	@TextType()
	@FindBy(xpath = "//div[1]/div[6]/div[3]/input[1]")
	public WebElement CI_PAstate;

	@TextType()
	@FindBy(xpath = "//div[2]/div[6]/div[3]/input[1]")
	public WebElement CI_VAstate;

	@TextType()
	@FindBy(xpath = "//div[1]/div[6]/div[4]/input[1]")
	public WebElement CI_PA_Country;

	@TextType()
	@FindBy(xpath = "//div[2]/div[6]/div[4]/input[1]")
	public WebElement CI_VA_Country;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='First name']/following-sibling::input[1]")
	public WebElement FirstName;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Title']/following-sibling::input[1]")
	public WebElement Title_Contact;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Department']/following-sibling::input[1]")
	public WebElement dept1;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Phone']/following-sibling::input[1]")
	public WebElement Phone;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Mobile']/following-sibling::input[1]")
	public WebElement ContactMobile;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Street line 1']/following-sibling::input[1]")
	public WebElement StreetLine1;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Street line 2']/following-sibling::input[1]")
	public WebElement StreetLine2;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Street line 3']/following-sibling::input[1]")
	public WebElement StreetLine3;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Street line 4']/following-sibling::input[1]")
	public WebElement StreetLine4;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='City']/following-sibling::input[1]")
	public WebElement City;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Postal code']/following-sibling::input[1]")
	public WebElement PostalCode1;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='Country']/following-sibling::input[1]")
	public WebElement Country;

	@TextType()
	@FindBy(xpath = "//div[text()='Add new contact information']/following::label[text()='State']/following-sibling::input[1]")
	public WebElement State;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Provide MyGard access?']/../input[1]")
	public WebElement ProvideMyGardAccess;

}
