package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardPortfolioReports"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class MyGardPortfolioReports {

	@TextType()
	@FindBy(xpath = "//div[text()='My portfolio report']")
	public WebElement MyPortfolioReport;
			
}
