package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCBjornNorsethContactDetailsPage"                                
     , connection="MyGardRegressionStaging"
     )             
public class SFDCBjornNorsethContactDetailsPage {

	@TextType()
	@FindBy(xpath = "//*[@id='workWithPortalButton']")
	public WebElement SFDCManageExternalUserBtn;
	@LinkType()
	@FindBy(xpath = "//div[@id=\"workWithPortal\"]/descendant::a[2]")
	public WebElement SFDCLogInToCommunityAsUser;
	@TextType()
	@FindBy(xpath = "//div[@id='userNavButton']/descendant::span[1]")
	public WebElement UserMenuLabel;
	@LinkType()
	@FindBy(xpath = "//div[@id='userNav-menuItems']/descendant::a[2]")
	public WebElement ProfileSetup;
	@ButtonType()
	@FindBy(xpath = "//h3[text()='MyGard Company-Contact relationships']/ancestor::div[1]/following-sibling::div[1]/descendant::a[text()='Edit'][2]")
	public WebElement MyGardCompany_ContactRelationshipsEdit;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Logout' and contains(@class,'menuButtonMenuLink') and @title='Logout']")
	public WebElement Logout;
			
}
