package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Opp Close Lost Declined"                                
               , summary=""
               , page="OppCloseLostDeclined"
               , namespacePrefix=""
               , object="Opportunity"
               , connection="MyGardRegressionQABox"
     )             
public class OppCloseLostDeclined {

	@ChoiceListType()
	@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
	public WebElement reasonLostDeclined;
	
}
