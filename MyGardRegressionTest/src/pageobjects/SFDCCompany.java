package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCCompany"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SFDCCompany {

	@LinkType()
	@FindBy(xpath = "//span[text()='All Updates']/ancestor::span[2]/following-sibling::div[5]/descendant::div[11]")
	public WebElement ViewMeeting;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='BrokerSelect to create a new Broker company']//span[contains(@class,'slds-radio--faux')]")
	public WebElement CheckBroker;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Next' and @type='button']")
	public WebElement NextButton;
			
}
