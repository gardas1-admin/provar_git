package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCNewSharedCompany"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCNewSharedCompany {

	@ButtonType()
	@FindBy(xpath = "//h2[text()='Shared Company Edit']/ancestor::td[1]/following-sibling::td[1]/descendant::input[1]")
	public WebElement Save;
	@TextType()
	@FindBy(xpath = "//label[text()='Shared With Broker']/ancestor::td[1]/following-sibling::td[1]/descendant::input[1]")
	public WebElement SharedWithBroker;
			
}
