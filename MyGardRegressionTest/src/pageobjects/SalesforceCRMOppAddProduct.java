package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMOppAddProduct"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class SalesforceCRMOppAddProduct {

	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Select item 1']/span[contains(@class,'slds-checkbox--faux')]/../input[1]")
	public WebElement BR_Misc;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Next' and contains(@class,'slds-button') and @title='Next' and @type='button']")
	public WebElement NextButton;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Select item 2']/span[contains(@class,'slds-checkbox--faux')]")
	public WebElement Builders_Risk;
	@ButtonType()
	@FindBy(css = "td.slds-cell-edit.slds-is-edited.cellContainer.slds-has-focus button.slds-button.trigger.slds-button_icon-border")
	public WebElement editEPIPrognosisItem1Edited;
	@TextType()
	@AuraBy(componentXPath = "//force:modal//ui:inputCurrency[@label= 'EPI (Prognosis)']")
	public WebElement Builders_Risk__Locked_Product__Item_1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='EPI (Prognosis)*']/following-sibling::input[@type='text']")
	public WebElement Builders_Risk__Locked_Product__Item_11;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Save' and contains(@class,'slds-button') and @title='Save' and @type='button']")
	public WebElement save;
	@BooleanType()
	@FindBy(xpath = "//span[text()='Select item 1']/../../../../../td[2]")
	public WebElement BR_MiscProduct;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Select item 3']/span[contains(@class,'slds-checkbox--faux')]/../input[1]")
	public WebElement Bunkers_Cover;
	@BooleanType()
	@FindBy(xpath = "//span[text()='Select item 3']/../input[1]")
	public WebElement BunkersCover;
	@ButtonType()
	@FindBy(xpath = "//div[@class='forceModalActionContainer--footerAction forceModalActionContainer']/button/span[text()='Next']")
	public WebElement NextBtn;
			
}
