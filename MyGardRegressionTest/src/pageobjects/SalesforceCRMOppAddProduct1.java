package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMOppAddProduct1"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class SalesforceCRMOppAddProduct1 {

	@ButtonType()
	@FindBy(xpath = "(//button[@class='slds-button trigger slds-button_icon-border'])[1]")
	public WebElement editEPIPrognosisItem1Edited;
	@LinkType()
	@FindBy(linkText = "BR Misc")
	public WebElement bRMisc;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='EPI (Prognosis)*']/following-sibling::input[1]")
	public WebElement BR_Misc__Locked_Product__Item_1;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Save' and contains(@class,'slds-button') and @data-aura-class='uiButton--default uiButton--brand uiButton forceActionButton' and @title='Save' and @type='button']")
	public WebElement save;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Actual Premium']/following-sibling::input[1]")
	public WebElement Actual_Premium__c;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Edit EPI (Prognosis): Item 1 Edited' and contains(@class,'slds-button') and @type='button']")
	public WebElement UnitPrice;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Edit Renewal Index (Prognosis): Item 1' and contains(@class,'slds-button') and @type='button']")
	public WebElement Index__c;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Renewal Index (Prognosis)']/following-sibling::input[1]")
	public WebElement Index__c1;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Edit EPI (Prognosis): Item 1' and contains(@class,'slds-button') and @type='button']")
	public WebElement EPI_Prognosis;
			
}
