package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCompanyOnRiskIndicator"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionQABox"
     )             
public class SalesforceCompanyOnRiskIndicator {

	@LinkType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//a[normalize-space(.)='New']")
	public WebElement NewCompany;
			
}
