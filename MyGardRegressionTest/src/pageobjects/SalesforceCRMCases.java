package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMCases"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceCRMCases {

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New']")
	public WebElement NewButton;
	@BooleanType()
	@FindBy(xpath = "//div[@class='changeRecordTypeRightColumn slds-form-element__control']/descendant::input[2]")
	public WebElement AdminRequestRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Admin RequestThis is the record type for cases generated from all types of admin request like Admin user change, Add new user, Contact me and Revoke user access']//input[1]")
	public WebElement AdminReqRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Blue cardThis is the record type for cases generated from all types of Blue Card forms (New, Renewal or Cancellation)']//input[1]")
	public WebElement BlueCardRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='COFR Requestused for COFR type of cases only']//input[1]")
	public WebElement CofrRequestRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Crew contract LOCWhen crew contract review type of case is created in CRM manually/from MyGard']//input[1]")
	public WebElement CrewContractLOCRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='MLC Certificatefor all details SF-3703']//input[1]")
	public WebElement MLCCertificateRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='MyGardClaimThis Record Type is Used to case claim only to System Admin and AdminLoader']//input[1]")
	public WebElement MyGardClaimRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Request changeThis is the record type for cases generated from all types of Request Forms']//input[1]")
	public WebElement RequestChangeRadioBtn;
	@BooleanType()
	@FindBy(xpath = "(//span[normalize-space(.)='Service Request']/../..//input)[1]")
	public WebElement ServiceRequestRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Charter clientsform type charter clients']//input[1]")
	public WebElement UWRCharterClientsRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Charter object']//input[1]")
	public WebElement UWRCharterObject;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Charterers Renewal Questionnaire']//input[1]")
	public WebElement UWRChartersRenewalQuestionaireRadioBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - ITOPF']//input[1]")
	public WebElement UWRCharterersITOPFRadiBtn;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Lay up Marine']//input[1]")
	public WebElement UWRLayUpMarine;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Lay up P&I']//input[1]")
	public WebElement UWRLayUpP_I;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Mobile offshore unitform type Mobile offshore unit']//input[1]")
	public WebElement UWRMobileOffshoreUnit;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Offshore objectform type Offshore object']//input[1]")
	public WebElement UWROffshoreObject;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - RI declarationform type RI declaration']//input[1]")
	public WebElement UWRRIDeclaration;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Ship ownerForm type Ship owner']//input[1]")
	public WebElement UWRShipOwner;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR FormsThis is the record type for cases generated from all types of Underwriter forms']//input[1]")
	public WebElement UWRForms;
	@ButtonType()
	@FindBy(xpath = "(//div[@class='button-container slds-text-align_right forceRecordEditActions'])[2]/div/div[3]/button[1]")
	public WebElement CancelButton;
	@ButtonType()
	@FindBy(xpath = "//div[@class='inlineFooter']/descendant::span[text()='Cancel']")
	public WebElement CancelButton1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Cases']")
	public WebElement CasesLink;
	@LinkType()
	@AuraBy(componentXPath = "//ui:virtualDataTable[@aura:id= 'grid']")
	public WebElement _0676848;
	@TextType()
	@FindBy(xpath = "//input[@name='Case-search-input']")
	public WebElement searchThisList;
	@TextType()
	@FindBy(xpath = "//header[@id='oneHeader']//div/div/div/input")
	public WebElement searchCasesAndMore;
	@TextType()
	public WebElement Status;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Next' and contains(@class,'slds-button') and @type='button']")
	public WebElement NextButton;
	@TextType()
	@FindBy(xpath = "//p[normalize-space(.)='No items to display.']")
	public WebElement NoItemstoDisplay;
	@LinkType()
	@FindBy(xpath = "//span/span[text()='Status']/following::a[1]")
	public WebElement Status1;
	@LinkType()
	@FindBy(xpath = "(//a[text()='--None--'])[2]")
	public WebElement StatusDDVerification1;
	@LinkType()
	@FindBy(xpath = "(//a[text()='New'])[2]")
	public WebElement StatusDDVerification2;
	@LinkType()
	@FindBy(xpath = "(//a[text()='In Progress'])[2]")
	public WebElement StatusDDVerification3;
	@LinkType()
	@FindBy(xpath = "(//a[text()='Waiting for customer'])[2]")
	public WebElement StatusDDVerification4;
	@LinkType()
	@FindBy(xpath = "(//a[text()='Closed'])[2]")
	public WebElement StatusDDVerification5;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short '])[10]/div/ul/li/a)[6]")
	public WebElement StatusDDVerification6;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short '])[10]/div/ul/li/a)[7]")
	public WebElement StatusDDVerification7;
	@TextType()
	@FindBy(xpath = "//div[@class='slds-grid listDisplays safari-workaround-anchor slds-border_right slds-border_left']/descendant::p[1]")
	public WebElement NoItemstoDisplayCase;
	@LinkType()
	@FindBy(xpath = "(//div[@class='listViewContent slds-table--header-fixed_container'])[1]/following::table[1]/tbody/tr/td[6]/span/div/div/div/a[1]")
	public WebElement CASES_SUBJECT1;
	@LinkType()
	@FindBy(xpath = "//span//a[normalize-space(.)='Email-to-Case verification for automation.']")
	public WebElement CASES_SUBJECT11;
	@BooleanType()
	@FindBy(xpath = "//span[normalize-space(.)='Claim comment']/../input[1]")
	public WebElement ClaimComment;
	@BooleanType()
	@FindBy(xpath = "//span[normalize-space(.)='Claim service request']/../input[1]")
	public WebElement ClaimServiceRequest;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - RI declarationform type RI declaration']/input[1]")
	public WebElement RecordType_UWRRIDeclaration;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='UWR - Offshore objectform type Offshore object']/input[1]")
	public WebElement RecordType_UWROffshore;
			
}
