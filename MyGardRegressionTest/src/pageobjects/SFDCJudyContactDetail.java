package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCJudyContactDetail"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCJudyContactDetail {

	@TextType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::div[2]")
	public WebElement ManageExternalUserBtn;
	@LinkType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::div[3]/descendant::a[2]")
	public WebElement LogInToCommunityAsUser;
	@LinkType()
	@FindBy(xpath = "//img[@class='allTabsArrow']")
	public WebElement PlusIcon;
	@LinkType()
	@FindBy(xpath = "//li[@id='MoreTabs_Tab']")
	public WebElement MoreTabs;
	@LinkType()
	@FindBy(xpath = "//a[text()='Client Activities']")
	public WebElement ClientActivities;
	@LinkType()
	@FindBy(linkText = "Cases")
	public WebElement Cases;
			
}
