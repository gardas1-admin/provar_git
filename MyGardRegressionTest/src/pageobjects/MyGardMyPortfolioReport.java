package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardMyPortfolioReport"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardE2E"
     )             
public class MyGardMyPortfolioReport {

	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabClaims Analysis']/a")
	public WebElement claimsAnalysis;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabClaims In Excess of Abatement']/a[1]")
	public WebElement claimsInExcessOfAbatement;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabDistribution of RI Costs etc. Per Year']/a[1]")
	public WebElement distributionOfRICostsEtcPerYear;
	@LinkType()
	@FindBy(xpath = "//div[2]/div/a[1]")
	public WebElement ExportToExcel;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabLoss Ratios']/a[1]")
	public WebElement lossRatios;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabVessels On Risk']/a[1]")
	public WebElement vesselsOnRisk;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabBreakdown of Claims']/a[1]")
	public WebElement breakdownOfClaims;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabFleet Development']/a[1]")
	public WebElement fleetDevelopment;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabFleet Presentation']/a[1]")
	public WebElement fleetPresentation;
	@LinkType()
	@FindBy(xpath = "//div[@id='reportTabFleet Statistics']/a[1]")
	public WebElement fleetStatistics;
			
}
