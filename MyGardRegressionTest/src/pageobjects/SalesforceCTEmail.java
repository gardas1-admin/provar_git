package pageobjects;



import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.ButtonType;
import com.provar.core.testapi.annotations.Page;
import com.provar.core.testapi.annotations.PageFrame;
import com.provar.core.testapi.annotations.TestLogger;
import com.provar.core.testapi.annotations.TextType;

@Page( title="SalesforceCTEmail"                                
, summary=""
, relativeUrl=""
, connection="MyGardRegression"
		)             
public class SalesforceCTEmail {

	static WebDriver driver;
	@TestLogger
	public Logger testLogger;

	public SalesforceCTEmail(WebDriver driver){

		this.driver=driver;
	}

	public void EnterTextEmailBody(String iframe1,String iframe2,String Locator, String EmailText){

		WebElement OuterFrame = driver.findElement(By.xpath(iframe1));

		driver.switchTo().frame(OuterFrame);

		WebElement InnerFrame= driver.findElement(By.xpath(iframe2));

		driver.switchTo().frame(InnerFrame);

		driver.findElement(By.xpath(Locator)).sendKeys(EmailText);

		driver.switchTo().defaultContent();	

	}
	public String CaseNumberinEmailSub(String iframe1,String iframe2,String Locator){

		WebElement OuterFrame = driver.findElement(By.xpath(iframe1));

		driver.switchTo().frame(OuterFrame);

		WebElement InnerFrame= driver.findElement(By.xpath(iframe2));

		driver.switchTo().frame(InnerFrame);

		String Subject=driver.findElement(By.xpath(Locator)).getText();

		driver.switchTo().defaultContent();
		
		return Subject;	

	}

	@PageFrame()
	public static class Frame {

		@TextType()
		@FindBy(xpath = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']/br[1]")
		public WebElement EmailBody;		
	}

	@FindBy(xpath = "//iframe[@title='Email Body']")
	public Frame EmailSection;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Send' and contains(@class,'slds-button')]")
	public WebElement publishersharebutton;
	@TextType()
	@FindBy(xpath = "(//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']/br/following::text()[9])[1]")
	public WebElement subject;	
}
