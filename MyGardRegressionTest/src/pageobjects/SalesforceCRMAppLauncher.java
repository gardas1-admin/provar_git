package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMAppLauncher"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceCRMAppLauncher {

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='App Launcher']")
	public WebElement AppLauncher;
	@LinkType()
	@FindBy(xpath = "//div[@class='app-grid']/descendant::li//a[text()='Sales']")
	public WebElement Sales;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Search apps and items...']/following-sibling::div//input")
	public WebElement searchAppsAndItems;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Sales']")
	public WebElement sales;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Agreements']")
	public WebElement Agreements;
			
}
