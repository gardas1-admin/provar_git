package pageobjects;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCTMyOpenCases"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionQABox"
     )             
public class SalesforceCTMyOpenCases {

	@PageFrame()
	public static class Frame {

		@TextType()
		@FindBy(xpath = "(//th/div/div/span/span[normalize-space(.)='Date/Time Opened'])[1]")
		public WebElement date_timeOpened;
		@TextType()
		@FindBy(xpath = "(//span/span[text()='Case Owner'])")
		public WebElement CaseOwner;
		@LinkType()
		@FindBy(xpath = "//div[@class='data-grid-table-ctr']/table/tbody/tr[2]/descendant::a[3]")
		public WebElement Subject;
		@LinkType()
		@FindBy(xpath = "(//div[@class='data-grid-table-ctr'])[2]/table/tbody/tr[2]/descendant::a[3]")
		public WebElement Subject1;
		@ButtonType()
		@FindBy(xpath = "(//button[text()='Change Owner'])[1]")
		public WebElement ChangeOwner;
		@TextType()
		@FindBy(xpath = "//table/tbody/tr[2]/th/div/div/input[1]")
		public WebElement AddDuplicateCase;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Merge' and @type='button']")
		public WebElement MergeBtn;
		@TextType()
		@FindBy(xpath = "//table/tbody/tr[2]/th/div/div/label[1]")
		public WebElement AddDupeCase;
		@TextType()
		@FindBy(xpath = "//table/tbody/tr[2]/th/div/div/input[1]")
		public WebElement AddDupeCase1;
		@LinkType()
		@FindBy(xpath = "(//div/a[@class='case-number-link'])[2]")
		public WebElement CaseNumber;
		@LinkType()
		@FindBy(xpath = "(//div/a[@class='case-number-link'])[2]")
		public WebElement CaseNumber1;
	}

	@FindBy(xpath = "//iframe[@class='isView reportsReportBuilder']")
	public Frame frame;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Reply' and @role='button']")
	public WebElement Mailreply;
	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='Email to case verification-Automation' and @target])[1]")
	public WebElement EmailSubject;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='AutomationTest <gardastest@gmail.com>']")
	public WebElement fromName;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='CompletedSend case emails/files to M-Files' and contains(@class,'bare') and @type='button']")
	public WebElement SendCaseEmailsFilesToMFiles;
	@ButtonType()
	@FindBy(xpath = "//div/div/article/div/div/div/button[normalize-space(.)='Convert emails to files' and contains(@class,'slds-button') and @type='button']")
	public WebElement convertEmailsToFiles;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneWorkspace') and (ancestor::div[contains(@class,'active') and contains(@class,'main-content')]//div[contains(@class, 'oneGlobalNav') or contains(@class, 'tabBarContainer')]//div[contains(@class, 'tabContainer') and contains(@class, 'active')] )]//button[normalize-space(.)='Next']")
	public WebElement next;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Select All']/span[contains(@class,'slds-checkbox_faux')]")
	public WebElement SelectAll;
	@TextType()
	@FindBy(xpath = "//lightning-formatted-text[normalize-space(.)='3']")
	public WebElement NumberodAttachment3;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Submit' and contains(@class,'slds-button') and @type='button']")
	public WebElement submit;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneWorkspace') and (ancestor::div[contains(@class,'active') and contains(@class,'main-content')]//div[contains(@class, 'oneGlobalNav') or contains(@class, 'tabBarContainer')]//div[contains(@class, 'tabContainer') and contains(@class, 'active')] )]//button[normalize-space(.)='Finish']")
	public WebElement finish;
	@LinkType()
	@FindBy(xpath = "//span[text()='4 items • Sorted by Last Modified • ']")
	public WebElement AttachedContentDocuments;
	@LinkType()
	@FindBy(xpath = "//a/span[normalize-space(.)='Contact Roles']/..")
	public WebElement contactRoles;
	@LinkType()
	@FindBy(xpath = "//span/a[normalize-space(.)='Nirmal Kumar']")
	public WebElement ContactName;
	@LinkType()
	@FindBy(xpath = "//div[@class='slds-col slds-no-flex slds-grid slds-align-top slds-p-bottom--xx-small test-lvmForceActionsContainer']/descendant::a/div[text()='New']")
	public WebElement NewContactRoles;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Contact*']/following-sibling::div//input[@placeholder='Search Contacts...' and @title='Search Contacts' and @type='text']")
	public WebElement contact;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='--None--' and contains(@class,'select') and @role='button']")
	public WebElement role;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Accounting' and @title='Accounting']")
	public WebElement Role_accounting;
	@ButtonType()
	@FindBy(xpath = "//button/span[text()='Save & New']/following::button/span[text()='Save']")
	public WebElement saveButton;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contact RolesContact Roles' and @role='tab' and @title='Contact Roles']")
	public WebElement ContactRolesTab;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Waiting for customer']")
	public WebElement Waiting_for_customer;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Mark as Current Status' and contains(@class,'slds-button') and @data-aura-class='uiButton' and @type='button']")
	public WebElement MarkAsCurrentStatus;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Reasons for Waiting for Customer']/following-sibling::div//input")
	public WebElement reasonsForWaitingForCustomer;
	@TextType()
	@FindBy(xpath = "//span[text()='Checklist incomplete' and @title='Checklist incomplete']")
	public WebElement ChecklistIncomplete;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Done' and contains(@class,'slds-button') and @type='submit']")
	public WebElement Done;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Category: These required fields must be completed: Policy Year, Category']")
	public WebElement RequiredFieldErr;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Close' and contains(@class,'slds-button') and @type='button']")
	public WebElement close;
	@ButtonType()
	@FindBy(xpath = "//span[text()='Edit Product Areas']/../span[1]")
	public WebElement Edit_Product_Areas;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='*Product Areas']/following-sibling::div//input")
	public WebElement Product_Areas_Dropdown;
	@TextType()
	@FindBy(xpath = "//span[text()='Marine' and @title='Marine']")
	public WebElement Product_Area_Marine;
	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='Team--None--CT South & WestCT AsiaTrading CertificatesCT AccountingCT NorthView all dependencies']//button[normalize-space(.)='View all dependencies' and contains(@class,'slds-button') and @type='button']")
	public WebElement viewAllDependencies;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Team']/following-sibling::div//input[1]")
	public WebElement Team__DD;
	@TextType()
	@FindBy(xpath = "(//span[text()='Trading Certificates'])[1]")
	public WebElement Team_TradingCertificates;
	@ButtonType()
	@FindBy(xpath = "//button[@name='apply']")
	public WebElement save;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Category']/following-sibling::div//input[1]")
	public WebElement Category_DD;
	@TextType()
	@FindBy(xpath = "//span[text()='New customer (Marine)' and @title='New customer (Marine)']")
	public WebElement Category_New_Customer;
	@ButtonType()
	@FindBy(xpath = "//button[text()='Save' and @type='button']")
	public WebElement SaveButton;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='*Policy Year']/following-sibling::div//input[1]")
	public WebElement Policy_Year_DD;
	@TextType()
	@FindBy(xpath = "//span[text()='2020']")
	public WebElement PolicyYear_2020;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Closed' and @role='option' and @title='Closed']")
	public WebElement PathAssistantStep_Closed;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Select Closed Status' and contains(@class,'slds-button') and @type='button']")
	public WebElement PathAssistantStep_SelectClosedStatus;
	@ChoiceListType()
	@FindBy(xpath = "//label[normalize-space(.)='Status*']/following-sibling::select")
	public WebElement status_CLose;
	@ChoiceListType()
	@FindBy(xpath = "//select/option[normalize-space(.)='Closed']")
	public WebElement status_CLosed;
	@ChoiceListType()
	@FindBy(xpath = "//option[text()='Closed']")
	public WebElement status_Close;
	@LinkType()
	@FindBy(xpath = "(//a[@class='tabHeader slds-tabs--default__link slds-p-right--small slds-grow '])[2]/span[2]")
	public WebElement FirstTabNavigation;
	@LinkType()
	@FindBy(xpath = "//button[normalize-space(.)='Find Duplicates']")
	public WebElement FindDuplicates;
	@TextType()
	@FindBy(xpath = "//div[@id='1criteriaValueContainer']/input")
	public WebElement SearchSubjectVerification;
	@PageFrame()
	public static class Frame1 {

		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Merge' and @type='button']")
		public WebElement MergeBtn;
	}

	@FindBy(xpath = "//iframe[@title='Find Duplicates ~ Salesforce - Performance Edition']")
	public Frame1 frame1;
	@PageFrame()
	public static class DupePage_frame {

		@TextType()
		@FindBy(xpath = "//table/tbody/tr[2]/th/div/div/label[1]")
		public WebElement AddDupeCase;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Merge' and @type='button']")
		public WebElement MergeBtn;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Find']")
		public WebElement findBtm;
		@ButtonType()
		@FindBy(xpath = "(//span[text()='Delete'])[2]")
		public WebElement delete2ndRow;
		@ButtonType()
		@FindBy(xpath = "(//span[text()='Delete'])[2]/..")
		public WebElement delete2ndRow1;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Find']")
		public WebElement findBtn;
	}

	@FindBy(xpath = "//iframe[@title='Find Duplicates ~ Salesforce - Performance Edition']")
	public DupePage_frame dupePage_frame;
	@PageFrame()
	public static class MergeCase {

		@LinkType()
		@FindBy(xpath = "(//div/a[@class='case-number-link'])[2]")
		public WebElement CaseNumber;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Set as Primary' and @type='button']")
		public WebElement setAsMaster;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Merge' and @id='merge-btn' and @type='button']")
		public WebElement MergeButton;
		@ButtonType()
		@FindBy(xpath = "//footer/button[normalize-space(.)='Merge' and contains(@class,'modal-footer-btn') and @type='button']")
		public WebElement mergeConfirmation;
		@ButtonType()
		@FindBy(xpath = "//span[normalize-space(.)='Clean Merge']/following::button[1]")
		public WebElement CleanMergeMessage;
		@TextType()
		@FindBy(xpath = "(//div/div[@class='slds-popover__body'])[1]")
		public WebElement CleanMergeCheckboxMessage;
		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='Unselect All']")
		public WebElement unselectAllObjects;
		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='Select All']")
		public WebElement selectAllObject;
		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='00663784'][1]")
		public WebElement CaseNumber1;
	}

	@FindBy(xpath = "//iframe[@title='Salesforce - Performance Edition']")
	public MergeCase mergeCase;
	@TextType()
	@FindBy(xpath = "//slot[@slot='primaryField']/lightning-formatted-text[1]")
	public WebElement MYOpenCase_CaseNumber;
	@LinkType()
	@FindBy(xpath = "(//span[text()='View All'])[2]")
	public WebElement ViewAllAttachment;

	@LinkType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//div[13]//a[normalize-space(.)='--None--']")
	public WebElement VATApplicable;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Yes']")
	public WebElement yes;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='These required fields must be completed: Policy Year, Product Areas']")
	public WebElement CategorizationErrorMessage;

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"My Open Cases\"']")
	public WebElement expandMyOpenCases;
	
	@PageFrame()
	public static class DupePage_frame1 {

		@TextType()
		@FindBy(xpath = "//table/tbody/tr[2]/th/div/div/label[1]")
		public WebElement AddDupeCase;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Merge' and @type='button']")
		public WebElement MergeBtn;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Find']")
		public WebElement findBtm;
		@ButtonType()
		@FindBy(xpath = "(//span[text()='Delete'])[2]")
		public WebElement delete2ndRow;
		@ButtonType()
		@FindBy(xpath = "(//span[text()='Delete'])[2]/..")
		public WebElement delete2ndRow1;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Find']")
		public WebElement findBtn;
	}

	@FindBy(xpath = "//iframe[@title='Find Duplicates']")
	public DupePage_frame dupePage_frame1;
	
	@PageFrame()
	public static class EmailFrame {
	
		@TextType()
		@FindBy(xpath = "//html/body/br[1]")
		public WebElement emailBody;

	}

	@FindBy(xpath = "//iframe[@title='Email Body']")
	public EmailFrame emailFrame;
	@PageFrame()
	
	public static class MergeCase1 {

		@LinkType()
		@FindBy(xpath = "(//div/a[@class='case-number-link'])[2]")
		public WebElement CaseNumber;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Set as Master' and @type='button']")
		public WebElement setAsMaster;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Merge' and @id='merge-btn' and @type='button']")
		public WebElement MergeButton;
		@ButtonType()
		@FindBy(xpath = "//footer/button[normalize-space(.)='Merge' and contains(@class,'modal-footer-btn') and @type='button']")
		public WebElement mergeConfirmation;
		@ButtonType()
		@FindBy(xpath = "//span[normalize-space(.)='Clean Merge']/following::button[1]")
		public WebElement CleanMergeMessage;
		@TextType()
		@FindBy(xpath = "(//div/div[@class='slds-popover__body'])[1]")
		public WebElement CleanMergeCheckboxMessage;
		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='Unselect All']")
		public WebElement unselectAllObjects;
		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='Select All']")
		public WebElement selectAllObject;
	}

	@FindBy(xpath = "(//iframe[@title='accessibility title'])[2]")
	public MergeCase mergeCase1;
	@PageFrame()
	public static class Frame2 {
	}

	@FindBy(xpath = "//div[contains(@class, \"oneAlohaPage\")]/force-aloha-page/div//iframe")
	public Frame2 frame2;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Change Owner' and @name='ChangeOwnerOne' and @type='button']")
	public WebElement ChangeOwnerOne;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search Users...']")
	public WebElement SearchUser;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Reply']")
	public WebElement replyLink;
	@TextType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneWorkspace') and (ancestor::div[contains(@class,'active') and contains(@class,'main-content')]//div[contains(@class, 'oneGlobalNav') or contains(@class, 'tabBarContainer')]//div[contains(@class, 'tabContainer') and contains(@class, 'active')] )]//button[normalize-space(.)='Send']")
	public WebElement publishersharebutton;
	@TextType()
	@FindBy(xpath = "//a[text()='Email']")
	public WebElement EmailBtn;
	@TextType()
	@FindBy(xpath = "//a[text()='Blank Email']")
	public WebElement BlankEmailBtn;

			
}
