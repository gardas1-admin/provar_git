package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardOpenClaimsClaimDetails"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGarOpenClaiamsClaimDetails {

	@TextType()
	@FindBy(xpath = "//label[text()='Gard reference no.:']/following-sibling::div[1]")
	public WebElement GardReferenceNo;
	@LinkType()
	@FindBy(xpath = "//span[text()='Reserve:']/ancestor::label[1]/following-sibling::div[1]/descendant::span[1]")
	public WebElement ClaimReserveAmt;
			
}
