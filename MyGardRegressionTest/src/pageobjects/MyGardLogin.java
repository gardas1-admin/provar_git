package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardLogin"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class MyGardLogin {

	@TextType()
	@FindBy(xpath = "//label[text()='User name:']/following-sibling::input[1]")
	public WebElement UserName;
	@TextType()
	@FindBy(xpath = "//label[text()='Password:']/following-sibling::input[1]")
	public WebElement Password;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Log in']")
	public WebElement LogInButton;
	@ButtonType()
	@FindBy(xpath = "//div/input[contains(@class,'btn') and @type='button' and @value='Ok']")
	public WebElement CommunityOk;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard' and @href='https://mygard.force.com/mygard/gardlogin']")
	public WebElement myGard;
	@TextType()
	@FindBy(xpath = "//td/div[contains(@class,'messageText')]")
	public WebElement Error;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Remember me']")
	public WebElement RememberMe;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='Please enter a valid user name. (User names in MyGard are email address plus “.mygard“)']")
	public WebElement MyGardExtnError;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Forgot your password?']")
	public WebElement forgotPassword;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='User name:']/parent::div/following-sibling::div[1]//input")
	public WebElement submit;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Forgot your user name?']")
	public WebElement forgotUser;
	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div[1]")
	public WebElement theForm;
	@ButtonType()
	@FindBy(xpath = "//div/div/input")
	public WebElement close;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Back to login']")
	public WebElement backToLogin;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Remember me']/parent::div/following-sibling::div//input")
	public WebElement next;
			
}
