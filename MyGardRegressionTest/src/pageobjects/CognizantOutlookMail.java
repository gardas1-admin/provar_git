package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="CognizantOutlookMail"                                
     , summary=""
     , relativeUrl=""
     , connection="GardMail"
     )             
public class CognizantOutlookMail {

	@TextType()
	@FindBy(xpath = "//input[@name='loginfmt' and @placeholder='Email or phone' and @type='email']")
	public WebElement OutlookemailOrPhone;
	@TextType()
	@FindBy(xpath = "//input[@id='userNameInput' and @name='UserName' and @placeholder='Associate ID' and @type='text']")
	public WebElement associateID;
	@TextType()
	@FindBy(xpath = "//input[@id='passwordInput' and @title='Enter password' and @type='Password']")
	public WebElement password;
	@ButtonType()
	@FindBy(xpath = "//input[@id='Log_On1' and @type='submit']")
	public WebElement lOGON;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='New message' and @type='button']")
	public WebElement newMessage;
	@TextType()
	@FindBy(xpath = "//div[text()='To']/following::input[1]")
	public WebElement ToAddress;
	@TextType()
	@FindBy(xpath = "//input[@id='subjectLine0']")
	public WebElement addASubject;
	@ButtonType()
	@FindBy(xpath = "//button[@title='Send (Ctrl+Enter)' and @type='button']/span[1]")
	public WebElement send;
	@TextType()
	@FindBy(xpath = "//input[@aria-label='Add a subject' and @placeholder='Add a subject' and @type='text']")
	public WebElement addASubject1;
			
}
