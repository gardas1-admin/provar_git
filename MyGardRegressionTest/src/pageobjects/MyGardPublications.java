package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardPublications"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardPublications {

	@TextType()
	@FindBy(xpath = "//h1[text()='Publications']")
	public WebElement Publications;
			
}
