package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="desktopDashboards__embeddedDashboard"                                
               , summary=""
               , connection="MyGardRegressionQABox"
               , lightningComponent="embeddedDashboard"
               , namespacePrefix="desktopDashboards"
     )             
public class desktopDashboards__embeddedDashboard {

	@PageFrame()
	public static class Frame {

		@ButtonType()
		@FindByLabel(label = "Expand \"New Unassigned Cases each Queue\"")
		public WebElement expandNewUnassignedCasesEachQueue;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"New Unassigned Cases each Queue\"' and contains(@class,'slds-button') and @title='Expand' and @type='button']")
		public WebElement expandNewUnassignedCasesEachQueue1;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Close' and contains(@class,'slds-button') and @title='Close' and @type='button' and @title='Close']")
		public WebElement closeUnassignedCase;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Open' and contains(@class,'slds-button') and @type='button']")
		public WebElement open;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"My Open Cases\"']")
		public WebElement expandMyOpenCases;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"Closed Cases Today\"']")
		public WebElement expandClosedCasesToday;
	}

	@FindBy(css = "//iframe[@title='dashboard']")
	public Frame frame;

	@PageFrame()
	public static class Frame1 {

		@TextType()
		@FindBy(xpath = "//span[normalize-space(.)='Customer Transactions Main']")
		public WebElement DashBoard_CTMain;
	}

	@FindBy(css = "//iframe[@title='dashboard']")
	public Frame1 frame1;

	@PageFrame()
	public static class Frame2 {
	}

	@FindBy(xpath = "//iframe[]")
	public Frame2 frame2;

	@PageFrame()
	public static class Frame3 {
	}

	@FindBy(xpath = "//iframe[]")
	public Frame3 frame3;
	
}
