package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardProduct"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardProduct {

	@TextType()
	@FindBy(xpath = "//h1[text()='Products']")
	public WebElement Products;
			
}
