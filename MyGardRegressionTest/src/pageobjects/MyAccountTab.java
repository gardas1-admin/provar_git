package pageobjects;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import com.provar.core.model.ui.api.UiFacet;
import com.provar.core.testapi.annotations.*;

@Page( title="MyAccountTab"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class MyAccountTab {


WebDriver driver;
@TestLogger
	public Logger testLogger;

	public MyAccountTab(WebDriver driver) {

		this.driver = driver;
	}

	public void actionClick(String elementXpath, String elementName) {

		testLogger.info("Working with " + elementName);
		WebElement e = driver
				.findElement(By.xpath(elementXpath));

		Actions actions = new Actions(driver);
		actions.moveToElement(e).click().build().perform();

	}


	@TextType()
	@FindBy(xpath = "//div[@title='My account']")
	public WebElement MyAccountIcon;
	@LinkType()
	@FindBy(xpath = "//a[text()='Activity feed']")
	public WebElement ActivityFeed;
	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following::button[1]")
	public WebElement crownShipBuildersCorp;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[2]")
	public WebElement SetClient;
	@ButtonType()
	@FindBy(xpath = "//label[text()='From']/following::button[text()='Pick a date'][1]")
	public WebElement FromDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/table/tbody/tr/td[@class='dp_current']")
	public WebElement TodayFromDate;
	@ButtonType()
	@FindBy(xpath = "//label[text()='To']/following::button[text()='Pick a date'][1]")
	public WebElement ToDate;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Go' and @class='tab-filter-btn']")
	public WebElement GoFilter;
	@ButtonType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::td[@class='dp_current']/following::td[1]")
	public WebElement ToDateTmrw;
	@ButtonType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::td[@class='dp_current']/following::td[1]")
	public WebElement ToDateTmrw1;
	@PageRow()
	public static class ActivityFeed {

		@TextType()
		@FindBy(xpath = "(//div[@class='client-list-section']/table/tbody/tr/td[3])[1]")
		public WebElement activityType;
		@TextType()
		@FindBy(xpath = "//div[contains(@class, \"client-list-section\")]/table/tbody/tr[1]/td[2]")
		public WebElement userName;
		@TextType()
		@FindBy(xpath = "//tr[contains(@class, \"odd\")]//td[4]")
		public WebElement ObjectVerification;
		@TextType()
		@FindBy(xpath = "//tr[contains(@class, \"odd\")]//td[5]")
		public WebElement CoverVerification;
	}
	@FacetFindBys(value = { @FacetFindBy(findBy = @FindBy(xpath = ".//tr"), facet = UiFacet.DATA_ROWS) })
	@FindBy(xpath = "//div[contains(@class, \"client-list-section\")]//table")
	@PageTable(firstRowContainsHeaders = true, row = ActivityFeed.class)
	public List<ActivityFeed> ActivityFeed1;
	@ButtonType()
	@FindBy(xpath = "//input[@type='submit' and @value='Clear' and contains(@class,'form-btn')]")
	public WebElement ClearButton;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='7']")
	public WebElement TodayFromDate1;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[3]")
	public WebElement filter2ndClient;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[2]")
	public WebElement filter1stClient;
	@TextType()
	@FindBy(css = "td.dp_current")
	public WebElement TodayFromDate2;
	@TextType()
	@FindBy(css = "td.dp_weekend.dp_hover")
	public WebElement ToDateTmrw2;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='21']")
	public WebElement TodayFromDate3;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[4]")
	public WebElement filterClient3rd;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Pacific Gas Cha…']")
	public WebElement PacificGas;
	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[5]")
	public WebElement filterClient4th;
			
}
