package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardEmergency"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardEmergency {

	@TextType()
	@FindBy(xpath = "//h1[text()='Emergency']")
	public WebElement EmergencyText;
			
}
