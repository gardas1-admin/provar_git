package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardCreateNewShare"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardCreateNewShare {

	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement NewShareClientPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::div[1]/descendant::div[1]/descendant::input[1]")
	public WebElement NewShareClientPicklistSearchClient;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Client:')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement NewShareClientPicklistSearchedResult;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Business area:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement NewShareBuisnessAreaPicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Business area')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement NewShareBusinessAreaFirstChecklist;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Policy year:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement NewSharePolicyYearPicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Policy year:')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement NewSharePolicyYearFirstChecklist;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement NewShareObjectPicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object:')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement NewShareObjectFirstCheckbox;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Object:')]/ancestor::span[1]/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewShareGoBtn;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Target company:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement NewShareTargetCompanyPicklist;
	@TextType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Target company:')]/following-sibling::div[1]/descendant::div[1]/descendant::input[1]")
	public WebElement NewShareTargetCompanyPicklistSearch;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Target company:')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/descendant::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement NewShareTargetCompanySearchedResult;
	@TextType()
	@FindBy(xpath = "//label[text()='Selected contacts']")
	public WebElement SelectedContactsRadioBtn;
	@ButtonType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Target contacts:')]/following-sibling::div[1]/descendant::div[1]")
	public WebElement NewShareTagetContactsPicklist;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Target contacts:')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/following-sibling::ul[1]/descendant::li[2]/descendant::span[1]")
	public WebElement NewShareTargetcontactsFirstCheckbox;
	@LinkType()
	@FindBy(xpath = "//label[contains(normalize-space(text()),'Target contacts:')]/following-sibling::div[1]/descendant::div[1]/descendant::div[1]/following-sibling::ul[1]/descendant::li[3]/descendant::span[1]")
	public WebElement NewShareTargetContactSecondCheckbox;
	@TextType()
	@FindBy(xpath = "//label[text()='Share name:']/following-sibling::input[1]")
	public WebElement NewShareShareName;
	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Cover')]/ancestor::th[1]/following-sibling::th[1]/descendant::div[2]")
	public WebElement NewShareAgreementCheckbox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Next']")
	public WebElement CreateNewNextBtn;
	@ButtonType()
	@FindBy(xpath = "//input[@class='form-btn rvw-sub-multi']")
	public WebElement NewShareSubmitBtn;
	@TextType()
	@FindBy(xpath = "//div[text()='Thank you for contacting Gard. Your request has been successfully registered.']")
	public WebElement NewShareConfirmationMessage;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Thank you for contacting Gard. Your request has been successfully registered.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewShareConfirmationOkBtn;
	@TextType()
	@FindBy(xpath = "//td[text()='Active']")
	public WebElement SharedDataStatusActive;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Review and submit']")
	public WebElement NewShareReviewAndSubmitBtn;
	@TextType()
	@FindBy(xpath = "//div[text()='Thank you for contacting Gard. Your request has been successfully modified.']")
	public WebElement NewShareModificationConfirmationMessage;
	@ButtonType()
	@FindBy(xpath = "//div[text()='Thank you for contacting Gard. Your request has been successfully modified.']/following-sibling::div[1]/descendant::input[1]")
	public WebElement NewShareModificationConfirmationOkBtn;
			
}
