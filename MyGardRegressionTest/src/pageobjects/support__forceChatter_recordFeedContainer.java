package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="support__forceChatter_recordFeedContainer"                                
               , summary=""
               , connection="MyGardE2E"
               , object=""
     )             
public class support__forceChatter_recordFeedContainer {

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Reply']")
	public WebElement ReplyLink;
	
}
