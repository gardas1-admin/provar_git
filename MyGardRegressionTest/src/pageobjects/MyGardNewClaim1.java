package pageobjects;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page(title = "MyGardNewClaim1", summary = "", relativeUrl = "", connection = "MyGardE2E")
public class MyGardNewClaim1 {

	WebDriver driver;

	public MyGardNewClaim1(WebDriver driver) {

		this.driver = driver;

	}

	@FindBy(xpath = "//button[contains(@data-id,'client-field')]//span[1]")
	public WebElement clientDropD;

	public void clickDropDown() {

		WebElement e = driver.findElement(By.xpath("//button[contains(@data-id,'client-field')]//span[1]"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", e);

	}

}
