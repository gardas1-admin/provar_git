package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCompanies"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceCompanies {

	@TextType()
	@FindBy(xpath = "//h2[normalize-space(.)='Related List Quick Links']")
	public WebElement RelatedListQuickLinks;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(.),'Related Contacts')]")
	public WebElement RelatedContactsLink;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Select List View']")
	public WebElement searchType;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='SelectedAll Brokers']")
	public WebElement selectedAllBrokers;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='No items to display.']")
	public WebElement NoItemsToDisplay;
	@LinkType()
	@FindBy(xpath = "(//a[contains(normalize-space(.),'Contacts')])[3]")
	public WebElement Contacts;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Companies']")
	public WebElement Companies_Link;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Related']")
	public WebElement Related_Link;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(.),'Customer Feedback from this company')]")
	public WebElement Customer_Feedbacks;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class, \"empty\")]/div[1]/div[1]/div[2]/ul/li[1]//a")
	public WebElement FeedbackNew;
	@LinkType()
	@FindBy(xpath = "//li/a/div[text()='Change Owner']/ancestor::a/../../li/a/div[text()='New']")
	public WebElement New_Feedback;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(.),'Customer Feedback to this company')]")
	public WebElement Customer_Feedback__r;
	@TextType()
	@FindBy(xpath = "//input[@name='Account-search-input' and @placeholder='Search this list...' and @type='search']")
	public WebElement searchBox;
	@LinkType()
	@AuraBy(componentXPath = "//ui:virtualDataTable[@aura:id= 'grid']")
	public WebElement Petro_Services_Congo;
	@LinkType()
	@FindBy(xpath = "//span/a[normalize-space(.)='AKOFS Offshore AS' and contains(@class,'slds-truncate') and @data-aura-class='forceOutputLookup' and @title='AKOFS Offshore AS']")
	public WebElement Petro_Services_Congo1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Covers on risk report' and @title='Covers on risk report']")
	public WebElement coversOnRiskReport;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Open Claims report' and @title='Open Claims report']")
	public WebElement openClaimsReport;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Open Claims amount per type' and @title='Open Claims amount per type']")
	public WebElement openClaimsAmountPerType;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Open Claims percentage per type' and @title='Open Claims percentage per type']")
	public WebElement openClaimsPercentagePerType;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Client Claim Payment/Reserve history' and @title='Client Claim Payment/Reserve history']")
	public WebElement clientClaimPaymentReserveHistory;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contacts and their subscriptions' and @title='Contacts and their subscriptions']")
	public WebElement contactsAndTheirSubscriptions;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contacts and events'and @title='Contacts and events']")
	public WebElement contactsAndEvents;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard activities' and @title='MyGard activities']")
	public WebElement myGardActivities;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard brokers shared access' and @title='MyGard brokers shared access']")
	public WebElement myGardBrokersSharedAccess;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard login (only for System Admin)'  and @title='MyGard login (only for System Admin)']")
	public WebElement myGardLoginOnlyForSystemAdmin;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Ship Insurers Ltd.' and contains(@class,'slds-truncate') and @data-aura-class='forceOutputLookup' and @title='Ship Insurers Ltd.']")
	public WebElement Ship_Insurers_Ltd_;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Clients and covers on risk report' and @title='Clients and covers on risk report']")
	public WebElement clientsAndCoversOnRiskReport;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Clients and open claims report' and @title='Clients and open claims report']")
	public WebElement clientsAndOpenClaimsReport;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='All Opportunities' and @title='All Opportunities']")
	public WebElement allOpportunities;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard clients shared access' and @title='MyGard clients shared access']")
	public WebElement myGardClientsSharedAccess;
	@TextType()
	@FindBy(xpath = "//span[text()='Set up an event...']")
	public WebElement SetupAnEvent;
	@TextType()
	@FindBy(xpath = "(//a[text()='TestAutomation' and @title='TestAutomation'])[1]")
	public WebElement EventSubject;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Edit' and @role='button' and @title='Edit']")
	public WebElement edit;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Cancel' and contains(@class,'slds-button') and @data-aura-class='uiButton forceActionButton' and @title='Cancel' and @type='button']")
	public WebElement cancel;
	@LinkType()
	@FindBy(xpath = "(//a/slot/span[contains(normalize-space(.),'Contacts')])[1]")
	public WebElement Contacts1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New' and contains(@class,'forceActionLink') and @data-aura-class='forceActionLink' and @data-aura-rendered-by='13006:0' and @href='javascript:void(0);' and @role='button' and @title='New']")
	public WebElement COntactNew;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Next' and @type='button']")
	public WebElement next;
	@LinkType()
	@FindBy(xpath = "//span[@id='hideFeedLink']/a")
	public WebElement hideFeed;
	@LinkType()
	@FindBy(xpath = "//div[@id='acc1_ileinner']/span/a[2]")
	public WebElement OwnerId;
	@BooleanType()
	@FindBy(xpath = "(//span[text()='Correspondent Contact']/../../div/span)[1]")
	public WebElement CorrespondentContactReadOnly;
	@LinkType()
	@FindBy(xpath = "//span/span[text()='LoC Order']/following::a[1]")
	public WebElement LoC_Order__c;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='1']")
	public WebElement LoCOrderValue;
	@TextType()
	@FindBy(xpath = "//div[@class='pageLevelErrors']/div/ul/li[1]")
	public WebElement CorrespondingContactErrors;
	@LinkType()
	@FindBy(xpath = "//a[text()='Contacts']/following::table/tbody/tr/th/span/a[1]")
	public WebElement ContactName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Search by object type']/parent::lightning-grouped-combobox/parent::div/following-sibling::div//input")
	public WebElement searchSalesforce;
	@LinkType()
	@FindBy(xpath = "//button[normalize-space(.)='Log in to Experience as User' and @type='button']")
	public WebElement logInToCommunityAsUser;
	@LinkType()
	@FindBy(xpath = "//li/a[normalize-space(.)=concat('Nathan O',\"'\",'Connor') and @title=concat('Nathan O',\"'\",'Connor')]")
	public WebElement ReturnToContactPage;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='New' and @title='New']")
	public WebElement NewCompany;
	@BooleanType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//label[normalize-space(.)='Bank']/div[1]/span")
	public WebElement RecordType_Bank;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='VAT number']/following::input[1]")
	public WebElement VAT_number;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Local Registry ID']/following::input[1]")
	public WebElement localRegistryID;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='InsureWave ID']/following::input[1]")
	public WebElement insureWaveID;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='*Company Name']/following::input[1]")
	public WebElement CompanyName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='*Office Location']/following::input[1]")
	public WebElement Office;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='*Country']/following::input[1]")
	public WebElement Country_c;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='LEI code']/following::input[1]")
	public WebElement LEICode;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='BIC Code']/following::input[1]")
	public WebElement BICCode;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Responsible Underwriter']/following::input[1]")
	public WebElement Underwriter_Responsible;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Responsible VP']/following::input[1]")
	public WebElement Area_Manager_Responsible_vp;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='BvD Company name']/following::input[1]")
	public WebElement BvD_Company_name__c;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='BvD Id']/following::input[1]")
	public WebElement BvD_ID;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='KYC exception approved (DMS ref)']/following::input[1]")
	public WebElement KYC_exception_DMS;
	@FindBy(xpath = "//button[@title='Save' and @type='button']")
	@ButtonType()
	public WebElement Save;
	@LinkType()
	@FindBy(xpath = "//th/span/a[normalize-space(.)='AKOFS Offshore AS' and contains(@class,'slds-truncate')]")
	public WebElement ACCOUNT_NAME_AKOFS;
	@LinkType()
	@FindBy(xpath = "//span[contains(text(),'Related companies (Member)')]")
	public WebElement Related_companies1__r;
	@FindBy(xpath = "//a[contains(text(),'Show All')]")
	@PageWaitAfter.Field(timeoutSeconds = 10)
	@LinkType()
	public WebElement Show_all_less;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Company']/following-sibling::div//input[@placeholder='Search Companies...']")
	public WebElement Company__c;
	@LinkType()
	@FindBy(xpath = "(//table/tbody/tr/td[1]/a[@target='_blank'])[1]")
	public WebElement SelectCompany;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='Ready for approval' and contains(@class,'select')]")
	public WebElement Status__c;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Approved' and @role='menuitemradio' and @title='Approved']")
	public WebElement approved;
	@TextType()
	@FindBy(xpath = "//ul[@class='branding-actions slds-button-group slds-m-left--xx-small small oneActionsRibbon forceActionsContainer']/li[1]/a[1]")
	public WebElement NewRelatedCompany;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//button[@name='SaveEdit']")
	public WebElement RCSave;
	@LinkType()
	@FindBy(xpath = "(//table)[2]/tbody/tr[1]/td[9]/span/div/a[1]")
	public WebElement ShowMore;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Delete' and @title='Delete']")
	public WebElement Delete;
	@LinkType()
	@FindBy(xpath = "//button/span[text()='Delete']")
	public WebElement DeleteConfirm;
	@LinkType()
	@FindBy(xpath = "//span/a[normalize-space(.)='TestAutomationNew' and @title='TestAutomationNew']")
	public WebElement testAutomationNew;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contacts(1)' and contains(@class,'rlql-relatedListLink')]")
	public WebElement Contacts2;
	@LinkType()
	@FindBy(xpath = "//th/span/a[normalize-space(.)='Nirmal Kumar']")
	public WebElement Contact_Nirmalkumar;
	@LinkType()
	@FindBy(xpath = "//span[text()='Follow']/following::li[1]/a")
	public WebElement editContact;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='No Longer Employed by Company?Check if the Person has left the company. Also remember to delete the contact from the ACTIVE events and campaigns']/following-sibling::input[@type='checkbox']")
	public WebElement No_Longer_Employed_by_Company__c;
	@TextType()
	@FindBy(xpath = "//div[@class='desktop forcePageError']/ul[1]/li[1]")
	public WebElement ContactErrors_PCasNLE;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New' and @role='button' and @title='New']")
	public WebElement New;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Clients and covers on risk report' and @title='Clients and covers on risk report']")
	public WebElement Clients_and_covers_on_risk_report;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Clients and open claims report' and @title='Clients and open claims report']")
	public WebElement Clients_and_open_claims_report;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='All Opportunities' and @title='All Opportunities']")
	public WebElement All_Opportunities;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contacts and their subscriptions' and @title='Contacts and their subscriptions']")
	public WebElement Contacts_and_their_subscriptions;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contacts and events' and @title='Contacts and events']")
	public WebElement Contacts_and_events;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard clients shared access' and @title='MyGard clients shared access']")
	public WebElement MyGard_clients_shared;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard login (only for System Admin)' and @title='MyGard login (only for System Admin)']")
	public WebElement MyGard_login_only_for_System_Admin;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Covers on risk report' and @title='Covers on risk report']")
	public WebElement Covers_on_risk_report;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Open Claims report' and @title='Open Claims report']")
	public WebElement Open_Claims_report;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Open Claims amount per type' and @title='Open Claims amount per type']")
	public WebElement Open_claims_type_amount;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Open Claims percentage per type' and @title='Open Claims percentage per type']")
	public WebElement Open_claims_type_percentage;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Client Claim Payment/Reserve history' and @title='Client Claim Payment/Reserve history']")
	public WebElement Client_Claim_Payment_Reserve_history;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard activities' and @title='MyGard activities']")
	public WebElement MyGard_activities;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard brokers shared access' and @title='MyGard brokers shared access']")
	public WebElement MyGard_brokers_shared;
	@LinkType()
	@FindBy(xpath = "(//div/a/span[contains(text(),\"Contacts\")])[1]")
	public WebElement Contacts3;
	@LinkType()
	@FindBy(xpath = "(//div/a/span[contains(text(),\"Contacts\")])[1]")
	public WebElement Contacts31;
	@LinkType()
	@FindBy(xpath = "(//div/a/span[contains(text(),\"Contacts\")])[1]")
	public WebElement Contacts311;
	@LinkType()
	@FindBy(xpath = "//span/a[normalize-space(.)='Crown Ship Builders Corp']")
	public WebElement ACCOUNT_NAME_Crown_Ship;
	@BooleanType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//label[normalize-space(.)='External Service ProviderSelect to create a new company for an ESP e.g. Lawyer']/div[1]/span")
	public WebElement RecordType;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//span/span[normalize-space(.)='ESP - Media Consultant']")
	public WebElement Sub_Roles__c;
	@BooleanType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//input[@id='checkbox-406']")
	public WebElement Block_for_payment__c;
	@TextType()
	@FindBy(xpath = "//lightning-icon[@title='Error']")
	public WebElement errorIcon;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Search...']")
	public WebElement search;
	@TextType()
	@FindBy(xpath = "//input[@id='input-103']")
	public WebElement search1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Customer Transactions broker cases']")
	public WebElement Customer_Transactions_broker_cases;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MyGard broker activities']")
	public WebElement MyGard_broker_activities;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Customer Transactions customer cases']")
	public WebElement Customer_Transaction_customer_cases;
	@LinkType()
	@FindBy(xpath = "//a[text()='Contacts']/following::table[1]/tbody/tr/th/span/a[1]")
	public WebElement Contact;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//a[normalize-space(.)='Petro Services Congo']")
	public WebElement PetroServiceCongoCompany_Client;
	@TextType()
	@FindBy(xpath = "//div[text()='Account name must be less than 120 characters']")
	public WebElement CompanyNameLengthError;
	@TextType()
	@FindBy(xpath = "//label[text()='Topics']/following::input[1]")
	public WebElement ActivityTabTopics;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Topics (0)']")
	public WebElement Topics0;
	@TextType()
	@FindBy(xpath = "//div[text()='Custom permission \"Edit_BvD_ID\" is required to modify existing BvD ID.']")
	public WebElement BvD_ID_Custome_Error;
	@TextType()
	@FindBy(xpath = "//div[text()='Please enter a state for the US company.']")
	public WebElement USStateErrorMessage;
			
}
