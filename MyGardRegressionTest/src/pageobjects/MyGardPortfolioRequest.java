package pageobjects;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardPortfolioRequest"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class MyGardPortfolioRequest {


WebDriver driver;
@TestLogger
	public Logger testLogger;

	public MyGardPortfolioRequest(WebDriver driver) {

		this.driver = driver;
	}

	public void actionsClick(String elementXpath, String elementName) {

		testLogger.info("Working with " + elementName);
		WebElement e = driver.findElement(By.xpath(elementXpath));
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",e);
		js.executeScript("arguments[0].style.border='3px solid red'",e);

		Actions actions = new Actions(driver);
		actions.moveToElement(e).click().build().perform();

	}



	@LinkType()
	@FindBy(xpath = "//a[text()='MY PORTFOLIO']")
	public WebElement MyProtfolioTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='New request ']")
	public WebElement NewRequest;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to assureds']")
	public WebElement ChangeToAssureds;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Client:']/following-sibling::span//button[contains(@class,'btn') and @type='button']")
	public WebElement Client_dropdown;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Client:']/following::li[2]")
	public WebElement CrownShipBuildersCorp;
	@ButtonType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'The user is already logged in to the Community')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonLoggedIntoCommunity;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Date effective:']/following::input[1]")
	public WebElement DateEffective;
	@TextType()
	@FindBy(xpath = "//td[@class='dp_current']/following::td[2]")
	public WebElement Pick_a_future_date;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Cover:']/following::button[1]")
	public WebElement Cover;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Cover:']/following::li[2]")
	public WebElement PAndICover;
	@TextType()
	@FindBy(xpath = "//label[text()=' Comments:']/following::textarea[1]")
	public WebElement Comments;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Object name:']/following::button[1]")
	public WebElement ObjectName;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Object name:']/following::li[2]/descendant::input[1]")
	public WebElement ObjectSelection;
	@ButtonType()
	@FindBy(xpath = "(//input[@class='form-btn' and @type='button' and @value='Submit'])[2]")
	public WebElement Submit;
	@TextType()
	@FindBy(xpath = "(//div[@class='pop-up-sub-title'])[3]")
	public WebElement Success;
	@ButtonType()
	@FindBy(xpath = "(//div[@class='pop-up-sub-title'])[3]/following::input[1]")
	public WebElement Ok;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to mortgagees']")
	public WebElement ChangeToMortgagees;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to terms']")
	public WebElement ChangetoTerms;
	@LinkType()
	@FindBy(xpath = "//a[text()='Miscellaneous changes']")
	public WebElement MiscellaneousChanges;
	@LinkType()
	@FindBy(xpath = "//a[text()='Covers for new objects']")
	public WebElement CoversForNewObjects;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Product area:']/following::button[1]")
	public WebElement ProductAreaDropdown;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Product area:']/following::li[2]")
	public WebElement PAndI;
	@LinkType()
	@FindBy(xpath = "//span[text()='Bunkers Cover']")
	public WebElement BunkersCover;
	@TextType()
	@FindBy(xpath = "//label[text()=' Object name:']/following::textarea[1]")
	public WebElement ObjectName1;
	@LinkType()
	@FindBy(xpath = "//a[text()='Additional covers for existing objects']")
	public WebElement AdditionalCoversForExistingObjects;
	@LinkType()
	@FindBy(xpath = "//a[text()='Blue Card for new object']")
	public WebElement BlueCardForNewObject;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Type of Blue Card:']/following::button[1]")
	public WebElement TypeOfBlockDropdown;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Type of Blue Card:']/following::li[2]/descendant::input[1]")
	public WebElement Bunkers_BBC_;
	@TextType()
	@FindBy(xpath = "//label[text()=' Object name:']/following::input[1]")
	public WebElement ObjectName2;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Flag:']/following::button[1]")
	public WebElement FlagDropdown;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Flag:']/following::li[2]")
	public WebElement Aland;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Port of registry:']/following::button[1]")
	public WebElement PortOfReg;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Port of registry:']/following::li[2]")
	public WebElement Aalborg;
	@TextType()
	@FindBy(xpath = "//label[text()=' Call sign:']/following::input[1]")
	public WebElement CallSign;
	@TextType()
	@FindBy(xpath = "(//label[contains(text(),'Registered owner')])[1]/following::input[1]")
	public WebElement RegOwnerName;
	@TextType()
	@FindBy(xpath = "(//label[contains(text(),'Registered owner')])[1]/following::textarea[1]")
	public WebElement RegOwnerAddress;
	@LinkType()
	@FindBy(xpath = "//a[text()='Change to the Blue Card']")
	public WebElement ChangeToTheBlueCard;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Clients:']/following::button[1]")
	public WebElement Client_dropdown1;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Clients:']/following::li[2]/a[1]")
	public WebElement CrownShipSelect;
	@LinkType()
	@FindBy(linkText = "Bunkers (BBC)")
	public WebElement typeOfBlock;
	@LinkType()
	@FindBy(xpath = "//a[text()='Renewal of Blue Card']")
	public WebElement RenewalOfBlueCard;
	@ButtonType()
	@FindBy(xpath = "//label[text()=' Client :']/following::button[1]")
	public WebElement Client_dropdown_sec;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Client :']/following::li[2]")
	public WebElement client_dropdown_CSB;
	@TextType()
	@FindBy(xpath = "//label[contains(text(),'I hereby')]")
	public WebElement Acknowledgement;
	@ButtonType()
	@FindBy(xpath = "(//input[@class='form-btn' and @type='submit' and @value='Submit'])[2]")
	public WebElement submit1;
	@LinkType()
	@FindBy(xpath = "//a[text()='New MLC Certificate']")
	public WebElement NewMLCCertificate;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Effective from:']/following-sibling::span//input")
	public WebElement mlc_change_EffFrom;
	@TextType()
	@FindBy(xpath = "//td[@class='dp_current']/following::td[2]")
	public WebElement PickTodayFromDate;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Effective to:']/following-sibling::span//input")
	public WebElement mlc_change_EffTo;
	@TextType()
	@FindBy(xpath = "//td[@class='dp_disabled dp_disabled_current']/following::td[2]\n")
	public WebElement PickToDate;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Change to MLC Certificate']")
	public WebElement changeToMLCCertificate;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Change of object name']")
	public WebElement ChangeOfObj;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='New object name:']/following-sibling::input[1]")
	public WebElement change_obj_name;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='COFR letter']")
	public WebElement COFRLetter;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search by object name / IMO']")
	public WebElement objSearchCOFR;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Vespa']")
	public WebElement vespa_Obj;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Applicant/responsible operator:']/following-sibling::input[1]")
	public WebElement reqest_applicant_address;
	@LinkType()
	@FindBy(xpath = "//label[text()=' COFR guarantor:']/following::li[2]")
	public WebElement COFR_guarantor_DD;
	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div/div/div/div/input")
	public WebElement imo_number;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='BlueNose']")
	public WebElement blueNose;
	@ButtonType()
	@FindBy(xpath = "//input[@class='btn form-btn' and @type='submit' and @value='Continue']")
	public WebElement continue_;
	@TextType()
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']//textarea")
	public WebElement Comments1;
	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='Havsel'])[1]")
	public WebElement adella;
	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='Duncan Yacht'])[1]")
	public WebElement Duncan;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='IMO no.:']/following-sibling::input")
	public WebElement IMONo;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_16_container']//a[normalize-space(.)='Fjord Havsel AS']")
	public WebElement fjordHavselAS;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Client:']/following::li[3]/a")
	public WebElement client_dropdown_2nd;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_13_container']/li[3]/a[1]")
	public WebElement client_dropdown_2nd_DD;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_13_container']/li[6]/a[1]")
	public WebElement client_dropdown_5thDD;
	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_16_container']/li[3]/a[1]")
	public WebElement ClientDD2ndFilter;
	@LinkType()
	@FindBy(xpath = "//div[@class='mCSB_container']/li[3]/a[1]")
	public WebElement _ndClient;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Havsel 7817256']")
	public WebElement Havselobject;
	@ButtonType()
	@FindBy(xpath = "//button[1]")
	public WebElement ClientDropdown;
	@LinkType()
	@FindBy(xpath = "//li[3]/a[1]")
	public WebElement _rdClientSelection;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Flag:']/following::li[8]")
	public WebElement australia;
			
}
