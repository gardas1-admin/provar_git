package pageobjects;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="EnrollmentApproveRejectRequest"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class EnrollmentApproveRejectRequest {

	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Comments']/ancestor::th[1]/following-sibling::td[1]/descendant::textarea[1]")
	public WebElement Comments;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Approve']")
	public WebElement ApproveBtn;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Cancel']")
	public WebElement cancel;
			
}
