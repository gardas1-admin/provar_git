package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="CompaniesTab"                                
     , connection="MyGardRegressionUAT"
     )             
public class CompaniespTab {

	@LinkType()
	@FindBy(xpath = "//*[@id=\"Account_Tab\"]//a")
	public WebElement Companies;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"Account_Tab\"]//a")
	public WebElement Companies1;
			
}
