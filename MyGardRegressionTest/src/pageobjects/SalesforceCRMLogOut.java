package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMLogOut"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceCRMLogOut {

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='View profile']")
	public WebElement viewProfile;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Log Out']")
	public WebElement logOut;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Select item 1']/span[contains(@class,'slds-checkbox--faux')]")
	public WebElement BR_Misc;
	@TextType()
	@FindBy(xpath = "//button[normalize-space(.)='View profile']/div/span/div/span")
	public WebElement viewProfile1;
			
}
