package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCPerformEdition"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCPerformEdition {

	@TextType()
	@FindBy(xpath = "//div[@id='userNavButton']/descendant::span[1]")
	public WebElement UserMenuLink;
			
}
