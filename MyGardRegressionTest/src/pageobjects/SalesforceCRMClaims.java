package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMClaims"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceCRMClaims {

	@LinkType()
	@FindBy(xpath = "//ul[contains(@class, \"slds-button-group\")]/li[1]//a[normalize-space(.)='New' and contains(@class,'forceActionLink') and @role='button' and @title='New']")
	public WebElement NewClaim;
	@LinkType()
	@FindBy(xpath = "//label[text()='Claim origin']/following::input[1]")
	public WebElement claimOrigin;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Email']")
	public WebElement email;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim Title']/following::input[1]")
	public WebElement claimTitle;
	@TextType()
	@FindBy(xpath = "//label[text()='Claims handlers on Customer']/following::input[1]")
	public WebElement claimsHandlersOnCustomer;
	@LinkType()
	@FindBy(xpath = "//td/a[normalize-space(.)=concat('Nathan O',\"'\",'Connor') and contains(@class,'outputLookupLink') and @data-aura-class='forceOutputLookup' and @rel='noreferrer' and @target='_blank' and @title=concat('Nathan O',\"'\",'Connor')]")
	public WebElement nathanOConnor;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Gard claim reference']/following::input[1]")
	public WebElement gardClaimReference;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Company']/following::div//input[1]")
	public WebElement company;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Ship Insurers Ltd.' and @data-aura-class='forceOutputLookup' and @rel='noreferrer' and @target='_blank' and @title='Ship Insurers Ltd.']")
	public WebElement shipInsurersLtd;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Client claim reference']/following::input[1]")
	public WebElement clientClaimReference;
	@ButtonType()
	@FindBy(xpath = "//button[@title='Save' and normalize-space(.)='Save' and contains(@class,'slds-button') and @data-aura-class='uiButton forceActionButton' and @type='button']")
	public WebElement save;
	@ButtonType()
	@FindBy(xpath = "//button[@name='SaveEdit']")
	public WebElement save1;
	@TextType()
	@FindBy(xpath = "//div[text()='Claim']/following::lightning-formatted-text[1]")
	public WebElement ClaimName;
	@TextType()
	@FindBy(xpath = "//input[@name='Claim__c-search-input' and @placeholder='Search this list...' and @type='search']")
	public WebElement ClaimssearchThisList;
	@LinkType()
	@FindBy(xpath = "//table/tbody/tr/th/span/a")
	public WebElement ClaimNumberCheck;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space()='Related']")
	public WebElement Related;
	@TextType()
	@AuraBy(componentXPath = "//lightning:input[@aura:id= 'fileInput']", qualifier = "fileInput")
	public WebElement AttachFile;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Upload Files']/lightning-primitive-icon")
	public WebElement AttachFile1;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Upload Files']")
	public WebElement UploadFiles;
			
}
