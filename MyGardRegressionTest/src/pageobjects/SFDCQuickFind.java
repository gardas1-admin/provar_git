package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCQuickFind"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCQuickFind {

	@TextType()
	@FindBy(xpath = "//label[text()='Quick Find']/following-sibling::input[1]")
	public WebElement QuickFindSearch;
	@LinkType()
	@FindBy(xpath = "//a[text()='Custom Settings']")
	public WebElement CustomSettingsLink;
	@LinkType()
	@FindBy(xpath = "//a[text()='Blue Card renewal availability']/ancestor::th[1]/preceding-sibling::td[2]/descendant::a[3]")
	public WebElement ManageBlueCardRenewAvailability;
	@LinkType()
	@FindBy(xpath = "//a[text()='Companies']")
	public WebElement CompaniesTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Contacts']")
	public WebElement ContactsTab;
			
}
