package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardHomeServiceReq"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class MyGardHomeServiceReq {

	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/thead/tr/th[1]/div")
	public WebElement ServiceReqHeading;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Request ID:']")
	public WebElement service_req_details_form;
	@TextType()
	@FindBy(xpath = "//div[@id='new_case_comment']//textarea")
	public WebElement ReqIDComment;
	@ButtonType()
	@FindBy(xpath = "//div[@id='new_case_comment']/span/input")
	public WebElement SerReqSubmit;
			
}
