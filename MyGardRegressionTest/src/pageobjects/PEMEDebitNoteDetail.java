package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PEMEDebitNoteDetail"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class PEMEDebitNoteDetail {

	@ButtonType()
	@FindBy(xpath = "//h2[text()='PEME Debit Note Detail Detail']/ancestor::td[1]/following-sibling::td[1]/descendant::input[@title='Create Debit Note Company']")
	public WebElement CreateDebitNoteCompanyBtn;
			
}
