package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="AllTabs"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class AllTabs {

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(text())='PEME Invoices']")
	public WebElement PEMEInvoices;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(text())='PEME Enrolment Details']")
	public WebElement PEMEEnrollmentDetails;
	@LinkType()
	@FindBy(xpath = "//a[contains(normalize-space(text()),'Cases')]")
	public WebElement Cases;
	@LinkType()
	@FindBy(linkText = "Contacts")
	public WebElement contacts;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(text())='Console']/ancestor::td[1]/following-sibling::td[1]/descendant::a[1]")
	public WebElement OpportunityChecklists;
			
}
