package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceEvents"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceEvents {

	@TextType()
	@FindBy(xpath = "//span[@class='slds-day weekday todayDate selectedDate DESKTOP uiDayInMonthCell--default']")
	public WebElement DatePicker_TodayDate;
	@TextType()
	@FindBy(xpath = "//span[@class='slds-day weekday todayDate selectedDate DESKTOP uiDayInMonthCell--default']/following::span[1]")
	public WebElement datepicker_tmrwdate;
	@TextType()
	@FindBy(xpath = "//a[normalize-space(.)='Chatter']")
	public WebElement chatter;
	@TextType()
	@FindBy(xpath = "//div/div/div/div/div/input")
	public WebElement searchSalesforce;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New' and @role='button' and @title='New']")
	public WebElement new_;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='View More' and @type='button']")
	public WebElement ViewMore;
	@TextType()
	@FindBy(xpath = "//div/input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input' and @placeholder='Search People…']")
	public WebElement searchSalesforce1;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Add']")
	public WebElement ActivityNewEventAdd;
			
}
