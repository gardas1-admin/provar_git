package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardTwitter"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class MyGardTwitter {

	@LinkType()
	@FindBy(xpath = "(//span[text()='Gard'])[1]")
	public WebElement About;
	@TextType()
	@FindBy(xpath = "//div/div/div/div/span/span[normalize-space(.)='Gard']")
	public WebElement CompanyTwitter;
			
}
