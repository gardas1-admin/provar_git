package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="RecentPEMEInvoice"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class RecentPEMEInvoice {

	@LinkType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'PEME Invoice Name')]/ancestor::table[1]/ancestor::div[3]/following-sibling::div[1]/descendant::tr[1]/descendant::a[3]")
	public WebElement PEMEInvoiceName;
			
}
