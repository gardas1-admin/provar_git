package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceRelatedCompany"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardE2E"
     )             
public class SalesforceRelatedCompany {

	@ButtonType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//button[normalize-space(.)='Show more actions' and @type='button']")
	public WebElement showMoreActions;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//button[normalize-space(.)='Delete']")
	public WebElement Delete;
			
}
