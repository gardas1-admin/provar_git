package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PEMEEnrollmentDetailsDetail"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class PEMEEnrollmentDetailsDetail {

	@ButtonType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::input[4]")
	public WebElement ApprovalRejectBtn;
			
}
