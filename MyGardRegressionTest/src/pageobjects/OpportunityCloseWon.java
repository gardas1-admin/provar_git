package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="OpportunityCloseWon"                                
               , summary=""
               , connection="MyGardE2E"
               , auraComponent="OpportunityCloseWon"
               , namespacePrefix=""
     )             
public class OpportunityCloseWon {

	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//button[normalize-space(.)='Next']")
	public WebElement NewChklistnext;
	
}
