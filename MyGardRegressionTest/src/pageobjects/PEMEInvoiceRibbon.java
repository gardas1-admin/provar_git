package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="PEME Invoice Ribbon"                                
               , summary=""
               , page="PEMEInvoiceRibbon"
               , namespacePrefix=""
               , object="PEME_Invoice__c"
               , connection="MyGardRegressionStaging"
     )             
public class PEMEInvoiceRibbon {

	@TextType()
	@VisualforceBy(componentXPath = "apex:outputPanel[not(@id)][5]//div[3]//div")
	public WebElement PEMEInvoiceRibbon;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(text())='Partially']")
	public WebElement PEMEInvoiceProgressRibbon;
	
}
