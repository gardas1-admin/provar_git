package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PEMEInvoiceDetailView"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class PEMEInvoiceDetailView {

	@BooleanType()
	@FindBy(xpath = "//th[contains(normalize-space(text()),'PEME Exam Details Name')]/ancestor::tr[1]/following-sibling::tr[1]/descendant::input[1]")
	public WebElement FirstCheckBoxInPEMEExamDetails;
	@ButtonType()
	@FindBy(xpath = "//input[@title='Approve/Reject']")
	public WebElement ApproveRejectBtn;
	@LinkType()
	@FindBy(xpath = "//th[contains(normalize-space(text()),'Status')]/ancestor::tr[1]/following-sibling::tr[1]/descendant::td[2]")
	public WebElement RejectedRowStatus;
	@LinkType()
	@FindBy(xpath = "//th[contains(normalize-space(text()),'Status')]/ancestor::tr[1]/following-sibling::tr[1]/descendant::td[2]")
	public WebElement Status;
	@ButtonType()
	@FindBy(xpath = "//th[contains(normalize-space(text()),'Status')]/ancestor::tr[1]/following-sibling::tr[1]/descendant::a[2]")
	public WebElement DeleteLinkForOneInvoice;
	@BooleanType()
	@FindBy(xpath = "//th[contains(normalize-space(text()),'Status')]/preceding-sibling::th[2]/descendant::input[1]")
	public WebElement AllActionCheckBoxPEMEExamDetails;
	@ButtonType()
	@FindBy(xpath = "//h3[text()='PEME Exam Details']/ancestor::td[1]/following-sibling::td[1]/descendant::input[2]")
	public WebElement ApproveRejectBtn1;
			
}
