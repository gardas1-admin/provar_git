package pageobjects;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.logging.Logger;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.provar.core.testapi.annotations.ButtonType;
import com.provar.core.testapi.annotations.JavascriptBy;
import com.provar.core.testapi.annotations.LinkType;
import com.provar.core.testapi.annotations.Page;
import com.provar.core.testapi.annotations.TestLogger;
import com.provar.core.testapi.annotations.TextType;

@Page(title = "MyGardNewClaim", summary = "", relativeUrl = "", connection = "MyGardRegressionUAT")
public class MyGardNewClaim {

	WebDriver driver;
	@TestLogger
	public Logger testLogger;

	public MyGardNewClaim(WebDriver driver) {

		this.driver = driver;

	}

	public void actionsClick(String elementXpath) {

		testLogger.info("Working with " + elementXpath);
		WebElement e = driver.findElement(By.xpath(elementXpath));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",e);
		js.executeScript("arguments[0].style.border='3px solid red'",e);

		Actions actions = new Actions(driver);
		actions.moveToElement(e).click().build().perform();

	}

	public void scrollIntoElement(String xpathLocator){
	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.xpath(xpathLocator));
		js.executeScript("arguments[0].scrollIntoView();", Element);
	}
	public void selectDropdownUsingList(String WebElements, String ExpectedValue){

		List<WebElement> Options=driver.findElements(By.xpath(WebElements));
		for(WebElement Option: Options){
			System.out.println(Option.getText());
			if(Option.getText().equalsIgnoreCase(ExpectedValue)){
				Option.click();
				break;
			}

		}
	}



	public void UploadFile() throws AWTException   {


		Robot r = new Robot();
		String text = "C:\\Users\\kumnir\\Provar\\Workspace\\MyGardRegressionTest\\templates\\template 1.xml";
		StringSelection stringSelection = new StringSelection(text);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);

		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

	}
	/**public void SikuliUploadFile() throws AWTException   {
		try {

			WebElement Ele= driver.findElement(By.xpath("(//div[@class='new-claim-doc-upld file-upload btn btn-default form-btn'])[2]"));
			Ele.click();
			Thread.sleep(2000);

			String inputFilePath = "C:\\Users\\kumnir\\Provar\\Workspace\\MyGardRegressionTest\\templates\\TestAttachment.docx";
			
			Pattern fileInputTextBox = new Pattern("C:\\Users\\kumnir\\Provar\\Workspace\\MyGardRegressionTest\\SikuliImage\\FileName.PNG");
			Pattern openButton = new Pattern("C:\\Users\\kumnir\\Provar\\Workspace\\MyGardRegressionTest\\SikuliImage\\Open.PNG");

			Screen s = new Screen();
			s.wait(fileInputTextBox, 20);
			s.type(fileInputTextBox, inputFilePath);
			//Thread.sleep(5000);
			s.wait(openButton, 20);
			s.click(openButton);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}**/


	@TextType()
	@FindBy(xpath = "//label[text()='My claim reference:']/following::input[1]")
	public WebElement MyClaimReference;

	@FindBy(xpath = "//label[normalize-space(.)='Client:']/following-sibling::div//button")
	public WebElement ClientDropdown;

	@LinkType()
	@FindBy(xpath = "//label[text()='Client:']/following::li[3]")
	public WebElement SecondOptionClient;

	@TextType()
	@FindBy(xpath = "//td[@class='dp_current']")
	public WebElement PickTodayDate;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Cover:']/following::button[1]")
	public WebElement CoverDropdown;

	@TextType()
	@FindBy(xpath = "//span[text()='Event date:']/following::input[1]")
	public WebElement EventDate;

	@LinkType()
	@FindBy(xpath = "//label[text()='Cover:']/following::li[3]")
	public WebElement P_ICover;

	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search by object name / IMO']")
	public WebElement ObjectLookup;

	@TextType()
	@FindBy(xpath = "//div[@class='auto-popup']/descendant::a[1]")
	public WebElement ObjectLookup1;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim type:']/following::button[1]")
	public WebElement ClaimType;

	@LinkType()
	@FindBy(xpath = "//label[text()='Claim type:']/following::li[2]")
	public WebElement cargo;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim details:']/following::button[1]")
	public WebElement ClaimDetailButton;

	@LinkType()
	@FindBy(xpath = "//label[text()='Claim detail:']/following::li[9]")
	public WebElement ClaimDetail_as_ContainerisedCargo;

	@TextType()
	@FindBy(xpath = "//label[text()='My sub claim reference:']/following::input[1]")
	public WebElement MySubClaimRef;

	@ButtonType()
	@FindBy(xpath = "//input[@type='submit'and @value='Save']")
	public WebElement save;

	@ButtonType()
	@FindBy(xpath = "//input[@type='submit' and @value='Review and submit']")
	public WebElement ReviewAndSubmit;

	@TextType()
	@FindBy(xpath = "//label[text()='My claim reference:']/following::div[1]")
	public WebElement MyClaimAssert;

	@TextType()
	@FindBy(xpath = "//label[text()='Client:']/following::div[1]")
	public WebElement ClientAssertVerification;

	@TextType()
	@FindBy(xpath = "//label[text()='Cover:']/following::div[1]")
	public WebElement Cover;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Claim type:']/following::div[1]")
	public WebElement ClaimTypeValue;

	@TextType()
	@FindBy(xpath = "//label[text()='Object:']/following::div[1]")
	public WebElement ObjectVerification;

	@TextType()
	@FindBy(xpath = "//label[text()='Claim detail:']/following::div[1]")
	public WebElement ClaimDetailVerifivation;

	@TextType()
	@FindBy(xpath = "//label[text()='My sub claim reference:']/following::div[1]")
	public WebElement MySubClaimRef1;

	@TextType()
	@FindBy(xpath = "(//input[@type='button' and @value='Submit'])[2]")
	public WebElement MySubClaimRef2;

	@TextType()
	@FindBy(xpath = "(//input[@type='button' and @value='Submit'])[2]")
	public WebElement MySubClaimRef21;

	@TextType()
	@FindBy(xpath = "(//div[@class='pop-up-sub-title'])[5]")
	public WebElement ServiceDownMessage;

	@LinkType()
	@FindBy(xpath = "//div[@class='form-submit-section']/span/a[1]")
	public WebElement ok;

	@TextType()
	@FindBy(xpath = "//span//div[@id='contact-info-pop-wrapper']/div/div/div")
	public WebElement SussessfullMessage;

	@TextType()
	@FindBy(xpath = "//td[@class='dp_current']")
	public WebElement TodayDate;

	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='For urgent matters, please contact our 24/7 EMERGENCY number']//input[contains(@class,'form-btn') and @type='submit' and @value='Cancel']")
	public WebElement Cancel;

	@ButtonType()
	@FindBy(xpath = "//div[text()='All unsaved information would be lost.']/following::input[1]")
	public WebElement Continue;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='No' and contains(@class,'form-btn')]")
	public WebElement NewClaim_No;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='HOME' and contains(@class,'main-menu-links')]")
	public WebElement homeTab;

	@TextType()
	@FindBy(xpath = "//div[text()='Service requests' and @class='request-updates single-tab deselected' and normalize-space(.)='Service requests']")
	public WebElement ServiceReqTab;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='View all service requests' and @target='_blank']")
	public WebElement viewAllServiceRequests;

	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_14_container']/li[3]/a")
	public WebElement CoverP_I;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='P&I Cover' and @data-normalized-text='P&I Cover']")
	public WebElement P_ICover1;

	@TextType()
	@FindBy(xpath = "//td[@class='dp_weekend dp_current']")
	public WebElement TodaydateWeekend;

	@TextType()
	@FindBy(xpath = "//td[@class='dp_weekend']/following::td[normalize-space(.)='7'][1]")
	public WebElement ProdDate7;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='17']")
	public WebElement TodayDate1;

	@TextType()
	@FindBy(xpath = "//span[text()='Event date:']/following::input[1]")
	public WebElement EventDateInput;

	@TextType()
	@FindBy(xpath = "//td[text()='February, 2020']/following::tr[5]/td[text()='20']")
	public WebElement EventDateInput1;

	@TextType()
	@FindBy(css = "td.dp_current")
	public WebElement TodayDate2;

	@LinkType()
	@FindBy(xpath = "//label[text()='Client:']/following::li[3]/a[1]/span[1]")
	public WebElement SecondClient;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Engine - Auxilliary' and @data-normalized-text='Engine - Auxilliary']")
	public WebElement Engine_Auxilliary;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Crankshaft' and @data-normalized-text='Crankshaft']")
	public WebElement CrankShaft;

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Pick a date']")
	public WebElement pickADate;

	@TextType()
	@JavascriptBy(jspath = "return document.querySelector('td.dp_current')")
	public WebElement TodayDate21;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Pacific Gas Chartering Limited']")
	public WebElement pacificGasCharteringLimited;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Place:']/following-sibling::input")
	public WebElement place;

	@TextType()
	@FindBy(xpath = "//label[text()='Brief description:']/following::textarea[1]")
	public WebElement new_claim_description;

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Add']")
	public WebElement addButton;

	@TextType()
	@FindBy(xpath = "//span[text()='Add documents']")
	public WebElement AddDoc;

	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Item no.:Voucher no.:Description: Browsex']/div/div[1]/input")
	public WebElement ItemNo;

	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Item no.:Voucher no.:Description: Browsex']/div/div[2]/input")
	public WebElement VoucherNo;

	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Item no.:Voucher no.:Description: Browsex']//textarea")
	public WebElement Description;

	@ButtonType(file = true)
	@FindBy(xpath = "(//span[text()='Browse']/following-sibling::input)[1]")
	public WebElement new_claim_upload_doc;

	@ButtonType(file = true)
	@FindBy(xpath = "//span[normalize-space(.)='Item no.:Voucher no.:Description: Browsex']/div/div/div/input")
	public WebElement new_claim_upload_doc1;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='«']")
	public WebElement PreviousMonth;

	@TextType()
	@FindBy(xpath = "//tr[2]/td[normalize-space(.)='1']")
	public WebElement PreviousDate;


	@FindBy(xpath = "//label[normalize-space(.)='Client:']/following-sibling::div//button")
	public WebElement ClientDropD;

	public void click(){
		driver.findElement(By.xpath("//label[normalize-space(.)='Client:']/following-sibling::div//button")).click();
	}

	@ButtonType()
	@FindBy(xpath = "//span[text()='Add reimbursement claim']")
	public WebElement addReimburseClaim;

	@ButtonType()
	@FindBy(xpath = "//div[@class='currency-submission']/input[1]")
	public WebElement continue_;

	@ButtonType()
	@FindBy(xpath = "//tr[1]/td[2]/div/div/button")
	public WebElement ExpenseTypeRow1Dropdown;
	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='x'])[2]")
	public WebElement upload_section_Deletion;
	@TextType()
	@FindBy(xpath = "//label[text()='Sub claim description:']/following-sibling::textarea[1]")
	public WebElement New_sub_claim_description;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Place of receipt:']/following-sibling::input")
	public WebElement place_of_receipt;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Port of loading:']/following-sibling::input")
	public WebElement port_loading_cargo;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Place of delivery:']/following-sibling::input")
	public WebElement place_of_delivery;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Port of discharge:']/following-sibling::input")
	public WebElement port_discharge_cargo;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim amount:']/following-sibling::input")
	public WebElement new_claim_cargo_claim_amount;
	@TextType()
	@FindBy(xpath = "(//div/input[@placeholder='Search' and @class='input-block-level form-control'])[13]")
	public WebElement CurrentyAmtTypeInput;

}
