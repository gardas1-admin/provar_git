package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMOpportunityLostDeclined"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceCRMOpportunityLostDeclined {

	@PageFrame()
	public static class Frame {

		@ChoiceListType()
		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		public WebElement reasonLostDeclined;
		@TextType()
		@FindBy(name = "j_id0:j_id1:j_id5:j_id11:j_id13")
		public WebElement reasonLostDeclinedOtherPrice;
	}

	@PageFrame()
	public static class CRMDeclineLostFrame {

		@VisualforceBy(componentXPath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		@ChoiceListType()
		public WebElement reasonLostDeclined;
		@TextType()
		@FindBy(name = "j_id0:j_id1:j_id5:j_id11:j_id13")
		public WebElement reasonLostDeclinedOtherPrice;
		@ChoiceListType()
		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		public WebElement reasonLostDeclined1;
		@ChoiceListType()
		@FindBy(xpath = "//table[@class='detailList']/tbody/tr/td/select")
		public WebElement reasonLostDeclinedNew;
		@ButtonType()
		@FindBy(xpath = "(//form/div/div/div/div/div/table//input[contains(@class,'btn') and @type='submit' and @value='Save'])[1]")
		public WebElement save;
	}

	@PageFrame()
	public static class Frame11 {

		@ChoiceListType()
		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		public WebElement reasonLostDeclined;
		@TextType()
		@FindBy(name = "j_id0:j_id1:j_id5:j_id11:j_id13")
		public WebElement reasonLostDeclinedOtherPrice;
	}

	@PageFrame()
	public static class ReasonFrame {

		@ChoiceListType()
		@FindBy(xpath = "//td/select")
		public WebElement reasonLostDeclined;
		@TextType()
		@FindBy(name = "j_id0:j_id1:j_id5:j_id11:j_id13")
		public WebElement reasonLostDeclinedOtherPrice;
		@ButtonType()
		@FindBy(xpath = "(//form/div/div/div/div/div/table//input[contains(@class,'btn') and @type='submit' and @value='Save'])[1]")
		public WebElement save;
	}

	@PageFrame()
	public static class Reason {

		@ChoiceListType()
		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		public WebElement reasonLostDeclined;
		@TextType()
		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined (Other/Price)']/parent::th/following-sibling::td//input[@type='text']")
		public WebElement reasonLostDeclinedOtherPrice;
		@ButtonType()
		@FindBy(xpath = "(//td/input[1][@value='Save'])[1]")
		public WebElement save;
		@ChoiceListType()
		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		public WebElement reasonLostDeclinedDD;
	}

	@FindBy(xpath = "//div[@class='iframe-parent slds-template_iframe slds-card']/iframe")
	public Reason reason;
			
}
