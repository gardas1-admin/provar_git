package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardPortfolioForms"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardPortfolioForms {

	@TextType()
	@FindBy(xpath = "//div[contains((text()),'Are you sure you want to download')]")
	public WebElement ShipOwnerPDFFormOpenMessage;
	@LinkType()
	@FindBy(xpath = "//div[contains((text()),'Are you sure you want to download')]/following-sibling::div[1]/descendant::a[2]")
	public WebElement CancelBtn;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Charterers renewal questionnaire']")
	public WebElement CharterersRenewalForm;
			
}
