package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardHelp"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardHelp {

	@TextType()
	@FindBy(xpath = "//h1[text()='MyGard user guide for PEME members']")
	public WebElement UserGuide;
			
}
