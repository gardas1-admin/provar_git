package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceClaim"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceClaim {

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claim Title']/following-sibling::input[contains(@class,'input') and @type='text']")
	public WebElement ClaimTitle;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='--None--' and contains(@class,'select') and @data-interactive-lib-uid='128' and @role='button']")
	public WebElement caseOrigin;
	@LinkType()
	@FindBy(xpath = "//span[text()='Case Origin']/following::a[1]")
	public WebElement caseOrigin1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Phone']")
	public WebElement CaseOrigin_phone;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Claims handlers on Customer']/following-sibling::div//input")
	public WebElement ClaimsHandlersOnCustomer;
	@LinkType()
	@FindBy(xpath = "//td/a[normalize-space(.)=concat('Nathan O',\"'\",'Connor')]")
	public WebElement Contact;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Client claim referenceknown as My Claim Reference in MyGard']/following-sibling::input")
	public WebElement clientClaimReference;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Company Name']/following-sibling::div//input")
	public WebElement companyName;
	@LinkType()
	@FindBy(xpath = "//td/a[normalize-space(.)='Crown Ship Builders Corp']")
	public WebElement crownShipBuildersCorp;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Save' and contains(@class,'slds-button') and @title='Save' and @type='button']")
	public WebElement CaseSave;
	@TextType()
	@FindBy(xpath = "//span[text()='MyGard Claim ID']/following::span[2]")
	public WebElement myGardClaimID;
	@TextType()
	@FindBy(xpath = "//span[text()='SFDC Claim Ref ID']/following::span[2]")
	public WebElement SFDCClaimRefID;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Claims' and contains(@class,'slds-context-bar__label-action') and @title='Claims']")
	public WebElement Claims;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='New' and contains(@class,'select') and @role='button']")
	public WebElement Status;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[1]")
	public WebElement StatusDDVerification;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[2]")
	public WebElement StatusVerification_Draft;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[3]")
	public WebElement StatusVerification_New;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[4]")
	public WebElement StatusVerification_pending;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[5]")
	public WebElement StatusVerificatio_Processed;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[6]")
	public WebElement StatusVerification_closed;
	@LinkType()
	@FindBy(xpath = "((//div[@class='select-options']/ul)[5]/li/a)[7]")
	public WebElement StatusVerification_rejected;
			
}
