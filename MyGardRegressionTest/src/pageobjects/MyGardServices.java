package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardServices"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardServices {

	@TextType()
	@FindBy(xpath = "//h1[text()='Services']")
	public WebElement Services;
			
}
