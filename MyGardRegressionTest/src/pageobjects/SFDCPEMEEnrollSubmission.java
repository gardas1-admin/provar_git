package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCPEMEEnrollSubmission"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SFDCPEMEEnrollSubmission {

	@LinkType()
	@FindBy(xpath = "//div/div/span[text()='Related To']/following::span[1]/div[contains(@class,'outputLookupContainer') and contains(@class,'forceOutputLookupWithPreview')]/a[1]")
	public WebElement relatedTo;
	@LinkType()
	@FindBy(xpath = "(//div[@class='slds-col slds-no-flex slds-grid slds-grid_vertical-align-center actionsContainer']/following::li/a/div[text()='Edit'])[1]")
	public WebElement edit;
	@TextType()
	@FindBy(xpath = "//label/span[text()='Gard PEME References']/following::textarea[1]")
	public WebElement gardPEMEReferences;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='Under Approval' and contains(@class,'select') and @role='button']")
	public WebElement enrollmentStatus;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Enrolled' and @title='Enrolled']/..")
	public WebElement enrolled;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Edit Enrollment Status' and @title='Edit Enrollment Status']")
	public WebElement editEnrollmentStatus;
	@TextType()
	@FindBy(xpath = "//label[text()='Gard PEME References']/following::textarea[1]")
	public WebElement gardPEMEReferences1;
	@ButtonType()
	@FindBy(xpath = "//button[@name='SaveEdit']")
	public WebElement save;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Enrollment Status']/following::input[1]")
	public WebElement enrollmentStatus1;
	@TextType()
	@FindBy(xpath = "//span/span[text()='Enrolled']")
	public WebElement Enrolled;
	@TextType()
	@FindBy(xpath = "//li[normalize-space(.)='Atleast one approved manning agent and debit note should be there for submitting the Enrollment for approval']")
	public WebElement ReviewErrorMessage;
	@TextType()
	@FindBy(xpath = "//span/span[text()='Under Approval']")
	public WebElement UnderApproval;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Manning agent details(1)' and contains(@class,'slds-card__header-link')]")
	public WebElement manningAgentDetails1;
	@LinkType()
	@FindBy(xpath = "//span/a[contains(@class,'slds-truncate') and @data-aura-class='forceOutputLookup' and @data-refid='recordId' and @target='_blank']")
	public WebElement ManagingAgent;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Enrolled' and @role='menuitemradio' and @title='Enrolled']")
	public WebElement ManagingAgentenrolled;
	@LinkType()
	@FindBy(xpath = "//label[text()='Status']/following::input[1]")
	public WebElement StatusDD;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Approved' and @title='Approved']")
	public WebElement Approved;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Company Name']/following::div//input[1]")
	public WebElement companyName;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='TestAutomationnkuma' and @role='option']")
	public WebElement FieldSelect;
	@ButtonType()
	@FindBy(xpath = "//button[@class='slds-button slds-button_brand']")
	public WebElement ManagingAgentSave;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Contact Person']/following::div//input[1]")
	public WebElement contactPerson;
	@LinkType()
	@FindBy(linkText = "Nirmal Kumar")
	public WebElement ContactSelect;
	@LinkType()
	@FindBy(xpath = "//span[text()='PEME Enrollment Form']/following::a[1]")
	public WebElement PEMEEnrollmentForm;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='PEME Debit note details(1)']")
	public WebElement pEMEDebitNoteDetails1;
	@LinkType()
	@FindBy(xpath = "//table/tbody/tr/th/span/a[1]")
	public WebElement PEMEDebitNotes;
	@LinkType()
	@FindBy(xpath = "//lightning-button/button[text()='Create Manning Company']/../../../../../../../li[1]/descendant::button[1]")
	public WebElement ManningAgentEdit;
	@LinkType()
	@FindBy(xpath = "//button[text()='Create Debit Note Company']/ancestor::li/../li/descendant::button[1][text()='Edit']")
	public WebElement DebitNoteEdit;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(.)='Status']/following::input[1]")
	public WebElement DebitNoteStatus;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='*Company Name']/following::input[1]")
	public WebElement DebitNotecompanyName;
	@LinkType()
	@FindBy(xpath = "//td/a[normalize-space(.)='TestAutomation' and contains(@class,'outputLookupLink') and @target='_blank' and @title='TestAutomation']")
	public WebElement testAutomation;
	@ButtonType()
	@FindBy(xpath = "//button[text()='Save & New']/following::button[text()='Save']")
	public WebElement DebitNoteSave;
	@LinkType()
	@FindBy(xpath = "//flexipage-tab2[@class='slds-tabs_default__content slds-show']/following::span[text()='PEME Enrollment Form']/following::a[1]")
	public WebElement DebirFormPEMEEnrollmentForm;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='Under Approval' and contains(@class,'select') and @role='button']")
	public WebElement EnrollSts;
	@ButtonType()
	@FindBy(xpath = "//button[text()='Printable View']/ancestor::li/../li/descendant::button[1][text()='Edit']")
	public WebElement Edit;
			
}
