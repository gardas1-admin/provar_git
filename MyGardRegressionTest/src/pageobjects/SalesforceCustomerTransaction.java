package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCustomerTransaction"                                
, summary=""
, relativeUrl=""
, connection="MyGardRegressionQABox"
		)             
public class SalesforceCustomerTransaction {

	@ButtonType()
	@FindBy(xpath = "//button[@title='Show Navigation Menu']")
	public WebElement ShowNavigationMenu;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Home']")
	public WebElement homeTab;

	@PageFrame()
	public static class Dashboard {

		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Open' and contains(@class,'slds-button') and @type='button']")
		public WebElement openButton;
		@ButtonType()
		@FindBy(xpath = "//div/div/button[normalize-space(.)='Refresh' and @type='button' and @class='slds-button slds-button_neutral refresh']")
		public WebElement refresh;
		@TextType()
		@FindBy(xpath = "//div[normalize-space(.)='New Unassigned Cases each Queue']")
		public WebElement NewUnassignedCases;
		@TextType()
		@FindBy(xpath = "//div[normalize-space(.)='My Open Cases']")
		public WebElement MyOpenCases;
		@TextType()
		@FindBy(xpath = "//div[normalize-space(.)='Closed Cases Today']")
		public WebElement ClosedCaseToday;
		@TextType()
		@FindBy(xpath = "//div[normalize-space(.)='New Cases Today']")
		public WebElement TodayNewCases;
		@TextType()
		@FindBy(xpath = "//div[normalize-space(.)='CT Number of Open Cases']")
		public WebElement CTNumberofOpenCases;
		@TextType()
		@FindBy(xpath = "//div[normalize-space(.)='Closed Cases This Week']")
		public WebElement ClosedCasesThisWeek;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"New Unassigned Cases each Queue\"' and contains(@class,'slds-button') and @title='Expand' and @type='button']")
		public WebElement expandNewUnassignedCasesEachQueue1;
		@LinkType()
		@FindBy(xpath = "//h2/div[text()='My Open Cases']/following::a[1]")
		public WebElement viewReportMyOpenCases;
		@LinkType()
		@FindBy(xpath = "//h2/div[text()='New Unassigned Cases each Queue']/following::a[1]")
		public WebElement viewNewUnassignedCases;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Close' and contains(@class,'slds-button') and @title='Close' and @type='button']")
		public WebElement closeUnassignedCase;
		@ButtonType()
		@FindBy(css = "button.slds-button.slds-button_neutral.view")
		public WebElement open;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"My Open Cases\"']")
		public WebElement expandMyOpenCases;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"Closed Cases Today\"']")
		public WebElement expandClosedCasesToday;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Close']")
		public WebElement closetheExpanded;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Number of Open Cases\"']")
		public WebElement expandCTNumberOfOpenCases;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"Number of Cases in each Status\"']")
		public WebElement expandNumberOfCasesInEachStatus;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"New Cases Today\"']")
		public WebElement expandNewCasesToday;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"Closed Cases This Week\"']")
		public WebElement expandClosedCasesThisWeek;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"Last 7 days cases by day\"']")
		public WebElement expandLast7DaysCasesByDay;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)=concat('Expand \"Last 60 day',\"'\",'s cases by week\"')]")
		public WebElement expandLast60DaySCasesByWeek;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"Number of Open Cases per Category\"']")
		public WebElement expandNumberOfOpenCasesPerCategory;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"This week Cases per company\"']")
		public WebElement expandThisWeekCasesPerCompany;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Renewals cases\"']")
		public WebElement expandCTRenewalsCases;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT cases in waiting\"']")
		public WebElement expandCTCasesInWaiting;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Waiting Comp Catalyst - in Days\"']")
		public WebElement expandCTCaseWaitingCompCatalystInDays;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Lead Time - in Days\"']")
		public WebElement expandCTCaseLeadTimeInDays;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Touch Time - in Days\"']")
		public WebElement expandCTCaseTouchTimeInDays;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Lead Time - in Hours\"']")
		public WebElement expandCTCaseLeadTimeInHours;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Touch Time - in Hours\"']")
		public WebElement expandCTCaseTouchTimeInHours;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case In New - in Hours\"']")
		public WebElement expandCTCaseInNewInHours;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Next Component']")
		public WebElement nextComponent;
		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='View Report (New Unassigned Cases (CT)) for \"New Unassigned Cases each Queue\"']")
		public WebElement viewReportNewUnassignedCasesCTForNewUnassignedCasesEachQueue;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Next Component']")
		public WebElement nextComponent1;

	}

	@FindBy(xpath = "//iframe[@title='dashboard']")
	public Dashboard dashboard;
	@LinkType()
	@FindBy(xpath = "//header[@id='oneHeader']//a[normalize-space(.)='Customer Transactions Main' and @title='Customer Transactions Main']")
	public WebElement loadingCustomerTransactionsMain;
	@ButtonType()
	@FindBy(xpath = "//div/button[1][normalize-space(.)='Refresh' and contains(@class,'slds-button') and @type='button']")
	public WebElement RefreshButton;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='My Open Cases']")
	public WebElement MyOpenCases;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='My Open Cases' and @title='My Open Cases']")
	public WebElement MyOpenCases1;
	@TextType()
	@FindBy(xpath = "//div[normalize-space(.)='My Open Cases']")
	public WebElement MyOpenCaseTitle;

	@PageFrame()
	public static class Dashboard1 {
	}

	@FindBy(css = "iframe[src='https://gardas.lightning.force.com/desktopDashboards/dashboardApp.app?dashboardId=01Z3z000001NyUxEAK&displayMode=readonly&networkId=000000000000000&context=embedded console&userId=0053z00000AyO64AAF']")
	public Dashboard1 dashboard1;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"Closed Cases Today\"']")
	public WebElement expandClosedCasesToday;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"My Open Cases\"']")
	public WebElement expandMyOpenCases1;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"New Cases Today\"']")
	public WebElement expandNewCasesToday;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"Closed Cases This Week\"']")
	public WebElement expandClosedCasesThisWeek;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Renewals cases\"']")
	public WebElement expandCTRenewalsCases;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT cases in waiting\"']")
	public WebElement expandCTCasesInWaiting;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Waiting Comp Catalyst - in Days\"']")
	public WebElement expandCTCaseWaitingCompCatalystInDays;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Lead Time - in Days\"']")
	public WebElement expandCTCaseLeadTimeInDays;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Touch Time - in Days\"']")
	public WebElement expandCTCaseTouchTimeInDays;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case Lead Time - in Hours\"']")
	public WebElement expandCTCaseLeadTimeInHours;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Expand \"CT Case In New - in Hours\"']")
	public WebElement expandCTCaseInNewInHours;
	@TextType()
	@FindBy(xpath = "//h2[normalize-space(.)='Closed Cases Today' and contains(@class,'slds-text-heading_medium')]")
	public WebElement ClosedCasesToday;




}
