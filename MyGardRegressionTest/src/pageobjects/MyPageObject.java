package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage(connection = "MyGardRegressionStaging", title = "SFDCHomePage")
public class MyPageObject {

	@LinkType()
	@FindBy(xpath = "//a[@title='Contacts Tab']")
	public WebElement ContactsTab;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"cover-updates\")]")
	public WebElement MyGardCoversUpdatesTab;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"claim-updates\")]")
	public WebElement MyGardClaimsUpdatesTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='View all covers']")
	public WebElement MyGardViewAllCoversLink;
			
}
