package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardfacebook"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardfacebook {

	@TextType()
	@FindBy(xpath = "//a[@aria-label='Facebook']")
	public WebElement facebook;
			
}
