package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardCalener"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardE2E"
     )             
public class MyGardCalener {

	@TextType()
	@FindBy(css = "td.dp_current")
	public WebElement TodayFromDate;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='From']/following-sibling::span//button[contains(@class,'Zebra_DatePicker_Icon') and @type='button']")
	public WebElement FromDate;
	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='To']/following-sibling::span//button")
	public WebElement ToDate;
	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/table/tbody/tr/td[@class='dp_current']/following::td[1]")
	public WebElement ToTmrwDate;
			
}
