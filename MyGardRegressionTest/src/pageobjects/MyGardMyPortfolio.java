package pageobjects;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.ButtonType;
import com.provar.core.testapi.annotations.JavascriptBy;
import com.provar.core.testapi.annotations.LinkType;
import com.provar.core.testapi.annotations.Page;
import com.provar.core.testapi.annotations.TestLogger;
import com.provar.core.testapi.annotations.TextType;

@Page(title = "MyGardMyPortfolio", summary = "", relativeUrl = "", connection = "MyGardRegressionUAT")
public class MyGardMyPortfolio {

	WebDriver driver;
	@TestLogger
	public Logger testLogger;

	public MyGardMyPortfolio(WebDriver driver) {

		this.driver = driver;
	}

	public void actionsClick(String elementXpath, String elementName) {

		testLogger.info("Working with " + elementName);
		WebElement e = driver.findElement(By.xpath(elementXpath));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",e);
		js.executeScript("arguments[0].style.border='3px solid red'",e);
		
		Actions actions = new Actions(driver);
		actions.moveToElement(e).click().build().perform();

	}
	
	public void enterText(String Element, String Text){
	WebElement ele =driver.findElement(By.xpath(Element));
	JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",ele);
		js.executeScript("arguments[0].style.border='3px solid red'",ele);
	ele.sendKeys(Text);
	}
	
		public void scrollUp() {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,1000)");

	}
	
		public void scrollintoView(String elementID) {
		
		testLogger.info("Working with " + elementID);
		WebElement ele = driver.findElement(By.xpath(elementID));
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("arguments[0].scrollIntoView(true);",ele);
		


	}
	
	public void JSclick(String ElementLocator) {

		WebElement e = driver.findElement(By.xpath(ElementLocator));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.background='yellow'",e);
		js.executeScript("arguments[0].style.border='3px solid red'",e);

		js.executeScript("arguments[0].click();", e);

	}
	public void selectDropdownUsingList(String WebElements, String ExpectedValue){

		List<WebElement> Options=driver.findElements(By.xpath(WebElements));
		for(int i=1;i<=Options.size();i++){
		String ele= WebElements+"["+i+"]";
		String ele1=driver.findElement(By.xpath(ele)).getAttribute("data-normalized-text");
		System.out.println(ele1);
		if(ele1.equals(ExpectedValue)){
		WebElement Ele2=driver.findElement(By.xpath(ele));
		Ele2.click();
		}

		}
	}

	@LinkType()
	@FindBy(xpath = "//a[text()='Forms']")
	public WebElement MyPortfolioFormsTab;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Client']/following::button[1]")
	public WebElement Choose_Client_Name;

	@TextType()
	@FindBy(xpath = "//label[text()=' Choose client name']/following::button[1]")
	public WebElement ChooseClientName;

	@TextType()
	@FindBy(xpath = "//label[text()=' Choose client name']/following::li[2]")
	public WebElement SelectCrownShipBuilderCorp;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),'RI declaration form')]/div")
	public WebElement RIDeclarationFormActionButton;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'RI declaration form')]/descendant::div[7]/a[1]")
	public WebElement FillInOnline;

	@ButtonType()
	@FindBy(xpath = "//label[text()=' Client:']/following::button[1]")
	public WebElement ClientDropDown;

	@LinkType()
	@FindBy(xpath = "//label[text()=' Client:']/following::li[2]")
	public WebElement SelectCrownShipBuildersCorp;

	@LinkType()
	@FindBy(xpath = "//label[text()='Client']/following::li[3]")
	public WebElement Client;

	@LinkType()
	@FindBy(xpath = "//label[text()=' Client:']/following::li[3]")
	public WebElement Client1;

	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search by object name / IMO']")
	public WebElement ObjectSearch;

	@LinkType()
	@FindBy(xpath = "//div[@class='auto-popup']/descendant::a[1]")
	public WebElement SelectObject;

	@TextType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[1]")
	public WebElement LoadDate1;

	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tr/td[@class='dp_current']/following::td[2]")
	public WebElement DatePickerSelection;

	@ButtonType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[3]")
	public WebElement LoadDate2;

	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tr/td[@class='dp_current']/following::td[2]")
	public WebElement DatePickerSelection1;

	@TextType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[5]")
	public WebElement LoadDate3;

	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tr/td[@class='dp_current']/following::td[2]")
	public WebElement DatePickerSelection2;

	@TextType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[7]")
	public WebElement LoadDate4;

	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tr/td[@class='dp_current']/following::td[2]")
	public WebElement DatePickerSelection3;

	@TextType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[2]")
	public WebElement DischargeDate1;

	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::tr/td[@class='dp_disabled dp_disabled_current']/following::td[2]")
	public WebElement DischargeDateDatePickerSelection1;

	@ButtonType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[4]")
	public WebElement pickADate;

	@TextType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[4]")
	public WebElement DischargeDatePickerSelection2;

	@ButtonType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[6]")
	public WebElement DischargeDate3;

	@ButtonType()
	@FindBy(xpath = "//div[contains(text(),' Load date')]/following::input[8]")
	public WebElement DischargeDate4;

	@LinkType()
	@FindBy(xpath = "//a[text()='MY PORTFOLIO']")
	public WebElement MyProtfolioTab;

	@ButtonType()
	@FindBy(xpath = "//input[@type='submit' and @value='Next']")
	public WebElement NextButton;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),'I hereby')]")
	public WebElement Acknowledgement;

	@TextType()
	@FindBy(xpath = "//label[text()=' Place']/following::input[1]")
	public WebElement Place;

	@ButtonType()
	@FindBy(xpath = "//div[@class='right-button-set']/input[@type='button' and @value='Submit']")
	public WebElement Submit;

	@TextType()
	@FindBy(xpath = "(//div[@class='pop-up-sub-title'])[3]")
	public WebElement FormSubmitSuccessfulMessage;

	@ButtonType()
	@FindBy(xpath = "(//input[@class='form-btn'])[4]")
	public WebElement Ok;

	@ButtonType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'The user is already logged in to the Community')]/following-sibling::div[1]/descendant::input[1]")
	public WebElement OkButtonLoggedIntoCommunity;

	@TextType()
	@FindBy(xpath = "//div[@class='acc-info my-account closed']")
	public WebElement MyAccountIcon;

	@LinkType()
	@FindBy(xpath = "//a[text()='Activity feed']")
	public WebElement ActivityFeed;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'Layup form Marine')]/descendant::div[7]/a[1]")
	public WebElement FillInOnlineLayupMarine;

	@ButtonType()
	@FindBy(xpath = "//label[contains(text(),'moorings:')]/following::input[1]")
	public WebElement ArrivalDateFinalMoorings;

	@TextType()
	@FindBy(xpath = "//td[@class='dp_current']")
	public WebElement ArrivalDateSelect;

	@ButtonType()
	@FindBy(xpath = "//label[contains(text(),' Departure date:')]/following::input[1]")
	public WebElement DepatureDate;

	@TextType()
	@FindBy(xpath = "(//td[@class='dp_current'])[2]/following-sibling::td[2]")
	public WebElement DepartureDateSelect;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),'ITOPF form for new entries')]/descendant::div[7]/a[1]")
	public WebElement FillInOnlineITOPFForm;

	@TextType()
	@FindBy(xpath = "//label[text()=' Date for attachment of risk:']/following::input[1]")
	public WebElement DateOfAttachmentOfRisk;

	@TextType()
	@FindBy(xpath = "//div[@class='Zebra_DatePicker dp_visible']/descendant::td[@class='dp_current']")
	public WebElement DateForAttachmentCurrentDate;

	@TextType()
	@FindBy(xpath = "//label[text()=' Email address (where MRF will be sent):']/following::input[1]")
	public WebElement EmailAddrss;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'Layup form P&I')]/descendant::div[7]/a[1]")
	public WebElement FillInOnlineLayupP_I;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),' Lay-up location:')]/following::input[1]")
	public WebElement lay_up_location;

	@ButtonType()
	@FindBy(xpath = "//label[contains(text(),'Arrival')]/following::input[1]")
	public WebElement ArrivalDate;

	@LinkType()
	@FindBy(xpath = "//label[@for='uw-ship-owner-form-option']/descendant::a[2]")
	public WebElement FillinOnlineShipOwner;

	@ButtonType()
	@FindBy(xpath = "//label[text()=' Port of registry:']/following::button[1]")
	public WebElement PortOfRegistry;

	@LinkType()
	@FindBy(xpath = "//label[text()=' Port of registry:']/following::li[2]")
	public WebElement Aalborg;

	@ButtonType()
	@FindBy(xpath = "//label[text()=' Collision liabilities to be covered under P&I: ']/following::button[1]")
	public WebElement CollidionLiabilitiesDD;

	@LinkType()
	@FindBy(xpath = "//label[text()=' Collision liabilities to be covered under P&I: ']/following::li[2]")
	public WebElement __RDC;

	@TextType()
	@FindBy(xpath = "//label[text()=' Yes']/ancestor::td[1]/input")
	public WebElement including_ffo;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),'Registered owner')]/following::textarea[1]")
	public WebElement Registered_ownerName_and_Addr;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),'Name of assured/members and capacity (full name, including city/country)')]/following::textarea[1]")
	public WebElement NameOfAssured;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Name to appear on premium invoice:']/following-sibling::input[1]")
	public WebElement PremiumInvoiceName;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),'VAT – Name and address of the operating company:')]/following::textarea[1]")
	public WebElement OperatingCompanyName_Addrs;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),' Registered for VAT – VAT number:')]/following::input[1]")
	public WebElement VATNumber;

	@ButtonType()
	@FindBy(xpath = "//span[contains(text(),'Country code:')]/following::button[1]")
	public WebElement CountryField;

	@LinkType()
	@FindBy(xpath = "//span[text()='Country code:']/following::li[2]/a/span[1]")
	public WebElement Country_Andorra;

	@TextType()
	@FindBy(xpath = "//label[text()='Client:']/following::div[1]")
	public WebElement UICheckClient;

	@TextType()
	@FindBy(xpath = "//label[text()='Object name:']/following::div[1]")
	public WebElement UICheckObjName;

	@TextType()
	@FindBy(xpath = "//label[text()='Collision liabilities to be covered under P&I: ']/following::div[1]")
	public WebElement UI_Check_Collision_Liabilities;

	@TextType()
	@FindBy(xpath = "//label[text()='Port of registry:']/following::div[1]")
	public WebElement UICheckPortofReg;

	@TextType()
	@FindBy(xpath = "//div[text()='Registered owner’s name and address:']/following::div[1]/span")
	public WebElement frmRev;

	@TextType()
	@FindBy(xpath = "//div[text()='Name of assured/members and capacity (full name, including city/country)']/following::div[1]")
	public WebElement UICheckNameofAssured;

	@TextType()
	@FindBy(xpath = "//div[text()='Name to appear on premium invoice:']/following::div[1]")
	public WebElement UICheckPremiumInvoice;

	@TextType()
	@FindBy(xpath = "//div[text()='VAT – Name and address of the operating company:']/following::div[1]")
	public WebElement UICheckVATName;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Registered for VAT - VAT number:']/following-sibling::div[1]/span")
	public WebElement UICheckVATNo;

	@TextType()
	@FindBy(xpath = "//div[text()='Country code:']/following::div[1]")
	public WebElement UICheckCountryCode;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'Application form for Chartered object')]/descendant::div[7]/a[1]")
	public WebElement Fil_in_OnlineCharteredObj;

	@TextType()
	@FindBy(xpath = "(//label[normalize-space(.)='Address details:']/following::textarea)[1]")
	public WebElement premium_invoice;

	@LinkType()
	@FindBy(xpath = "//a[@href='/mygard/UWRMOUObjDtls?strParams=Entry%20form%20for%20Mobile%20offshore%20unit']")
	public WebElement Fill_in_Online_MobileOfshoreUnit;

	@LinkType()
	@FindBy(xpath = "//label[@for='uw-off-obj-form-option']/descendant::a[2]")
	public WebElement Fill_in_Oline_OffshoreObject;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'Comprehensive charterers cover entry form')]/descendant::div[7]/a[1]")
	public WebElement Fill_inOnlineCoverEntryForm;

	@TextType()
	@FindBy(xpath = "//label[text()=' Primary assured name:']/following::input[1]")
	public WebElement PrimaryAssuredName;

	@TextType()
	@FindBy(xpath = "//label[contains(text(),' Primary assured’s address:')]/following::textarea[1]")
	public WebElement PrimaryAddress;

	@TextType()
	@FindBy(xpath = "//label[text()=' VAT – name and address of the operating company:']/following::textarea[1]")
	public WebElement VAT_name_address;

	@TextType()
	@FindBy(xpath = "//table/tbody/tr/td[1]/input")
	public WebElement AssuredClaimContactName;

	@TextType()
	@FindBy(xpath = "//table/tbody/tr/td[2]/input")
	public WebElement AssuresClaimContactPhone;

	@TextType()
	@FindBy(xpath = "//table/tbody/tr/td[3]/input")
	public WebElement AssuredClaimContactEmail;

	@TextType()
	@FindBy(xpath = "//label[text()='Name:']/following::input[1]")
	public WebElement ClaimRelatedInvoiceName;

	@TextType()
	@FindBy(xpath = "(//label[text()='Email:']/following::input)[1]")
	public WebElement email_invoice;

	@TextType()
	@FindBy(xpath = "//label[text()='Primary assured’s address:']/following::div[1]/span")
	public WebElement UICheck_PrimaryAssuredAddr;

	@TextType()
	@FindBy(xpath = "//label[text()='Primary assured name:']/following::div[1]")
	public WebElement UICheckPrimaryAssuredName;

	@TextType()
	@FindBy(xpath = "//label[text()='Name to appear on premium invoice:']/following::div[1]")
	public WebElement UICheckPremiumInvoice1;

	@TextType()
	@FindBy(xpath = "(//label[text()='Address details:']/following::div)[1]")
	public WebElement UICheckAddressDetails;

	@TextType()
	@FindBy(xpath = "//label[text()='VAT – name and address of the operating company:']/following::div[1]")
	public WebElement UICheck_Name_and_Addrs;

	@TextType()
	@FindBy(xpath = "(//table/tbody)[4]//tr/td[1]")
	public WebElement UICheck_ContactName;

	@TextType()
	@FindBy(xpath = "(//table/tbody)[4]//tr/td[2]")
	public WebElement UICheck_ContactPHNo;

	@TextType()
	@FindBy(xpath = "(//table/tbody)[4]//tr/td[3]")
	public WebElement UICheck_ContactEmail;

	@TextType()
	@FindBy(xpath = "//label[text()='Name:']/following::div[1]")
	public WebElement UICheck_ClaimInvoiceName;

	@TextType()
	@FindBy(xpath = "(//label[text()='Email:']/following::div[1])[2]")
	public WebElement UICheck_ClaimInvoiceEmail;

	@LinkType()
	@JavascriptBy(jspath = "return document.querySelector('div.form-popup-options.form-fill-on > span > a')")
	public WebElement Fill_in_OnlineCharacterRenewal;

	@ButtonType()
	@FindBy(xpath = "//label[text()='Name of the Assured(s):']/following::button[1]")
	public WebElement NameOfAssured1;

	@LinkType()
	@FindBy(xpath = "//label[text()='Name of the Assured(s):']/following::li[2]")
	public WebElement CrownShipBuildersCorpDD;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[1]/td[2]/input")
	public WebElement CurrentInsurance1_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[1]/td[3]/input")
	public WebElement CurrentInsuranceYear1_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[1]/td[5]/input")
	public WebElement UpcomingYear1_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[1]/td[6]/input")
	public WebElement UpcomingInsurance1_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[2]/td[2]/input")
	public WebElement CurrentInsuranceYear2_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[2]/td[3]/input")
	public WebElement CurrentInsuranceYear2_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[2]/td[5]/input")
	public WebElement UpcomingInsuranceYear2_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[2]/td[6]/input")
	public WebElement UpcomingInsuranceYear2_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[3]/td[2]/input")
	public WebElement CurrentInsuranceYear3_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[3]/td[3]/input")
	public WebElement CurrentInsuranceYear3_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[3]/td[5]/input")
	public WebElement UpcomingInsuranceYear;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[3]/td[6]/input")
	public WebElement UpcomingInsuranceYear3_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[4]/td[2]/input")
	public WebElement CurrentInsuranceYear4_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[4]/td[3]/input")
	public WebElement CurrentInsuranceYear4_2;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[4]/td[5]/input")
	public WebElement UpcomingInsuranceYear4_1;

	@TextType()
	@FindBy(xpath = "//table/tbody[2]/tr[4]/td[6]/input")
	public WebElement UpcomingInsuranceYear4_2;

	@TextType()
	@FindBy(xpath = "(//table)[1]/tbody/tr[1]/td[2]/input")
	public WebElement Geography_Current_year_insurance;

	@TextType()
	@FindBy(xpath = "(//table)[1]/tbody/tr[1]/td[4]/input")
	public WebElement geo_upcoming_insurance_year;

	@TextType()
	@FindBy(xpath = "(//table)[1]/tbody/tr[2]/td[2]/input")
	public WebElement Cargo_current_insurance_year;

	@TextType()
	@FindBy(xpath = "(//table)[1]/tbody/tr[2]/td[4]/input")
	public WebElement cargo_upcoming_insurance_year;

	@TextType()
	@FindBy(xpath = "(//table)[1]/tbody/tr[3]/td[2]/input")
	public WebElement US_trade_current_insurance_year;

	@TextType()
	@FindBy(xpath = "(//table)[1]/tbody/tr[3]/td[4]/input")
	public WebElement us_trade_upcoming_insurance_year;

	@TextType()
	@FindBy(xpath = "(//table)[2]/tbody/tr[1]/td[2]/input")
	public WebElement Owned_cargo_crnt_insurance_yr;

	@TextType()
	@FindBy(xpath = "(//table)[2]/tbody/tr[1]/td[4]/input")
	public WebElement Owned_cargo_upcoming_insurance_yr;

	@TextType()
	@FindBy(xpath = "(//table)[2]/tbody/tr[2]/td[2]/input")
	public WebElement Third_party_cargo_crnt_insurance_yr;

	@TextType()
	@FindBy(xpath = "(//table)[2]/tbody/tr[2]/td[4]/input")
	public WebElement third_party_cargo_upcoming_insurance_yr;

	@TextType()
	@FindBy(xpath = "(//table)[1]/descendant::td[1]/input")
	public WebElement TradeOutside_radio;

	@TextType()
	@FindBy(xpath = "//label[text()='Frequency:']/following::input[1]")
	public WebElement Frequency;

	@TextType()
	@FindBy(xpath = "//label[text()='Applicable contract provisions:']/following::input[1]")
	public WebElement Applicable_contract_provisions;

	@TextType()
	@FindBy(xpath = "//label[text()='Geography:']/following::input[1]")
	public WebElement Geography;

	@TextType()
	@FindBy(xpath = "//label[text()='Is the vessel ice-classed for the trade?']/following::input[1]")
	public WebElement Vessel_ice_classed;

	@TextType()
	@FindBy(xpath = "//label[text()='Is H&M in place when the vessel is calling in ice?']/following::input[1]")
	public WebElement Is_HM_in_place;

	@TextType()
	@FindBy(xpath = "(//table)[2]/descendant::td[1]/input")
	public WebElement DoesTheAssuredSPerformSTSShipToShipTransfer;

	@TextType()
	@FindBy(xpath = "(//label[text()='Frequency:']/following::input[1])[2]")
	public WebElement Frequency1;

	@TextType()
	@FindBy(xpath = "(//label[text()='Applicable contract provisions:']/following::input[1])[2]")
	public WebElement Applicable_contract_provisions2;

	@TextType()
	@FindBy(xpath = "//label[text()='Charter party form:']/following::input[1]")
	public WebElement Charter_party_form;

	@TextType()
	@FindBy(xpath = "//label[text()='STS service contract:']/following::input[1]")
	public WebElement STS_service_contract;

	@TextType()
	@FindBy(xpath = "(//table)[3]/descendant::td[1]/input")
	public WebElement Assured_perform_blending_of_Cargo_yes; 
	
	@TextType()
	@FindBy(xpath = "(//table)[3]/descendant::td[2]/input")
	public WebElement Assured_perform_blending_of_Cargo_No;

	@TextType()
	@FindBy(xpath = "(//label[text()='Frequency:'])[3]/following::input[1]")
	public WebElement Frequency3;

	@TextType()
	@FindBy(xpath = "(//label[text()='Applicable contract provisions:'])[3]/following::input[1]")
	public WebElement Applicable_contract_provisions_3;

	@TextType()
	@FindBy(xpath = "//label[text()='Is blending onboard or ashore?']/following::table[1]/descendant::td[1]/input")
	public WebElement Blending_onboard_yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Is blending onboard or ashore?']/following::table[1]/descendant::td[2]/input")
	public WebElement Cargo_cmptibility_stbility_yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Does the Assured(s) test compatibility and stability prior to blending?']/following::table[1]/descendant::td[1]/input")
	public WebElement Cargo_cmptibility_stbility_yes1;

	@TextType()
	@FindBy(xpath = "//label[text()='Does the Assured(s) blend heavy fuel oil?']/following::table[1]/descendant::td[1]/input")
	public WebElement Cargo_heavy_fuel_oil_yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Does the Assured(s) own or operate marine terminals,']/following::table[1]/descendant::td[1]/input[1]")
	public WebElement DoesTheAssuredSOwnOrOperateMarineTerminalsPipelinesOnshoreAndOrOffshoreOrAnyOtherFacilitiesWhichAreInAnyWayConnectedToTheAssuredSActivityAsAChartererAndOrCargoOwner;

	@TextType()
	@FindBy(xpath = "//label[text()='Does the Assured(s) perform landside carriage?']/following::table[1]/descendant::td[1]/input[1]")
	public WebElement Perform_landside_carriage_Yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Is the Assured(s) engaged in landside storage of cargo?']/following::table[1]/descendant::td[1]/input[1]")
	public WebElement Landside_storage_of_cargo_yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Does the Assured(s) sell bunkers?']/following::table[1]/descendant::td[1]/input[1]")
	public WebElement Assured_sell_bunkers_yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Any other relevant chartering and trading information']/following::table[1]/descendant::td[1]/input[1]")
	public WebElement Other_relevant_info_Yes;

	@TextType()
	@FindBy(xpath = "//label[text()='Name of the Assured(s):']/following::div[1]")
	public WebElement UICheck_NameOfAssured;

	@TextType()
	@FindBy(xpath = "//td[text()='Number of vessels chartered:']/following::td[1]")
	public WebElement UICheck_No_OfVessels;

	@TextType()
	@FindBy(xpath = "//td[text()='Number of vessels chartered:']/following::td[2]")
	public WebElement UICheck_NoOfVessel_VC;

	@TextType()
	@FindBy(xpath = "//td[text()='Number of vessels chartered:']/following::td[3]")
	public WebElement UICheck_Upcoming_NoOfVessels;

	@TextType()
	@FindBy(xpath = "//td[text()='Number of vessels chartered:']/following::td[4]")
	public WebElement UICheck_NoOfVes_VC;

	@TextType()
	@FindBy(xpath = "//td[text()='Duration of Charters (average # of days):']/following::td[1]")
	public WebElement UICheck_Current_Duration_TC;

	@TextType()
	@FindBy(xpath = "//td[text()='Duration of Charters (average # of days):']/following::td[2]")
	public WebElement UICheck_CurrentDuration_VC;

	@TextType()
	@FindBy(xpath = "//td[text()='Duration of Charters (average # of days):']/following::td[3]")
	public WebElement UICheck_UpcomingDurarion_TC;

	@TextType()
	@FindBy(xpath = "//td[text()='Duration of Charters (average # of days):']/following::td[4]")
	public WebElement UICheck_UpcomingDurationVC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel size (in GT):']/following::td[1]")
	public WebElement UICheck_Size_CurrentYr_TC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel size (in GT):']/following::td[2]")
	public WebElement UiCheck_CurrentYr_VesselSize_VC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel size (in GT):']/following::td[3]")
	public WebElement UiCheck_UpcomingYr_VesselSize_TC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel size (in GT):']/following::td[4]")
	public WebElement UICheck_UpcomingYr_VesselSize_VC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel age (years):']/following::td[1]")
	public WebElement UICheck_VesselAgeCurrnt_TC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel age (years):']/following::td[2]")
	public WebElement UICheck_Curnt_VCVesselAge;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel age (years):']/following::td[3]")
	public WebElement UICheck_VesselAge_UpcomingYrTC;

	@TextType()
	@FindBy(xpath = "//td[text()='Average vessel age (years):']/following::td[4]")
	public WebElement UICheck_VesselAge_Upcoming_VC;

	@TextType()
	@FindBy(xpath = "//td[text()='Geography:(please be specific including ports and routes)']/following::td[1]")
	public WebElement UICheck_Geo_CurrentIYr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Geography:(please be specific including ports and routes)']/following::td[2]")
	public WebElement UICheck_Geo_UpcmgYrIns;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Cargo type(s) and MT volumes:']/following::td[1]")
	public WebElement UiCheck_CargoTypeCI;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Cargo type(s) and MT volumes:']/following::td[2]")
	public WebElement UICheck_CargoType_UpcmgYr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='US trade as a % of total trade:']/following::td[1]")
	public WebElement UICheck_USTrade_CurrntYr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='US trade as a % of total trade:']/following::td[2]")
	public WebElement UICheck_USTradeUpcmg_Yr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Owned Cargo']/following::td[1]")
	public WebElement UICheck_OwnedCargo_CurntYr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Owned Cargo']/following::td[2]")
	public WebElement UICheck_OwnerCargo_UpcmgYr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Third Party Cargo']/following::td[1]")
	public WebElement UICheck_3rdPartyCurntYr;

	@TextType()
	@FindBy(xpath = "//td[normalize-space(text()) ='Third Party Cargo']/following::td[2]")
	public WebElement UICheck_3rdPartyUpcmgYr;

	@TextType()
	@FindBy(xpath = "//label[text()='Does the Assured(s) trade outside Institute trade limits or other limits for ice?']/following::div[1]")
	public WebElement DoestheAssuredLimitOfIce;

	@TextType()
	@FindBy(xpath = "(//label[text()='Frequency:']/following::div[1])[1]")
	public WebElement UICheckFreq1;

	@TextType()
	@FindBy(xpath = "(//label[text()='Frequency:']/following::div[1])[2]")
	public WebElement UICheckFreq02;

	@TextType()
	@FindBy(xpath = "(//label[text()='Frequency:']/following::div[1])[2]")
	public WebElement UICheckFreq03;

	@TextType()
	@FindBy(xpath = "(//label[text()='Applicable contract provisions:']/following::div[1])[1]")
	public WebElement UICheck_ACP1;

	@TextType()
	@FindBy(xpath = "(//label[text()='Geography:']/following::div[1])[1]")
	public WebElement UICheck_Geography;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Owners/Insured:']/following-sibling::input[@type='text']")
	public WebElement lay_up_Owners_Insured;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Number of crew onboard:']/following-sibling::input")
	public WebElement no_of_crewOnBoard;

	@TextType()
	@FindBy(name = "j_id0:pageFrame:fr1:uploadFile")
	public WebElement uploadFile;

	@TextType()
	@FindBy(css = "td.dp_current")
	public WebElement DateForAttachmentCurrentDate1;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Call sign:']/following-sibling::input[@type='text']")
	public WebElement CallSign;

	@ButtonType()
	@FindBy(xpath = "//label[normalize-space(.)='Classification:']/following-sibling::div//button")
	public WebElement Classification;

	@LinkType()
	@FindBy(xpath = "//label[text()=' Classification:']/following::li[2]/a//label/input[1]")
	public WebElement ClassificationValue;

	@TextType()
	@FindBy(css = "td.dp_current")
	public WebElement DateForAttachmentCurrentDate11;

	@ButtonType()
	@FindBy(xpath = "//div[@class='right-button-set']/input[@value='Submit' and @type='button' and contains(@class,'form-btn')]")
	public WebElement SubmitBtn;

	@LinkType()
	@FindBy(xpath = "//div[@id='mCSB_4_container']//a[normalize-space(.)='Fjord Havsel AS' and @data-normalized-text='Fjord Havsel AS']")
	public WebElement fjordHavselAS;

	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('div.form-submit-section > input')")
	public WebElement continue_;

	@TextType()
	@FindBy(xpath = "//a/span[1][text()='Fjord Havsel AS']")
	public WebElement Fjord_Havsel;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Application form for Mobile offshore unitAction']/div")
	public WebElement MobileOffshoreUnit_action;

	@LinkType()
	@FindBy(xpath = "//label[normalize-space(.)='Entry form for Ship owners Action Upload Download Fill in online']/parent::div/following-sibling::div[1]//a[normalize-space(.)='Fill in online' and contains(@class,'form-fill-online-link')]")
	public WebElement EntryFormforMobileOffshoreUnit;

	@LinkType()
	@FindBy(xpath = "//label[text()=' Time in GMT:']/following::button[1]")
	public WebElement TimeInGMT;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='23:00 GMT']")
	public WebElement _2300GMT;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Other statutory certification:']/following::input[1]")
	public WebElement StatutoryCertification;

	@TextType()
	@FindBy(xpath = "//label[text()='Give a short description of the vessel and any conversions:']/following-sibling::textarea[1]")
	public WebElement ship_owner_short_desc;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Limit of cover(in millions):']/following-sibling::input")
	public WebElement limitofcover;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)=concat('Registered owner',\"'\",'s address:')]/following-sibling::textarea[1]")
	public WebElement Registered_ownerName_and_Addr1;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)=concat('Registered owner',\"'\",'s national registration no:')]/following-sibling::textarea[1]")
	public WebElement Registered_ownerNationalRegNo;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Mortgagee(s), name and address:']/following-sibling::textarea[1]")
	public WebElement MortgageeName;

	@TextType()
	@FindBy(xpath = "//td[1]/input[1]")
	public WebElement Assured_Name;

	@ButtonType()
	@FindBy(xpath = "//div[@id='assured-country']//button")
	public WebElement Country_Assured;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='India']")
	public WebElement Country_india;

	@TextType()
	@FindBy(xpath = "//tr[1]/td[4]/input[1]")
	public WebElement City_Assured;

	@ButtonType()
	@FindBy(xpath = "//div[@id='capacity']//button[1]")
	public WebElement Capacity_Assured;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Crew Agents']")
	public WebElement crewAgents_Assured;

	@TextType()
	@FindBy(xpath = "//tr[1]/td[7]/input[1]")
	public WebElement natRegNo_Assured;

	@TextType()
	@FindBy(xpath = "//tr[1]/td/div/label")
	public WebElement TickOff;

	@LinkType()
	@FindBy(xpath = "//tr[1]/td/a[1]")
	public WebElement action_Assured;

	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[1]/input[1]")
	public WebElement Assured_Name1;

	@ButtonType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]//div[@id='memberType']//button[1]")
	public WebElement MemberType2;

	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='Co-Assured'])[2]")
	public WebElement coAssured2;

	@ButtonType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]//div[@id='assured-country']//button[1]")
	public WebElement Country_Assured1;

	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='Albania'])[2]")
	public WebElement Contry_albania2;

	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[4]/input[1]")
	public WebElement City2_Assured;

	@ButtonType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]//div[@id='capacity']//button[1]")
	public WebElement Capacity2_Assured;

	@LinkType()
	@FindBy(xpath = "(//span[text()='Other'])[2]")
	public WebElement Capacity2_other;

	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[6]/input[1]")
	public WebElement capacityOther2;

	@TextType()
	@FindBy(xpath = "//tr[@id='tableRow'][2]/td[7]/input[1]")
	public WebElement natRegNo2_Assured;

	@TextType()
	@FindBy(xpath = "//label[text()='Object name:']/following::span[1]")
	public WebElement object;

	@TextType()
	@FindBy(xpath = "//label[text()='Flag:']/following-sibling::div[1]")
	public WebElement FlagNorway;

	@TextType()
	@FindBy(xpath = "//label[text()='Call sign:']/following-sibling::div[1]")
	public WebElement CallSign1;

	@TextType()
	@FindBy(xpath = "//label[text()='Year of built:']/following-sibling::div[1]")
	public WebElement YearofBuilt;

	@TextType()
	@FindBy(xpath = "//label[text()='IMO no.:']/following::div[1]")
	public WebElement IMONo;

	@TextType()
	@FindBy(xpath = "//label[text()='Gross tonnage:']/following-sibling::div[1]")
	public WebElement GrossTonnage;

	@TextType()
	@FindBy(xpath = "//label[text()='Classification:']/following::div[1]")
	public WebElement Classification1;

	@TextType()
	@FindBy(xpath = "//label[text()='Limit of cover(in millions):']/following::div[1]")
	public WebElement Limit_of_cover_in_millions__;

	@TextType()
	@FindBy(xpath = "//label[text()='Currency :']/following::div[1]")
	public WebElement Currency;

	@TextType()
	@FindBy(xpath = "//div[text()='Give a short description of the vessel and any conversions:']/following::span[1]")
	public WebElement Give_a_short_description_of_the_vessel_and_any_conversions_;

	@TextType()
	@FindBy(xpath = "//div[text()='Other statutory certification:']/following::span[1]")
	public WebElement Other_statutory_certification;

	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Pick a date']")
	public WebElement DateforAttachmentRisk;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Albania']")
	public WebElement albania;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Affiliated/Associated Charterers']")
	public WebElement affiliatedAssociatedCharterers;

	@TextType()
	@FindBy(xpath = "//label[text()=' Choose client name']/following::a[2]/span[1]")
	public WebElement _ndClient;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Entry form for Mobile offshore unit']/div[1]")
	public WebElement EntryFormAction;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='04:00 GMT']")
	public WebElement _0400GMT;

	@TextType()
	@FindBy(xpath = "//label[text()='Any expected changes in the Assured and Co-Assureds’ for the upcoming Policy year?']/following::textarea[1]")
	public WebElement UpComingPolicyYearChanges;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Primary assured’s national registration number:']/following-sibling::input")
	public WebElement primary_assureds_natRegNo;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'capacity:')]/following::div[1]")
	public WebElement PrimaryAssuredCapacity;

	@LinkType()
	@FindBy(xpath = "//label[contains(text(),'capacity:')]/following::button[1]")
	public WebElement PrimaryassuredCapacity;

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Bareboat Charterers']")
	public WebElement BareboatChard;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Contact person name:']/following-sibling::input")
	public WebElement COntactPerson;

	@TextType()
	@FindBy(xpath = "//label[text()='Phone no:']/following::input[1]")
	public WebElement PhNo;

	@TextType()
	@FindBy(xpath = "//label[text()='Email:']/following::input[1]")
	public WebElement email;

	@TextType()
	@FindBy(xpath = "//label[@for='uw-mou-form-option']/div[1]")
	public WebElement MOUActionButton;

	@TextType()
	@FindBy(xpath = "//label[@for='uw-off-obj-form-option']/div[1]")
	public WebElement OffshoreObjectAction;

	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Charterers renewal questionnaire']/div[1]")
	public WebElement CharterersRenewal;
	
	@LinkType()
	@FindBy(xpath = "//li[3]/a[1]")
	public WebElement _rdClientSelection;
	@LinkType()
	@FindBy(xpath = "//label[text()=' Choose client name']/following::a[3]/span[1]")
	public WebElement SecondClient;
	@TextType()
	@FindBy(xpath = "//td[@class='dp_current']/preceding-sibling::td[1]")
	public WebElement ArrivalDateSelect1;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search']")
	public WebElement EnterCompanyName;

}
