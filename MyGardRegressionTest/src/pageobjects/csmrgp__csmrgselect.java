package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Csmrgp__csmrgselect"                                
               , summary=""
               , page="CsMrgSelect"
               , namespacePrefix="csmrgp"
               , object="Case"
               , connection="MyGardRegressionQABox"
     )             
public class csmrgp__csmrgselect {

	@LinkType()
	@FindBy(xpath = "(//div/a[@class='case-number-link'])[2]")
	public WebElement CaseNumber;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Set as Master' and @type='button']")
	public WebElement setAsMaster;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Merge' and @id='merge-btn' and @type='button']")
	public WebElement merge;
	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='Clean Merge Enable this option to close each child case as a duplicate without cloning any objects or fields. Nothing will be transferred to the master case.']/div/button")
	public WebElement CleanMergeMessage;
	@TextType()
	@FindBy(xpath = "(//div/div[@class='slds-popover__body'])[1]")
	public WebElement CleanMergeCheckboxMessage;
	
}
