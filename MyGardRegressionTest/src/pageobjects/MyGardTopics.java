package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardTopics"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardTopics {

	@TextType()
	@FindBy(xpath = "//h1[text()='Topics']")
	public WebElement Topics;
			
}
