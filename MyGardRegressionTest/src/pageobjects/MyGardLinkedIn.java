package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardLinkedIn"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardLinkedIn {

	@LinkType()
	@FindBy(xpath = "//h1[normalize-space(text()='Gard AS')]")
	public WebElement linkedIn;
			
}
