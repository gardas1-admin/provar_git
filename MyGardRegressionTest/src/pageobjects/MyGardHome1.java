package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardHome1"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class MyGardHome1 {

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='HOME']")
	public WebElement HomeTab;
	@TextType()
	@FindBy(xpath = "//div[text()='Service requests' and @class='request-updates single-tab deselected']")
	public WebElement ServiceRequestLink;
	@LinkType()
	@FindBy(xpath = "(//table[@class='requpdatetable']/tbody/tr/td[2]/a)[1]")
	public WebElement RequestID;
	@LinkType()
	@FindBy(xpath = "//td/div/div/div/a[normalize-space(.)='Switch to Lightning Experience']")
	public WebElement switchToLightningExperience;
	@TextType()
	@FindBy(xpath = "//input[@id='phSearchInput']")
	public WebElement searchBox;
	@TextType()
	@FindBy(xpath = "//table[@class='requpdatetable']/tbody/tr[1]/td[2]/a[1]")
	public WebElement requestID;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='View all service requests']")
	public WebElement viewAllServiceRequests;
	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='Crown Ship Builders Corp Ship Insurers Ltd. Search byAny wordExact phrase Check all Crown Ship Builder... Ship Insurers Ltd.']/div/button")
	public WebElement ClientFilter;
	@LinkType()
	@FindBy(xpath = "(//div[@class='client-list-section']/table/tbody/tr/td[2]/a)[1]")
	public WebElement viewAllReqID;
	@LinkType()
	@FindBy(xpath = "//a[@id='add_new_file_btn']")
	public WebElement addNewFile;
	@TextType()
	@FindBy(xpath = "//span[text()='File upload']/following::input[1]")
	public WebElement uploadFileComment;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Upload' and contains(@class,'form-btn') and @type='submit']")
	public WebElement upload;
	@TextType()
	@FindBy(xpath = "//div[text()[normalize-space() = 'Raised date']]/div")
	public WebElement DateSorting;
	@TextType()
	@FindBy(xpath = "//div[@id='new_case_comment']//textarea")
	public WebElement Comment;
	@ButtonType()
	@FindBy(xpath = "//div[@id='new_case_comment']/span/input[@type='submit' and @value='Submit']")
	public WebElement Submit;
	@ButtonType(file = true)
	@FindBy(xpath = "//div[@id='contact-info-pop-wrapper']/div/div/div/div/input")
	public WebElement FileUpload1;
	@ButtonType(file = true)
	@FindBy(xpath = "(//span[text()='Browse'])[2]")
	public WebElement Browse;		
}
