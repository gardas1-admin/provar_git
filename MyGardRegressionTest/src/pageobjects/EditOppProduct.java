package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Edit Opp Product"                                
               , summary=""
               , page="EditOppProduct"
               , namespacePrefix=""
               , object="Opportunity"
               , connection="MyGardRegressionUAT"
     )             
public class EditOppProduct {

	@PageTable(row = AvailableProducts.class)
	@VisualforceBy(componentXPath = "apex:pageblockTable[@value = \"{!availableProducts}\"]")
	public List<AvailableProducts> AvailableProducts;

	@PageRow(byColumn = true)
	public static class AvailableProducts {

		@ButtonType()
		@FindBy(xpath = "//div[text()='Product Name']/ancestor::thead[1]/following-sibling::tbody[1]/descendant::tr[1]/descendant::td[1]/descendant::input[1]")
		public WebElement SelectBtn;
	}
	
}
