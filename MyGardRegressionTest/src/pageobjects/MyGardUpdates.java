package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardUpdates"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardUpdates {

	@TextType()
	@FindBy(xpath = "//h1[text()='Updates']")
	public WebElement Updates;
	@TextType()
	@FindBy(xpath = "//h1[text()='Updates']")
	public WebElement Updates1;
			
}
