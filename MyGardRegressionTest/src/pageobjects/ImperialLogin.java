package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="ImperialLogin"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class ImperialLogin {

	@TextType()
	@FindBy(id = "sfid-username")
	public WebElement enterYourEmail;
			
}
