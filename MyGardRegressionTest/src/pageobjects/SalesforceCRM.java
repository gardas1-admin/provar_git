package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="Salesforce - Performance Edition"                                
     , connection="MyGardRegressionStaging"
     )             
public class SalesforceCRM {

	@TextType()
	@FindBy(xpath = "//input[@href='https://cs88.salesforce.com/home/home.jsp/']")
	public WebElement CRMHomePage;
	@TextType()
	@FindBy(xpath = "//input[@name=\"str\"]")
	public WebElement searchBox;
	@TextType()
	@FindBy(xpath = "//*[@id='userNavButton']")
	public WebElement UserNavigationBtn;
	@LinkType()
	@FindBy(xpath = "//a[@title='Setup']")
	public WebElement Setup;
	@TextType()
	@FindBy(xpath = "//input[@name=\"setupSearch\"]")
	public WebElement QuickFind;
	@LinkType()
	@FindBy(xpath = "//a[text()='Contact Roles on Accounts']")
	public WebElement ContactRolesOnAccounts;
	@LinkType()
	@FindBy(xpath = "//a[text()='Opportunities']/ancestor::li")
	public WebElement OpportuniesTab;
	@TextType()
	@FindBy(xpath = "//span[text()='Primary']/ancestor::div[2]/preceding-sibling::div[2]/descendant::input[1]")
	public WebElement SearchSelectContacts;
	@ButtonType()
	@FindBy(xpath = "//span[text()='Primary']/ancestor::div[2]/preceding-sibling::div[2]/descendant::input[2]")
	public WebElement SelectContactFindBtn;
	@TextType()
	@FindBy(xpath = "//span[contains(normalize-space(text()),'Amber Shipping Limited')]")
	public WebElement SelectContactsSearchedContactRoyEklund;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"mw_picker_add_button\"]")
	public WebElement SelectContactsAddRight;
	@TextType()
	@FindBy(xpath = "//span[contains(normalize-space(text()),'P&I Associates (Pty) Ltd..')]")
	public WebElement SelectContactsSearchedContactRoyCarby;
	@TextType()
	@FindBy(xpath = "//span[contains((text()),'Japan Ship Owners')]")
	public WebElement SelectContactsSearchedContactRoyDeitch;
	@TextType()
	@FindBy(xpath = "//span[contains(normalize-space(text()),'Maruba Asia Ltd.')]")
	public WebElement SelectContactSearchedContactRoyChoy;
	@ButtonType()
	@FindBy(xpath = "//input[@name=\"saveBtn\"][@id=\"mw_picker_save_btn\"]")
	public WebElement SelectContactsDoneBtn;
	@LinkType()
	@FindBy(xpath = "(//span[text()='View Meeting'])[1]")
	public WebElement ViewMeeting;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='User name:']/following-sibling::input[1]")
	public WebElement UserName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Password:']/following-sibling::input")
	public WebElement Password;
	@LinkType()
	@FindByLabel(label = "Switch to Lightning Experience")
	public WebElement switchToLightningExperience;
	@LinkType()
	@FindBy(xpath = "//*[@role='list']/descendant::a[1]/span")
	public WebElement Homelink;
	@LinkType()
	@FindBy(xpath = "//*[@role='list']/descendant::a[2]/span")
	public WebElement ChatterLink;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Companies']")
	public WebElement Companies_Link;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Opportunities']")
	public WebElement Opportunities_Link;
	@LinkType()
	@FindBy(xpath = "//a/span[text()='Reports' and normalize-space(.)='Reports']")
	public WebElement Reports_Link;
	@LinkType()
	@FindBy(xpath = "//a/span[text()='Dashboards' and normalize-space(.)='Dashboards']")
	public WebElement Dashboards;
	@LinkType()
	@FindBy(xpath = "//a/span[text()='Events' and normalize-space(.)='Events']")
	public WebElement Events;
	@LinkType()
	@FindBy(xpath = "//a/span[text()='Groups' and normalize-space(.)='Groups']")
	public WebElement Groups;
	@LinkType()
	@FindBy(xpath = "//a/span[text()='People' and normalize-space(.)='People']")
	public WebElement People;
	@LinkType()
	@FindBy(xpath = "//a/span[text()='Cases']")
	public WebElement Cases_Link;
	@TextType()
	@FindBy(xpath = "(//span[@class='slds-card__header-title slds-truncate'])[1]")
	public WebElement QuarterlyPerformance_Title;
	@TextType()
	@FindBy(xpath = "(//span[@class='slds-card__header-title slds-truncate'])[2]")
	public WebElement TodayEvent;
	@TextType()
	@FindBy(xpath = "(//span[@class='slds-card__header-title slds-truncate'])[3]")
	public WebElement RecentRecords;
	@TextType()
	@FindBy(xpath = "(//span[@class='slds-card__header-title slds-truncate'])[4]")
	public WebElement TodaysTasks;
	@TextType()
	@FindBy(xpath = "(//span[@class='slds-card__header-title slds-truncate'])[5]")
	public WebElement KeyDeals;
	@TextType()
	@FindBy(id = "userNavLabel")
	public WebElement ClassicUserMenu;
	@TextType()
	@FindBy(xpath = "//span[@id='userNavLabel']")
	public WebElement ClassicUserMenu1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Logout']")
	public WebElement ClassicLogout;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Password']/following-sibling::input")
	public WebElement ReLaunchPassword;
	@ButtonType()
	@FindBy(xpath = "//input[@id='Login']")
	public WebElement LoginToSandBox;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New' and @role='button' and @title='New']")
	public WebElement NewCompany;
	@TextType()
	@FindBy(xpath = "//input[@id='input-150' and @placeholder='Search this list...' and @type='search']")
	public WebElement SearchThisListOpportunity;
	@TextType()
	@FindBy(xpath = "//input[@name='Opportunity-search-input' and contains(@class,'slds-input') and @placeholder='Search this list...' and @type='search']")
	public WebElement SearchTheOpportunity;
	@LinkType()
	@FindBy(xpath = "//span/span[text()='Home']")
	public WebElement HomePage;
	@LinkType()
	@FindBy(xpath = "//a/span[normalize-space(.)='Claims' ]")
	public WebElement claims;
	@ButtonType()
	@FindBy(xpath = "//button[@title='Show Navigation Menu']")
	public WebElement ShowNavigationMenu;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search Cases and more...' and @title='Search Cases and more']")
	public WebElement searchCasesAndMore;
	@TextType()
	@FindBy(xpath = "//div/input[@title='Search Salesforce' and @type='text']")
	public WebElement MainsearchBox;
	@LinkType()
	@FindBy(xpath = "(//table)[2]/tbody/tr/td[5]/span/div/div/div/a[1]")
	public WebElement CASES_SUBJECT;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='My Next 7 days Meetings' and @title='My Next 7 days Meetings']")
	public WebElement myNext7DaysMeetings;
	@TextType()
	@FindBy(xpath = "//div[@class='hideSelection forceListViewManagerGrid']/descendant::table/tbody/tr/td[3]/span/span[1]")
	public WebElement StartDate;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Show Navigation Menu']")
	public WebElement showNavigationMenu;
	@TextType()
	@FindBy(xpath = "//button[normalize-space(.)='View profile']/div/span/div/span")
	public WebElement viewProfile;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Switch to Salesforce Classic']")
	public WebElement switchToSalesforceClassic;
	@LinkType()
	@FindBy(xpath = "//div[@id='Contact_body']/descendant::tr[2]/th/a[1]")
	public WebElement ContactName;
	@TextType()
	@FindBy(xpath = "//span[@id='workWithPortalLabel']")
	public WebElement ManageExternalUser;
	@LinkType()
	@FindBy(xpath = "//div[@id='workWithPortalMenu']/a[@name='networklogin']")
	public WebElement logInToExperienceAsUser;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Pacific Gas Chartering Limited; Shanghai, China']")
	public WebElement pacificGasCharteringLimitedShanghaiChina;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Willis Towers WatsonRotterdam, Netherlands']")
	public WebElement MultiCompPopup;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Continue']")
	public WebElement continue_;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Ok']")
	public WebElement ok;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Contacts']")
	public WebElement contacts;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='App Launcher']")
	public WebElement appLauncher;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Search apps and items...']/following-sibling::div//input")
	public WebElement searchAppsAndItems;
	@LinkType()
	@FindBy(xpath = "//span/p/b[text()='Customer Transactions']")
	public WebElement customerTransactions;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Client Activities']")
	public WebElement clientActivities;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Cases']")
	public WebElement cases;
	@LinkType()
	@FindBy(xpath = "(//table/tbody/tr/th)[1]/span/a[1]")
	public WebElement ClientActivity;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search Companies and more...']")
	public WebElement searchCompaniesAndMoreEinstenSearchBox;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Search...']")
	public WebElement search;
	@TextType()
	@FindBy(xpath = "//div/div/lightning-input/div/input[@type='search' and @placeholder='Search...']")
	public WebElement searchCompaniesAndMoreEinstenSearchBox1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='User Detail']")
	public WebElement userDetail;
	@PageFrame()
	public static class Frame {

		@ButtonType()
		@FindBy(xpath = "//div[@class='pbHeader']/descendant::input[@name='login']")
		public WebElement login;
	}
	@FindBy(xpath = "//iframe[@title='User: Gudrun Mortensen Aaserud ~ Salesforce - Performance Edition']")
	public Frame frame;
			
}
