package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="GardOutlookMail"                                
     , summary=""
     , relativeUrl=""
     , connection="GardMail"
     )             
public class GardOutlookMail {

	@TextType()
	@FindBy(xpath = "//input[@name='loginfmt' and @placeholder='Email or phone' and @type='email']")
	public WebElement OutlookemailOrPhone;
	@ButtonType()
	@FindBy(xpath = "//input[@type='submit' and @value='Next']")
	public WebElement next;
	@TextType()
	@FindBy(xpath = "//input[@name='passwd' and @placeholder='Password' and @type='password']")
	public WebElement password;
	@ButtonType()
	@JavascriptBy(jspath = "return document.querySelector('input.form-control.ltr_override.input.ext-input.text-box.ext-text-box')")
	public WebElement signIn;
	@ButtonType()
	@FindBy(xpath = "//input[contains(@class,'btn') and @type='submit' and @value='Yes']")
	public WebElement yes;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='New message' and @type='button']")
	public WebElement newMessage;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Nirmal KumarNK' and @role='button' and @type='button']")
	public WebElement nirmalKumarNK;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Sign out']")
	public WebElement signOut;
	@TextType()
	@FindBy(xpath = "//input[@id='identifierId']")
	public WebElement EmailID;
	@ButtonType()
	@FindBy(xpath = "//div[@id='identifierNext']//button")
	public WebElement Next;
			
}
