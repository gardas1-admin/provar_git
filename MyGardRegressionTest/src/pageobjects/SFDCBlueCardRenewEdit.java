package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCBlueCardRenewEdit"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCBlueCardRenewEdit {

	@BooleanType()
	@FindBy(xpath = "//label[text()='BlueCardRenewalAvailability']/ancestor::th[1]/following-sibling::td[1]/descendant::input[1]")
	public WebElement BlueCardRenewalAvailabilityCheckBox;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Save']")
	public WebElement SaveButton;
			
}
