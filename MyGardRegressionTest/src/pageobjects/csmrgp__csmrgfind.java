package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Csmrgp__csmrgfind"                                
               , summary=""
               , page="CsMrgFind"
               , namespacePrefix="csmrgp"
               , object=""
               , connection="MyGardRegressionQABox"
     )             
public class csmrgp__csmrgfind {

	@TextType()
	@FindBy(xpath = "//div[@id='1criteriaValueContainer']/input")
	public WebElement SearchSubjectVerification;
	@TextType()
	@FindBy(xpath = "//table/tbody/tr[2]/th/div/div/label[1]")
	public WebElement AddCaseDupe;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Merge' and @type='button']")
	public WebElement merge;
	@ButtonType()
	@VisualforceBy(componentXPath = "apex:form[@id='pgForm']//div[contains(@class, \"outer-container\")]/div[1]/div[1]")
	public WebElement delete;
	
}
