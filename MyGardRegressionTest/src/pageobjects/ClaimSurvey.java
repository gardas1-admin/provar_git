package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="ClaimSurvey"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardE2E"
     )             
public class ClaimSurvey {



	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Send claim survey']")
	public WebElement SendClaimSurvey;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search Contacts']")
	public WebElement searchContacts;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Send survey']")
	public WebElement sendSurveyBtn;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Cancel' and @title='Cancel']")
	public WebElement cancel;
	@ButtonType()
	@FindBy(xpath = "(//button[normalize-space(.)='Send survey'])[2]")
	public WebElement sendSurveyBtn1;
			
}
