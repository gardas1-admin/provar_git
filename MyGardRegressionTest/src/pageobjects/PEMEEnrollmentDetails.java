package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PEMEEnrollmentDetails"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class PEMEEnrollmentDetails {

	@ButtonType()
	@FindBy(xpath = "//input[@title='Go!']")
	public WebElement SearchGo;
	@LinkType()
	@FindBy(xpath = "//div[normalize-space(text())='PEME Enrollment Form Name']/ancestor::thead[1]/ancestor::div[3]/following-sibling::div[1]/descendant::tr[1]/descendant::a[4]")
	public WebElement PEMEEnrolledFormNumber;
	@LinkType()
	@FindBy(xpath = "//th[text()='PEME Manning Agent Name']/ancestor::tr[1]/following-sibling::tr[1]/descendant::a[3]")
	public WebElement ManningAgentName;
	@ButtonType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::input[1]")
	public WebElement EditBtn;
	@ButtonType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::input[4]")
	public WebElement ApprovalRejectBtn;
	@LinkType()
	@FindBy(xpath = "//th[text()='PEME Manning Agent Name']/ancestor::tr[1]/following-sibling::tr[1]/descendant::td[text()='Approved']")
	public WebElement ManningAgentDetailStatus;
	@TextType()
	@FindBy(xpath = "//*[@id=\"01ID0000000un9t.00ND0000006IKMB-_help\"]//img")
	public WebElement GardPEMERef;
	@LinkType()
	@FindBy(xpath = "//td[text()='PEME Enrollment Form']/following-sibling::td[1]/descendant::a[1]")
	public WebElement PEMEEnrolmentFormNumber;
			
}
