package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PEMEApproveRejectRequest"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class PEMEApproveRejectRequest {

	@TextType()
	@FindBy(xpath = "//label[normalize-space(text())='Comments']/ancestor::th[1]/following-sibling::td[1]/descendant::textarea[1]")
	public WebElement ApproveRejectComments;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Reject']")
	public WebElement RejectBtn;
	@ButtonType()
	@FindBy(xpath = "//input[@value='Approve']")
	public WebElement ApproveBtn;
			
}
