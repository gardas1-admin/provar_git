package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="HomeTab"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class HomeTab {

	@LinkType()
	@FindBy(xpath = "//a[text()='Home']")
	public WebElement Home;
	@TextType()
	@FindBy(xpath = "//span[text()='Create New...']/ancestor::div[1]")
	public WebElement CreateNewLabelPicklist;
	@LinkType()
	@FindBy(xpath = "//input[@value='New Event']")
	public WebElement Event;
	@ButtonType()
	@FindBy(xpath = "//span[text()='View profile']/ancestor::div[1]")
	public WebElement ViewProfile;
	@LinkType()
	@FindBy(xpath = "//h3[text()='OPTIONS']/following-sibling::div[1]/descendant::a[1]")
	public WebElement SwitchToSalesforceClassic;
	@TextType()
	@FindBy(xpath = "//input[@placeholder='Search...']")
	public WebElement GlobalSearch;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"Account_Tab\"]//a")
	public WebElement companies;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Search...']")
	public WebElement search;
			
}
