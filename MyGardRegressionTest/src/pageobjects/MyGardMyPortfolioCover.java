package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardMyPortfolioCover"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class MyGardMyPortfolioCover {

	@TextType()
	@FindBy(xpath = "//div[contains(normalize-space(text()),'Client')]/ancestor::thead[1]/following-sibling::tbody[1]/descendant::td[1]")
	public WebElement ClientResult;
	@LinkType()
	@FindBy(xpath = "//a[text()='Forms']/ancestor::div[1]/following-sibling::div[1]")
	public WebElement PortfolioReportsTab;
	@LinkType()
	@FindBy(xpath = "//a[text()='Forms']/ancestor::div[1]/preceding-sibling::div[1]")
	public WebElement MyPortfolioCoverObjects;
			
}
