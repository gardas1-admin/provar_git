package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardHomeSerReq"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class MyGardHomeSerReq {

	@LinkType()
	@FindBy(xpath = "(//table[@class='requpdatetable']/tbody/tr[@class='odd']/td)[1]")
	public WebElement SerReq_Client;
	@TextType()
	@FindBy(xpath = "//a[@class='vcard-download']/following::div[@class='info-name']")
	public WebElement UnderWriterName;
			
}
