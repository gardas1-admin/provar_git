package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCBlueCardRenewSettings"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCBlueCardRenewSettings {

	@LinkType()
	@FindBy(xpath = "//th[text()='Action']/ancestor::tr[1]/following-sibling::tr[1]/descendant::a[1]")
	public WebElement EditLink;
			
}
