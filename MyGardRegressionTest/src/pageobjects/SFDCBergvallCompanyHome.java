package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SFDCBergvallCompanyHome"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class SFDCBergvallCompanyHome {

	@BooleanType()
	@FindBy(xpath = "//td[text()='Broker On Risk']/following-sibling::td[1]/descendant::img[@title='Checked']")
	public WebElement BrokerOnRiskFlag;
	@LinkType()
	@FindBy(xpath = "//th[text()='Contact Name']/ancestor::tr[1]/following-sibling::tr[1]/descendant::a[3]")
	public WebElement ContactBjornNorseth;
	@ButtonType()
	@FindBy(xpath = "//input[@title='New Shared Company']")
	public WebElement NewSharedCompanyBtn;
	@LinkType()
	@FindBy(xpath = "//span[text()='MyGard data sharing: shared data']")
	public WebElement MyGardDataSharingSharedData;
			
}
