package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceCRMOpportunity"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegUAT"
     )             
public class SalesforceCRMOpportunity {

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New']")
	public WebElement New_button;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, 'active') and contains(@class, 'open') and (contains(@class, 'forceModal') or contains(@class, 'uiModal'))][last()]//button[normalize-space(.)='Next']")
	public WebElement NextBtn;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Company Name*']/following-sibling::div//input")
	public WebElement companyName;
	@LinkType()
	@FindBy(xpath = "(//tbody)[2]/tr/td[1]")
	public WebElement CompanyNameSelection;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Opportunity NameThis field populates automatically with Company name, record type, year and type of business + entered text*']/following-sibling::input")
	public WebElement OpportunityName;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Type of Business']/following::a[1]")
	public WebElement typeOfBusiness;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Renewal']")
	public WebElement RenewalDropdown;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Budget Year']/following::a[1]")
	public WebElement BudgetYearDD;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='2019']")
	public WebElement YearSelecion_2019;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Sales Channel']/following::a[1]")
	public WebElement SalesChannelDD;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Broker']")
	public WebElement BrokerDD;
	@LinkType()
	@FindBy(xpath = "//label[normalize-space(.)='Close DateDay the opportunity must be closed. This value changes automatically to the day the opportunity is closed won.*']/following-sibling::div//a")
	public WebElement CloseDate;
	@TextType()
	@FindBy(xpath = "//span[@class='slds-day weekday todayDate selectedDate DESKTOP uiDayInMonthCell--default']")
	public WebElement TodayCloseDate;
	@LinkType()
	@FindBy(xpath = "//span[normalize-space(.)='Sales Stage']/following::div[2]\n")
	public WebElement SalesStage;
	@LinkType()
	@FindBy(xpath = "//div[@class='slds-clearfix slds-card groupDependentFieldEnabled forceDetailPanelDesktop']/descendant::span[text()='Sales Stage']/following::a[1]")
	public WebElement SalesStageDD;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Renewable Opportunity']")
	public WebElement RenewableOpportunitySelection;
	@ButtonType()
	@FindBy(xpath = "//button[@type='button' and text()='Save']")
	public WebElement Save_Btn;
	@ButtonType()
	@FindBy(xpath = "//button[@class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']/span[text()='Save']")
	public WebElement Save_Btn1;
	@BooleanType()
	@FindBy(xpath = "//span[text()='Marine']/../../descendant::input[1]")
	public WebElement MarineRadioButton;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Cancel' and contains(@class,'slds-button') and @title='Cancel' and @type='button']")
	public WebElement OppCancel;
	@TextType()
	@FindBy(xpath = "//label/span[text()='Close Date']/following::input[1]")
	public WebElement closeDate;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Close this window' and contains(@class,'slds-button') and @title='Close this window' and @type='button']")
	public WebElement closeThisWindow;
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@LinkType()
	@FindBy(xpath = "//span/a[normalize-space(.)='AKOFS Offshore AS' and contains(@class,'slds-truncate') and @title='AKOFS Offshore AS']")
	public WebElement ClientCompanyName;
	@LinkType()
	@FindBy(xpath = "//button[normalize-space(.)='Edit' and @type='button']")
	public WebElement EditOpportunity;
	@LinkType()
	@FindBy(xpath = "//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']/div/descendant::span[text()='Sales Stage']/following::a[1]")
	public WebElement SalesStage1;
	@LinkType()
	@FindBy(xpath = "//li/a[text()='Risk Evaluation' and @title='Risk Evaluation']")
	public WebElement riskEvaluation;
	@ButtonType()
	@FindBy(xpath = "//button[@title='Save' and normalize-space(.)='Save' and contains(@class,'slds-button') and @type='button']")
	public WebElement Save_Button;
	@LinkType()
	@FindBy(xpath = "//*[normalize-space(.)='Submit for Approval' and @type='button' and @name='Submit']")
	public WebElement submitForApproval;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Submit' and contains(@class,'slds-button') and @type='button']")
	public WebElement submit;
	@LinkType()
	@FindBy(xpath = "//div/span[text()='Opportunity Checklists' and normalize-space(.)='Opportunity Checklists']")
	public WebElement opportunityChecklists;
	@PageRow()
	public static class Opp_Checklists {

		@LinkType()
		@FindBy(xpath = ".//td/div/a[normalize-space(.)='Del']")
		public WebElement DeleteChecklist;
		@LinkType()
		@FindBy(xpath = "//td//a[normalize-space(.)='Edit']")
		public WebElement EditChecklist;
	}
	@AuraBy(componentXPath = "//aura:iteration[@iterationListName = 'v.Opp_Checklists']")
	@PageTable(firstRowContainsHeaders = false, row = Opp_Checklists.class)
	public List<Opp_Checklists> opp_Checklists;
	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='Opportunities > Petro Services (P&I) : 2019 - Renewal (PI,GL)Opportunity ChecklistsOpportunity ChecklistsNew']//button[normalize-space(.)='New' and contains(@class,'slds-button') and @type='button']")
	public WebElement NewChecklist;
	@TextType()
	@FindBy(xpath = "//span[text()='Owners checklist']/../span[1]")
	public WebElement value;
	@LinkType()
	@FindBy(xpath = "//*[normalize-space(.)='Close won' and @type='button']")
	public WebElement CloseWonButton;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Only approved opportunities can be changed to Closed Won.']")
	public WebElement OnlyApprovedOpportunities;
	@LinkType()
	@FindBy(xpath = "//a/span[contains(text(),'Approval History') and @title='Approval History']")
	public WebElement ApprovalHistory;
	@LinkType()
	@FindBy(xpath = "(//span/a[normalize-space(.)='Opportunity Owner Approval' and @title='Opportunity Owner Approval'])[1]")
	public WebElement Opportunity_Owner_Approval;
	@LinkType()
	@FindBy(xpath = "//header//a[normalize-space(.)='Approve' and @role='button' and @title='Approve']")
	public WebElement Approve;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Approve' and contains(@class,'slds-button') and @type='button']")
	public WebElement approve;
	@LinkType()
	@FindBy(xpath = "(//div[@class='slds-form slds-form_stacked ']/following::span/a[normalize-space(.)='AKOFS Offshore (P&I) : 2020 - Renewal Test Automation' and contains(@class,'outputLookupLink') and @target='_blank' and @title='AKOFS Offshore (P&I) : 2020 - Renewal Test Automation'])[2]")
	public WebElement ReturntoOpportunity;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Show 6 more actions' and contains(@class,'slds-grid') and @role='button' and @title='Show 6 more actions']")
	public WebElement trigger;
	@LinkType()
	@FindBy(xpath = "//*[normalize-space(.)='Close won' and @type='button']")
	public WebElement closeWon;
	@LinkType()
	@FindBy(xpath = "//div/span[text()='Opportunity']/following::a[1]")
	public WebElement ReturntoOppFromChecklist;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='You must create a checklist and mark the tick box \"Send checklist to Customer Transactions\" before a P&I Opportunity can be set to Closed Won.']")
	public WebElement SendChecklistandMarktheTickBox;
	@LinkType()
	@FindBy(xpath = "(//lightning-icon/span[text()='Show more actions'])[2]")
	public WebElement ShowMore;
	@LinkType()
	@AuraBy(componentXPath = "//ui:menuTriggerLink[@aura:id= 'actionsMenuTrigger']")
	public WebElement trigger1;
	@LinkType()
	@FindBy(xpath = "//ul[@data-aura-class='oneActionsRibbon forceActionsContainer']/li[4]/descendant::a[1]")
	public WebElement trigger2;
	@TextType()
	@FindBy(xpath = "//h2/a[normalize-space(.)='Opportunity Checklists(1)']//span[normalize-space(.)='Opportunity Checklists' and contains(@class,'slds-card__header-title')]")
	public WebElement opportunityChecklists1;
	@PageRow()
	public static class Opp_Checklists1 {
	}
	@AuraBy(componentXPath = "//aura:iteration[@iterationListName = 'v.Opp_Checklists']")
	@PageTable(firstRowContainsHeaders = false, row = Opp_Checklists1.class)
	public List<Opp_Checklists1> opp_Checklists1;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Send checklist to Customer TransactionsThe checklist information is ready to be handed over from the UWR to CT']/following-sibling::input[@type='checkbox']")
	public WebElement sendChecklistToCustomerTransactions;
	@LinkType()
	@FindBy(xpath = "//div/div/a[normalize-space(.)='Petro Services (P&I) : 2019 - Renewal (PI,GL)']")
	public WebElement ReturntoOpportunityfromChecklists;
	@LinkType()
	@FindBy(xpath = "(//ul/li[@class='slds-button slds-button--icon-border-filled oneActionsDropDown'])[1]/div/div/div/div/a[1]")
	public WebElement trigger3;
	@FindBy(xpath = "(//div[text()='Reopen opportunity'])/..")
	@LinkType()
	public WebElement reopenOpportunity;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Close Lost/Declined' and @role='menuitem' and @title='Close Lost/Declined']")
	public WebElement closeLostDeclined;
	@PageFrame()
	public static class Frame {

		@FindBy(xpath = "//label[normalize-space(.)='Reason Lost/Declined']/parent::th/following-sibling::td//select")
		@TextType()
		public WebElement reasonLostDeclined;
	}
	@FindBy(xpath = "//iframe[@name='vfFrameId_1568728074001']")
	public Frame frame;
	@PageFrame()
	public static class Frame1 {
	}
	@LinkType()
	@FindBy(xpath = "//article/div/div/div/div/div/div/div/div/div/div/div/div/div/div/ul//a[normalize-space(.)='Jonathan BurleyPress Delete to Remove']/a[normalize-space(.)='Press Delete to Remove']")
	public WebElement pressDeleteToRemove;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='A broker contact that is not set as no longer employed is required when sales channel is set to broker.']")
	public WebElement SalesChannelisSettoBroker;
	@TextType()
	@FindBy(xpath = "//label[text()='Broker Contact']/following::input[1]")
	public WebElement brokerContact;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Jonathan Burley' and contains(@class,'outputLookupLink') and @rel='noreferrer' and @target='_blank' and @title='Jonathan Burley']")
	public WebElement jonathanBurley;
	@LinkType()
	@FindBy(xpath = "//header//a[normalize-space(.)='Reject' and @role='button' and @title='Reject']")
	public WebElement reject;
	@TextType()
	@AuraBy(componentXPath = "//force:modal//ui:inputTextArea[@label= 'Comments']")
	public WebElement comments;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Reject' and contains(@class,'slds-button') and @type='button']")
	public WebElement reject1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New Business' and @role='menuitemradio' and @title='New Business']")
	public WebElement newBusiness;
	@BooleanType()
	@FindBy(xpath = "//label[normalize-space(.)='Accounting status checkedOk to submit for approval even if the \"Company accounting status\" is bankrupt/legal recovery']/following-sibling::input[@type='checkbox']")
	public WebElement accountingStatusChecked;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Mr. Jonathan BurleyPress Delete to Remove']/a[normalize-space(.)='Press Delete to Remove' and contains(@class,'deleteAction')]")
	public WebElement pressDeleteToRemove1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Mr. Jonathan Burley' and contains(@class,'outputLookupLink') and @data-aura-class='forceOutputLookup' and @target='_blank' and @title='Mr. Jonathan Burley']")
	public WebElement mrJonathanBurley;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Up Sell' and @title='Up Sell']")
	public WebElement upSell;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Please make sure that the Renewal index field is filled in for all products, under the Products related list']")
	public WebElement RenewalIndexMessage;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Loss of Hire (LOH)' and contains(@class,'textUnderline') and @data-aura-class='forceOutputLookup']")
	public WebElement Product;
	@LinkType()
	@FindBy(xpath = "//div[@class='slds-col slds-no-flex slds-grid slds-grid_vertical-align-center actionsContainer']/following::div[text()='Edit']")
	public WebElement EditProduct;
	@TextType()
	@FindBy(xpath = "//label/span[text()='Renewal Index (Prognosis)']/following::input[1]")
	public WebElement RenewalIndex;
	@TextType()
	@FindBy(xpath = "(//div[@class='flexipagePage']/following::div/div/ul[@class='branding-actions slds-button-group slds-m-left--xx-small oneActionsRibbon forceActionsContainer'])[2]/descendant::a[1]")
	public WebElement EditProduct1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='DWH Opportunity Id']/following-sibling::input[@type='text']")
	public WebElement DWH_Opportunity_Id;
	@TextType()
	@FindBy(xpath = "//label[text()='Broker Contact']/following::input[1]")
	public WebElement BrokerContact;
	@TextType()
	@FindBy(xpath = "//span[text()='Country']/following::input[1]")
	public WebElement Country;
	@BooleanType()
	@FindBy(xpath = "//span[text()='Accounting status checked']/following::input[1]")
	public WebElement AccountingStatusCheck;
	@LinkType()
	@FindBy(xpath = "//span[text()='Risk']/following::a[1]")
	public WebElement Risk;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Ship Owners']")
	public WebElement RiskShipOwners;
	@TextType()
	@FindBy(xpath = "//span/span[normalize-space(.)='Self Approval' and @title='Self Approval']")
	public WebElement Approval_Criteria;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Move selection to Chosen' and @title='Move selection to Chosen' and @type='button']")
	public WebElement MovetoChoosen;
	@TextType()
	@FindBy(xpath = "//div/div/div/div/div/div/div[normalize-space(.)='No applicable approval process was found.']")
	public WebElement NoApplicableApproval;
	@TextType()
	@FindBy(xpath = "(//div[normalize-space(.)='No applicable approval process was found.'])[2]")
	public WebElement NoApplicableApproval1;
	@ButtonType()
	@FindBy(xpath = "//span[normalize-space(.)='Opportunities > Crown Ship Buil (Marine) : 2020 - New Business Test OpportunityOpportunity ChecklistsOpportunity ChecklistsNew']//button[normalize-space(.)='New' and contains(@class,'slds-button') and @type='button']")
	public WebElement NewChecklist1;
	@TextType()
	@FindByLabel(label = "Product Manager Approver")
	public WebElement PM_Approver__c;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Responsible VPSet with the VP responsible of the opportunity']/following-sibling::div//input[@title='Search People' and @type='text']")
	public WebElement ResposibleVP;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='SVPEnter Senior Vice President']/following-sibling::div//input[@role='combobox' and @title='Search People' and @type='text']")
	public WebElement SVP_Approver__c;
	@LinkType()
	@FindBy(xpath = "//ul/li/a[normalize-space(.)='Renewable Opportunity']")
	public WebElement RenewableOpportunitySelection1;
	@LinkType()
	@FindBy(xpath = "//span[text()='Show more actions']/..")
	public WebElement ShowMore1;
	@LinkType()
	@FindBy(xpath = "(//span[text()='Approval History']/following::a[@class='slds-button slds-button--icon-x-small slds-button--icon-border-filled'])[1]")
	public WebElement ApprovalHistoryShowMore;
	@LinkType()
	@FindBy(xpath = "//li/a/div[normalize-space(.)='Approve']")
	public WebElement ApprovalProcessApprove;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Close Lost/Declined']")
	public WebElement Close_Lost_Declined;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Opportunity Market Area']/following-sibling::div//input[@placeholder='Search Market Areas...' and @role='combobox' and @title='Search Market Areas' and @type='text']")
	public WebElement Opportunity_Market_Area;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Marine Builders RisksPress Delete to Remove']/a[normalize-space(.)='Press Delete to Remove']")
	public WebElement Agreement_Market_Area_Close;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='New' and contains(@class,'slds-button') and @type='button']")
	public WebElement newChklist;
	@LinkType()
	@FindBy(xpath = "(//a[normalize-space(.)='AKOFS Offshore (P&I) : 2021 - Renewal Test Opportunity' and contains(@class,'slds-truncate') and @title='AKOFS Offshore  (P&I) : 2021 - Renewal Test Opportunity'])[1]")
	public WebElement Existing_Opportunity;
	@LinkType()
	@FindBy(xpath = "//div[@class='test-id__section-content slds-section__content section__content']/following::span[text()='Opportunity Name']/following::a[1]")
	public WebElement Existing_Opportunity1;
	@TextType()
	@FindBy(xpath = "//span[text()='Self Approval' and normalize-space(.)='Self Approval' and @title='Self Approval']")
	public WebElement SelfApproval;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='A broker contact that is not set as no longer employed is required when sales channel is set to broker.']")
	public WebElement SalesChannelSettoBroker;
	@TextType()
	@FindBy(xpath = "(//span/img[@title='Approval History'])[2]/following::a/span[text()='Approval History']/following::a[1]")
	public WebElement ApprovalHistoryProcessSteps;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Marine']")
	public WebElement OppRecordTypeMarine;
	@LinkType()
	@FindBy(xpath = "//div/a[normalize-space(.)='USD - U.S. Dollar']")
	public WebElement OpportunityDefaulCurrency;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Under Approval']")
	public WebElement StageunderApproval;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Quote' and @title='Quote']")
	public WebElement StageQuote;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Closed Declined' and @title='Closed Declined']")
	public WebElement StageClosedDeclined;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Closed Lost' and @title='Closed Lost']")
	public WebElement StageClosedLost;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Closed Other' and @title='Closed Other']")
	public WebElement StageClosedOther;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Closed Won' and @title='Closed Won']")
	public WebElement StageClosedWon;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Agreement Reference']")
	public WebElement Agreement_Reference__c;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Agreement Reference']/following-sibling::input[contains(@class,'input') and @type='text']")
	public WebElement Agreement_Reference__c1;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Description']/following-sibling::textarea[contains(@class,'textarea') and @role='textbox']")
	public WebElement Description;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='MOUs']")
	public WebElement RiskMOUs;
	@TextType()
	@FindBy(xpath = "//span/div/div/span[normalize-space(.)='P&I']")
	public WebElement OppTypeP_I;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='2021' and @role='menuitemradio' and @title='2021']")
	public WebElement Budget_Year_2020;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Direct' and @role='menuitemradio' and @title='Direct']")
	public WebElement SalesChannelDirect;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='AKOFS Offshore (Marine) : 2021 - New Business Test Opportunity' and @title='AKOFS Offshore  (Marine) : 2021 - New Business Test Opportunity']")
	public WebElement ReturnToOppFromApprovalPage;
	@LinkType()
	@FindBy(xpath = "//a[text()='AKOFS Offshore  (P&I) : 2021 - New Business Test Opportunity']")
	public WebElement ReturnToOppFromApprovalPageP;
	@LinkType()
	@FindBy(xpath = "//li/a[text()='Petro Services  (P&I) : 2021 - Renewal Test Opportunity']")
	public WebElement ReturnToOppFromApprovalPagePetroP;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='AKOFS Offshore (Marine) : 2021 - Up Sell Test Opportunity' and @title='AKOFS Offshore  (Marine) : 2021 - Up Sell Test Opportunity']")
	public WebElement ReturnToOppFromChkListUpSellBiz;
	@LinkType()
	@FindBy(xpath = "//li/a[text()='AKOFS Offshore  (P&I) : 2021 - Up Sell Test Opportunity' and @title='AKOFS Offshore  (P&I) : 2021 - Up Sell Test Opportunity']")
	public WebElement ReturnToOppFromApprovalPageP_IUpsel;
	@LinkType()
	@FindBy(xpath = "//span[text()='Close won' and normalize-space(.)='Close won']")
	public WebElement Opportunity_Closed_won_lightning;
	@LinkType()
	@FindBy(xpath = "//a/span[normalize-space(.)='AKOFS Offshore (P&I) : 2021 - Renewal Test Opportunity']")
	public WebElement ReturnToOppFromApprovalPage1;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//a[normalize-space(.)='BREAKWATER CAPI (Marine) : 2021 - Renewal Test Opportunity']")
	public WebElement ReturnToOppFromApprovalPage2;
	@LinkType()
	@FindBy(xpath = "//a/span[normalize-space(.)='AKOFS Offshore (Marine) : 2021 - Renewal Test Opportunity']")
	public WebElement ReturntoOppPageFromApprovalPage;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='BREAKWATER CAPI (Marine) : 2021 - Renewal Test Opportunity']")
	public WebElement ReturntoOppPageFromApprovalPage1;
	@ButtonType()
	@FindBy(xpath = "//button[normalize-space(.)='Today']")
	public WebElement today;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Products(1)']")
	public WebElement Product_1_Link;
			
}
