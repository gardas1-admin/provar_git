package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Logical Account Deletion"                                
               , summary=""
               , page="logicalAccountDeletion"
               , namespacePrefix=""
               , object="Account"
               , connection="MyGardRegressionUAT"
     )             
public class logicalAccountDeletion {

	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='This company is on risk and cannot be deactivated.']")
	public WebElement ErrorVerification;
	@ButtonType()
	@FindBy(xpath = "//input[@type='submit' and @value='Back' and contains(@class,'btn')]")
	public WebElement back;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='Company has been successfully deactivated.']")
	public WebElement DeactivatedMessage;
	@LinkType()
	@FindBy(xpath = "//div[@class='pbSubsection']/table/tbody/tr[3]/td/a[1]")
	public WebElement TestAutomationCompName;
	@TextType()
	@FindBy(xpath = "//td[normalize-space(.)='This company is on risk and cannot be deactivated.']")
	public WebElement BrokerOnRiskMessage;
	@TextType()
	@FindBy(xpath = "//span[normalize-space(.)='This company is on risk and cannot be deactivated.']")
	public WebElement BrokerOnRiskMessage1;
	
}
