package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="showCaseRelatedFilesWrapper"                                
               , summary=""
               , connection="MyGardE2E"
               , auraComponent="showCaseRelatedFilesWrapper"
               , namespacePrefix=""
     )             
public class showCaseRelatedFilesWrapper {

	@ButtonType()
	@AuraBy(componentXPath = "//lightning:button[@label= 'Clone with selected emails/files']")
	public WebElement cloneWithSelectedEmailsFiles;
	@ButtonType()
	@AuraBy(componentXPath = "//lightning:button[@label= 'Clone with selected emails/files']")
	public WebElement cloneWithSelectedEmailsFiles1;
	
}
