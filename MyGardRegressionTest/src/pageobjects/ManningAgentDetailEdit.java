package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="ManningAgentDetailEdit"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionStaging"
     )             
public class ManningAgentDetailEdit {

	@ChoiceListType(values = { @ChoiceListValue(value = "Under Approval"), @ChoiceListValue(value = "Approved"), @ChoiceListValue(value = "Rejected"), @ChoiceListValue(value = "Expired") })
	@FindBy(xpath = "//label[normalize-space(text())='Status']/ancestor::td[1]/following-sibling::td[1]/descendant::select")
	public WebElement Status;
	@TextType()
	@FindBy(xpath = "//label[text()='Contact Person']/ancestor::td[1]/following-sibling::td[1]/descendant::input")
	public WebElement ManningAgentContactPerson;
	@ButtonType()
	@FindBy(xpath = "//td[@id='topButtonRow']/descendant::input[1]")
	public WebElement SaveBtn;
	@TextType()
	@FindBy(xpath = "//td[text()='PEME Enrollment Form']/following-sibling::td[1]/descendant::a[1]")
	public WebElement PEMEEnrollmentForm;
			
}
