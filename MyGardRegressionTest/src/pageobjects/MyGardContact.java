package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyGardContact"                                
     , connection="MyGardRegressionStaging"
     )             
public class MyGardContact {

	@TextType()
	@FindBy(xpath = "//h1[contains(normalize-space(text()),'Contact')]")
	public WebElement Contact;
			
}
