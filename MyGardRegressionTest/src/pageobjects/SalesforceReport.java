package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="SalesforceReport"                                
     , summary=""
     , relativeUrl=""
     , connection="MyGardRegressionUAT"
     )             
public class SalesforceReport {

	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='New Report' and @data-aura-class='forceActionLink' and @role='button' and @title='New Report']")
	public WebElement newReport;

	@PageFrame()
	public static class ChooseReportType {

		@LinkType()
		@FindBy(xpath = "//a[normalize-space(.)='Companies' and @role='option']")
		public WebElement Companies;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Continue' and contains(@class,'slds-button') and @type='button']")
		public WebElement Continue;
	}

	@FindBy(xpath = "//iframe[@name='builder-1570085827942-962774']")
	public ChooseReportType chooseReportType;
	@TextType()
	@JavascriptBy(jspath = "return document.querySelector('div.autocompleteWrapper.slds-grow.slds-form-element__control > div > input')")
	public WebElement search;
	@TextType()
	@FindBy(xpath = "//div[@id='tilesHolder']/div/div/div/div/div/div[normalize-space(.)='nirmal.kumar@gard.no']")
	public WebElement PickAnAccount;
	@LinkType()
	@FindBy(xpath = "//a[@id='ShellMail_link']")
	public WebElement Outlook;
			
}
